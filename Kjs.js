var Kjs = {
    getUUID: function (window,success) {
        var iframe = document.createElement('iframe');
        var iframeStyle = document.createElement('style');
        var kHandler = function (e) {
            if (/^uuidLoaded/i.test(e.data)) {
                var split = e.data.split(':');
                window.removeEventListener('message', kHandler);
                success(split[1]);
            }
        };

        iframeStyle.innerHTML = "#artimedia-kjs{display:none!important;}"; // it isn't possible to apply !important using js.
        iframe.src = "//akamai.advsnx.net/CDN/sdk/k.html";
        iframe.id = 'artimedia-kjs';
        window.addEventListener('message', kHandler);
        document.head.appendChild(iframeStyle);
        document.body.appendChild(iframe);
    },

    setUUIDAjax: function (window) {
        window.$AM.ajaxSettings.cache = true;

        if (!AMUtils.hasValue(window.amLoadedUID)) {
            window.$AM.getScript("//lb.advsnx.net/asa/k.js").done(function (script, textStatus) {
                AMUtils.logAMTA("AMTALog: loaded #" + uuid);
                window.amLoadedUID = uuid;
            }).fail(function (jqxhr, settings, exception) {
                AMUtils.logAMTA(AMUtils.AMTALOG_WARN, "AMTAH5: k.js loading failed");
            });
        }
    }
};