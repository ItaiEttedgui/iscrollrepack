// universal module definition
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.Decoder = factory();
    }
}(this, function () {
  
  var global;
  
  function initglobal(){
    global = this;
    if (!global){
      if (typeof window != "undefined"){
        global = window;
      }else if (typeof self != "undefined"){
        global = self;
      };
    };
  };
  initglobal();
  
  
  function error(message) {
    console.error(message);
    console.trace();
  };

  
  function assert(condition, message) {
    if (!condition) {
      error(message);
    };
  };
  
  
  var getModule = function(par_bwOnHeadersDecoded, par_bwOnPictureDecoded){
    
    
   
    var resultModule = global.Module || Module;

    resultModule._bwOnHeadersDecoded = par_bwOnHeadersDecoded;
    resultModule._bwOnPictureDecoded = par_bwOnPictureDecoded;
    
    return resultModule;
  };

  return (function(){
    "use strict";
  
  
  var nowValue = function(){
    return (new Date()).getTime();
  };
  
  if (typeof performance != "undefined"){
    if (performance.now){
      nowValue = function(){
        return performance.now();
      };
    };
  };
  
  
  var Decoder = function(parOptions){
    this.options = parOptions || {};
    
    this.now = nowValue;
    
    var asmInstance;
    
    var fakeWindow = {
    };
    
    var onPicFun = function ($buffer, width, height) {
      var buffer = this.pictureBuffers[$buffer];
      if (!buffer) {
        buffer = this.pictureBuffers[$buffer] = toU8Array($buffer, (width * height * 3) / 2);
      };
      
      var infos;
      var doInfo = false;
      if (this.infoAr.length){
        doInfo = true;
        infos = this.infoAr;
      };
      this.infoAr = [];
      
      if (this.options.rgb){
        if (!asmInstance){
          asmInstance = getAsm(width, height);
        };
        asmInstance.inp.set(buffer);
        asmInstance.doit();

        var copyU8 = new Uint8Array(asmInstance.outSize);
        copyU8.set( asmInstance.out );
        
        if (doInfo){
          infos[0].finishDecoding = nowValue();
        };
        
        this.onPictureDecoded(copyU8, width, height, infos);
        return;
        
      };
      
      if (doInfo){
        infos[0].finishDecoding = nowValue();
      };
      this.onPictureDecoded(buffer, width, height, infos);
    }.bind(this);
    
    var ignore = false;
    
    if (this.options.sliceMode){
      onPicFun = function ($buffer, width, height, $sliceInfo) {
        if (ignore){
          return;
        };
        var buffer = this.pictureBuffers[$buffer];
        if (!buffer) {
          buffer = this.pictureBuffers[$buffer] = toU8Array($buffer, (width * height * 3) / 2);
        };
        var sliceInfo = this.pictureBuffers[$sliceInfo];
        if (!sliceInfo) {
          sliceInfo = this.pictureBuffers[$sliceInfo] = toU32Array($sliceInfo, 18);
        };

        var infos;
        var doInfo = false;
        if (this.infoAr.length){
          doInfo = true;
          infos = this.infoAr;
        };
        this.infoAr = [];

        /*if (this.options.rgb){
        
        no rgb in slice mode

        };*/

        infos[0].finishDecoding = nowValue();
        var sliceInfoAr = [];
        for (var i = 0; i < 20; ++i){
          sliceInfoAr.push(sliceInfo[i]);
        };
        infos[0].sliceInfoAr = sliceInfoAr;

        this.onPictureDecoded(buffer, width, height, infos);
      }.bind(this);
    };
    
    var Module = getModule.apply(fakeWindow, [function () {
    }, onPicFun]);
    

    var HEAP8 = Module.HEAP8;
    var HEAPU8 = Module.HEAPU8;
    var HEAP16 = Module.HEAP16;
    var HEAP32 = Module.HEAP32;

    
    var MAX_STREAM_BUFFER_LENGTH = 1024 * 1024;
  
    // from old constructor
    Module._bwInit();
    
    /**
   * Creates a typed array from a HEAP8 pointer. 
   */
    function toU8Array(ptr, length) {
      return HEAPU8.subarray(ptr, ptr + length);
    };
    function toU32Array(ptr, length) {
      //var tmp = HEAPU8.subarray(ptr, ptr + (length * 4));
      return new Uint32Array(HEAPU8.buffer, ptr, length);
    };
    this.streamBuffer = toU8Array(Module._bwCreateStream(MAX_STREAM_BUFFER_LENGTH), MAX_STREAM_BUFFER_LENGTH);
    this.pictureBuffers = {};
    // collect extra infos that are provided with the nal units
    this.infoAr = [];
    
    this.onPictureDecoded = function (buffer, width, height, infos) {
      
    };
    
    /**
     * Decodes a stream buffer. This may be one single (unframed) NAL unit without the
     * start code, or a sequence of NAL units with framing start code prefixes. This
     * function overwrites stream buffer allocated by the codec with the supplied buffer.
     */
    
    var sliceNum = 0;
    if (this.options.sliceMode){
      sliceNum = this.options.sliceNum;
      
      this.decode = function decode(typedAr, parInfo, copyDoneFun) {
        this.infoAr.push(parInfo);
        parInfo.startDecoding = nowValue();
        var nals = parInfo.nals;
        var i;
        if (!nals){
          nals = [];
          parInfo.nals = nals;
          var l = typedAr.length;
          var foundSomething = false;
          var lastFound = 0;
          var lastStart = 0;
          for (i = 0; i < l; ++i){
            if (typedAr[i] === 1){
              if (
                typedAr[i - 1] === 0 &&
                typedAr[i - 2] === 0
              ){
                var startPos = i - 2;
                if (typedAr[i - 3] === 0){
                  startPos = i - 3;
                };
                // its a nal;
                if (foundSomething){
                  nals.push({
                    offset: lastFound,
                    end: startPos,
                    type: typedAr[lastStart] & 31
                  });
                };
                lastFound = startPos;
                lastStart = startPos + 3;
                if (typedAr[i - 3] === 0){
                  lastStart = startPos + 4;
                };
                foundSomething = true;
              };
            };
          };
          if (foundSomething){
            nals.push({
              offset: lastFound,
              end: i,
              type: typedAr[lastStart] & 31
            });
          };
        };
        
        var currentSlice = 0;
        var playAr;
        var offset = 0;
        for (i = 0; i < nals.length; ++i){
          if (nals[i].type === 1 || nals[i].type === 5){
            if (currentSlice === sliceNum){
              playAr = typedAr.subarray(nals[i].offset, nals[i].end);
              this.streamBuffer[offset] = 0;
              offset += 1;
              this.streamBuffer.set(playAr, offset);
              offset += playAr.length;
            };
            currentSlice += 1;
          }else{
            playAr = typedAr.subarray(nals[i].offset, nals[i].end);
            this.streamBuffer[offset] = 0;
            offset += 1;
            this.streamBuffer.set(playAr, offset);
            offset += playAr.length;
            Module._bwPlayStream(offset);
            offset = 0;
          };
        };
        copyDoneFun();
        Module._bwPlayStream(offset);
      };
      
    }else{
      this.decode = function decode(typedAr, parInfo) {
        // console.info("Decoding: " + buffer.length);
        // collect infos
        if (parInfo){
          this.infoAr.push(parInfo);
          parInfo.startDecoding = nowValue();
        };

        this.streamBuffer.set(typedAr);
        Module._bwPlayStream(typedAr.length);
      };
    };

  };

  
  Decoder.prototype = {
    
  };
  
  
  
  
  /*
  
    asm.js implementation of a yuv to rgb convertor
    provided by @soliton4
    
    based on 
    http://www.wordsaretoys.com/2013/10/18/making-yuv-conversion-a-little-faster/
  
  */
  
  
  // factory to create asm.js yuv -> rgb convertor for a given resolution
  var asmInstances = {};
  var getAsm = function(parWidth, parHeight){
    var idStr = "" + parWidth + "x" + parHeight;
    if (asmInstances[idStr]){
      return asmInstances[idStr];
    };

    var lumaSize = parWidth * parHeight;
    var chromaSize = (lumaSize|0) >> 2;

    var inpSize = lumaSize + chromaSize + chromaSize;
    var outSize = parWidth * parHeight * 4;
    var cacheSize = Math.pow(2, 24) * 4;
    var size = inpSize + outSize + cacheSize;

    var chunkSize = Math.pow(2, 24);
    var heapSize = chunkSize;
    while (heapSize < size){
      heapSize += chunkSize;
    };
    var heap = new ArrayBuffer(heapSize);

    var res = asmFactory(global, {}, heap);
    res.init(parWidth, parHeight);
    asmInstances[idStr] = res;

    res.heap = heap;
    res.out = new Uint8Array(heap, 0, outSize);
    res.inp = new Uint8Array(heap, outSize, inpSize);
    res.outSize = outSize;

    return res;
  };


  function asmFactory(stdlib, foreign, heap) {
    "use asm";

    var imul = stdlib.Math.imul;
    var min = stdlib.Math.min;
    var max = stdlib.Math.max;
    var pow = stdlib.Math.pow;
    var out = new stdlib.Uint8Array(heap);
    var out32 = new stdlib.Uint32Array(heap);
    var inp = new stdlib.Uint8Array(heap);
    var mem = new stdlib.Uint8Array(heap);
    var mem32 = new stdlib.Uint32Array(heap);

    // for double algo
    /*var vt = 1.370705;
    var gt = 0.698001;
    var gt2 = 0.337633;
    var bt = 1.732446;*/

    var width = 0;
    var height = 0;
    var lumaSize = 0;
    var chromaSize = 0;
    var inpSize = 0;
    var outSize = 0;

    var inpStart = 0;
    var outStart = 0;

    var widthFour = 0;

    var cacheStart = 0;


    function init(parWidth, parHeight){
      parWidth = parWidth|0;
      parHeight = parHeight|0;

      var i = 0;
      var s = 0;

      width = parWidth;
      widthFour = imul(parWidth, 4)|0;
      height = parHeight;
      lumaSize = imul(width|0, height|0)|0;
      chromaSize = (lumaSize|0) >> 2;
      outSize = imul(imul(width, height)|0, 4)|0;
      inpSize = ((lumaSize + chromaSize)|0 + chromaSize)|0;

      outStart = 0;
      inpStart = (outStart + outSize)|0;
      cacheStart = (inpStart + inpSize)|0;

      // initializing memory (to be on the safe side)
      s = ~~(+pow(+2, +24));
      s = imul(s, 4)|0;

      for (i = 0|0; ((i|0) < (s|0))|0; i = (i + 4)|0){
        mem32[((cacheStart + i)|0) >> 2] = 0;
      };
    };

    function doit(){
      var ystart = 0;
      var ustart = 0;
      var vstart = 0;

      var y = 0;
      var yn = 0;
      var u = 0;
      var v = 0;

      var o = 0;

      var line = 0;
      var col = 0;

      var usave = 0;
      var vsave = 0;

      var ostart = 0;
      var cacheAdr = 0;

      ostart = outStart|0;

      ystart = inpStart|0;
      ustart = (ystart + lumaSize|0)|0;
      vstart = (ustart + chromaSize)|0;

      for (line = 0; (line|0) < (height|0); line = (line + 2)|0){
        usave = ustart;
        vsave = vstart;
        for (col = 0; (col|0) < (width|0); col = (col + 2)|0){
          y = inp[ystart >> 0]|0;
          yn = inp[((ystart + width)|0) >> 0]|0;

          u = inp[ustart >> 0]|0;
          v = inp[vstart >> 0]|0;

          cacheAdr = (((((y << 16)|0) + ((u << 8)|0))|0) + v)|0;
          o = mem32[((cacheStart + cacheAdr)|0) >> 2]|0;
          if (o){}else{
            o = yuv2rgbcalc(y,u,v)|0;
            mem32[((cacheStart + cacheAdr)|0) >> 2] = o|0;
          };
          mem32[ostart >> 2] = o;

          cacheAdr = (((((yn << 16)|0) + ((u << 8)|0))|0) + v)|0;
          o = mem32[((cacheStart + cacheAdr)|0) >> 2]|0;
          if (o){}else{
            o = yuv2rgbcalc(yn,u,v)|0;
            mem32[((cacheStart + cacheAdr)|0) >> 2] = o|0;
          };
          mem32[((ostart + widthFour)|0) >> 2] = o;

          //yuv2rgb5(y, u, v, ostart);
          //yuv2rgb5(yn, u, v, (ostart + widthFour)|0);
          ostart = (ostart + 4)|0;

          // next step only for y. u and v stay the same
          ystart = (ystart + 1)|0;
          y = inp[ystart >> 0]|0;
          yn = inp[((ystart + width)|0) >> 0]|0;

          //yuv2rgb5(y, u, v, ostart);
          cacheAdr = (((((y << 16)|0) + ((u << 8)|0))|0) + v)|0;
          o = mem32[((cacheStart + cacheAdr)|0) >> 2]|0;
          if (o){}else{
            o = yuv2rgbcalc(y,u,v)|0;
            mem32[((cacheStart + cacheAdr)|0) >> 2] = o|0;
          };
          mem32[ostart >> 2] = o;

          //yuv2rgb5(yn, u, v, (ostart + widthFour)|0);
          cacheAdr = (((((yn << 16)|0) + ((u << 8)|0))|0) + v)|0;
          o = mem32[((cacheStart + cacheAdr)|0) >> 2]|0;
          if (o){}else{
            o = yuv2rgbcalc(yn,u,v)|0;
            mem32[((cacheStart + cacheAdr)|0) >> 2] = o|0;
          };
          mem32[((ostart + widthFour)|0) >> 2] = o;
          ostart = (ostart + 4)|0;

          //all positions inc 1

          ystart = (ystart + 1)|0;
          ustart = (ustart + 1)|0;
          vstart = (vstart + 1)|0;
        };
        ostart = (ostart + widthFour)|0;
        ystart = (ystart + width)|0;

      };

    };

    function yuv2rgbcalc(y, u, v){
      y = y|0;
      u = u|0;
      v = v|0;

      var r = 0;
      var g = 0;
      var b = 0;

      var o = 0;

      var a0 = 0;
      var a1 = 0;
      var a2 = 0;
      var a3 = 0;
      var a4 = 0;

      a0 = imul(1192, (y - 16)|0)|0;
      a1 = imul(1634, (v - 128)|0)|0;
      a2 = imul(832, (v - 128)|0)|0;
      a3 = imul(400, (u - 128)|0)|0;
      a4 = imul(2066, (u - 128)|0)|0;

      r = (((a0 + a1)|0) >> 10)|0;
      g = (((((a0 - a2)|0) - a3)|0) >> 10)|0;
      b = (((a0 + a4)|0) >> 10)|0;

      if ((((r & 255)|0) != (r|0))|0){
        r = min(255, max(0, r|0)|0)|0;
      };
      if ((((g & 255)|0) != (g|0))|0){
        g = min(255, max(0, g|0)|0)|0;
      };
      if ((((b & 255)|0) != (b|0))|0){
        b = min(255, max(0, b|0)|0)|0;
      };

      o = 255;
      o = (o << 8)|0;
      o = (o + b)|0;
      o = (o << 8)|0;
      o = (o + g)|0;
      o = (o << 8)|0;
      o = (o + r)|0;

      return o|0;

    };



    return {
      init: init,
      doit: doit
    };
  };

  
  /*
    potential worker initialization
  
  */
  
  
  if (typeof self != "undefined"){
    var isWorker = false;
    var decoder;
    var reuseMemory = false;
    var sliceMode = false;
    var sliceNum = 0;
    var sliceCnt = 0;
    var lastSliceNum = 0;
    var sliceInfoAr;
    var lastBuf;
    var awaiting = 0;
    var pile = [];
    var startDecoding;
    var finishDecoding;
    var timeDecoding;
    
    var memAr = [];
    var getMem = function(length){
      if (memAr.length){
        var u = memAr.shift();
        while (u && u.byteLength !== length){
          u = memAr.shift();
        };
        if (u){
          return u;
        };
      };
      return new ArrayBuffer(length);
    }; 
    
    var copySlice = function(source, target, infoAr, width, height){
      
      var length = width * height;
      var length4 = length / 4
      var plane2 = length;
      var plane3 = length + length4;
      
      var copy16 = function(parBegin, parEnd){
        var i = 0;
        for (i = 0; i < 16; ++i){
          var begin = parBegin + (width * i);
          var end = parEnd + (width * i)
          target.set(source.subarray(begin, end), begin);
        };
      };
      var copy8 = function(parBegin, parEnd){
        var i = 0;
        for (i = 0; i < 8; ++i){
          var begin = parBegin + ((width / 2) * i);
          var end = parEnd + ((width / 2) * i)
          target.set(source.subarray(begin, end), begin);
        };
      };
      var copyChunk = function(begin, end){
        target.set(source.subarray(begin, end), begin);
      };
      
      var begin = infoAr[0];
      var end = infoAr[1];
      if (end > 0){
        copy16(begin, end);
        copy8(infoAr[2], infoAr[3]);
        copy8(infoAr[4], infoAr[5]);
      };
      begin = infoAr[6];
      end = infoAr[7];
      if (end > 0){
        copy16(begin, end);
        copy8(infoAr[8], infoAr[9]);
        copy8(infoAr[10], infoAr[11]);
      };
      
      begin = infoAr[12];
      end = infoAr[15];
      if (end > 0){
        copyChunk(begin, end);
        copyChunk(infoAr[13], infoAr[16]);
        copyChunk(infoAr[14], infoAr[17]);
      };
      
    };
    
    var sliceMsgFun = function(){};
    
    var setSliceCnt = function(parSliceCnt){
      sliceCnt = parSliceCnt;
      lastSliceNum = sliceCnt - 1;
    };
    
    
    self.addEventListener('message', function(e) {
      
      if (isWorker){
        if (reuseMemory){
          if (e.data.reuse){
            memAr.push(e.data.reuse);
          };
        };
        if (e.data.buf){
          if (sliceMode && awaiting !== 0){
            pile.push(e.data);
          }else{
            decoder.decode(
              new Uint8Array(e.data.buf, e.data.offset || 0, e.data.length), 
              e.data.info, 
              function(){
                if (sliceMode && sliceNum !== lastSliceNum){
                  postMessage(e.data, [e.data.buf]);
                };
              }
            );
          };
          return;
        };
        
        if (e.data.slice){
          // update ref pic
          var copyStart = nowValue();
          copySlice(new Uint8Array(e.data.slice), lastBuf, e.data.infos[0].sliceInfoAr, e.data.width, e.data.height);
          // is it the one? then we need to update it
          if (e.data.theOne){
            copySlice(lastBuf, new Uint8Array(e.data.slice), sliceInfoAr, e.data.width, e.data.height);
            if (timeDecoding > e.data.infos[0].timeDecoding){
              e.data.infos[0].timeDecoding = timeDecoding;
            };
            e.data.infos[0].timeCopy += (nowValue() - copyStart);
          };
          // move on
          postMessage(e.data, [e.data.slice]);
          
          // next frame in the pipe?
          awaiting -= 1;
          if (awaiting === 0 && pile.length){
            var data = pile.shift();
            decoder.decode(
              new Uint8Array(data.buf, data.offset || 0, data.length), 
              data.info, 
              function(){
                if (sliceMode && sliceNum !== lastSliceNum){
                  postMessage(data, [data.buf]);
                };
              }
            );
          };
          return;
        };
        
        if (e.data.setSliceCnt){
          setSliceCnt(e.data.sliceCnt);
          return;
        };
        
      }else{
        if (e.data && e.data.type === "init"){
          isWorker = true;
          decoder = new Decoder(e.data.options);
          
          if (e.data.options.sliceMode){
            reuseMemory = true;
            sliceMode = true;
            sliceNum = e.data.options.sliceNum;
            setSliceCnt(e.data.options.sliceCnt);

            decoder.onPictureDecoded = function (buffer, width, height, infos) {
              
              // buffer needs to be copied because we give up ownership
              var copyU8 = new Uint8Array(getMem(buffer.length));
              copySlice(buffer, copyU8, infos[0].sliceInfoAr, width, height);
              
              startDecoding = infos[0].startDecoding;
              finishDecoding = infos[0].finishDecoding;
              timeDecoding = finishDecoding - startDecoding;
              infos[0].timeDecoding = timeDecoding;
              infos[0].timeCopy = 0;
              
              postMessage({
                slice: copyU8.buffer,
                sliceNum: sliceNum,
                width: width, 
                height: height, 
                infos: infos
              }, [copyU8.buffer]); // 2nd parameter is used to indicate transfer of ownership
              
              awaiting = sliceCnt - 1;
              
              lastBuf = buffer;
              sliceInfoAr = infos[0].sliceInfoAr;

            };
            
          }else if (e.data.options.reuseMemory){
            reuseMemory = true;
            decoder.onPictureDecoded = function (buffer, width, height, infos) {
              
              // buffer needs to be copied because we give up ownership
              var copyU8 = new Uint8Array(getMem(buffer.length));
              copyU8.set( buffer, 0, buffer.length );

              postMessage({
                buf: copyU8.buffer, 
                length: buffer.length,
                width: width, 
                height: height, 
                infos: infos
              }, [copyU8.buffer]); // 2nd parameter is used to indicate transfer of ownership

            };
            
          }else{
            decoder.onPictureDecoded = function (buffer, width, height, infos) {
              if (buffer) {
                buffer = new Uint8Array(buffer);
              };

              // buffer needs to be copied because we give up ownership
              var copyU8 = new Uint8Array(buffer.length);
              copyU8.set( buffer, 0, buffer.length );

              postMessage({
                buf: copyU8.buffer, 
                length: buffer.length,
                width: width, 
                height: height, 
                infos: infos
              }, [copyU8.buffer]); // 2nd parameter is used to indicate transfer of ownership

            };
          };
          postMessage({ consoleLog: "bw worker initialized" });
        };
      };


    }, false);
  };
  
  Decoder.nowValue = nowValue;
  
  return Decoder;
  
  })();
  
  
}));


//
//  Copyright (c) 2015 Paperspace Co. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to
//  deal in the Software without restriction, including without limitation the
//  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//


// universal module definition
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.YUVCanvas = factory();
    }
}(this, function () {


/**
 * This class can be used to render output pictures from an H264bsdDecoder to a canvas element.
 * If available the content is rendered using WebGL.
 */
  function H264bsdCanvas(parOptions) {
    
    parOptions = parOptions || {};
    
    this.canvasElement = parOptions.canvas || document.createElement("canvas");
    this.contextOptions = parOptions.contextOptions;
    
    this.type = parOptions.type || "yuv420";
    
    this.customYUV444 = parOptions.customYUV444;
    
    this.width = parOptions.width || 640;
    this.height = parOptions.height || 320;
    
    this.animationTime = parOptions.animationTime || 0;
    
    this.canvasElement.width = this.width;
    this.canvasElement.height = this.height;

    this.initContextGL();

    if(this.contextGL) {
      this.initProgram();
      this.initBuffers();
      this.initTextures();
    };
    
    
    /**
 * Draw the next output picture using WebGL
 */
    if (this.type === "yuv420"){
      this.drawNextOuptutPictureGL = function(par) {
        var gl = this.contextGL;
        var texturePosBuffer = this.texturePosBuffer;
        var uTexturePosBuffer = this.uTexturePosBuffer;
        var vTexturePosBuffer = this.vTexturePosBuffer;
        
        var yTextureRef = this.yTextureRef;
        var uTextureRef = this.uTextureRef;
        var vTextureRef = this.vTextureRef;
        
        var yData = par.yData;
        var uData = par.uData;
        var vData = par.vData;
        
        var width = this.width;
        var height = this.height;
        
        var yDataPerRow = par.yDataPerRow || width;
        var yRowCnt     = par.yRowCnt || height;
        
        var uDataPerRow = par.uDataPerRow || (width / 2);
        var uRowCnt     = par.uRowCnt || (height / 2);
        
        var vDataPerRow = par.vDataPerRow || uDataPerRow;
        var vRowCnt     = par.vRowCnt || uRowCnt;
        
        gl.viewport(0, 0, width, height);

        var tTop = 0;
        var tLeft = 0;
        var tBottom = height / yRowCnt;
        var tRight = width / yDataPerRow;
        var texturePosValues = new Float32Array([tRight, tTop, tLeft, tTop, tRight, tBottom, tLeft, tBottom]);

        gl.bindBuffer(gl.ARRAY_BUFFER, texturePosBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, texturePosValues, gl.DYNAMIC_DRAW);
        
        if (this.customYUV444){
          tBottom = height / uRowCnt;
          tRight = width / uDataPerRow;
        }else{
          tBottom = (height / 2) / uRowCnt;
          tRight = (width / 2) / uDataPerRow;
        };
        var uTexturePosValues = new Float32Array([tRight, tTop, tLeft, tTop, tRight, tBottom, tLeft, tBottom]);

        gl.bindBuffer(gl.ARRAY_BUFFER, uTexturePosBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, uTexturePosValues, gl.DYNAMIC_DRAW);
        
        
        if (this.customYUV444){
          tBottom = height / vRowCnt;
          tRight = width / vDataPerRow;
        }else{
          tBottom = (height / 2) / vRowCnt;
          tRight = (width / 2) / vDataPerRow;
        };
        var vTexturePosValues = new Float32Array([tRight, tTop, tLeft, tTop, tRight, tBottom, tLeft, tBottom]);

        gl.bindBuffer(gl.ARRAY_BUFFER, vTexturePosBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, vTexturePosValues, gl.DYNAMIC_DRAW);
        

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, yTextureRef);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, yDataPerRow, yRowCnt, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, yData);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, uTextureRef);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, uDataPerRow, uRowCnt, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, uData);

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D, vTextureRef);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, vDataPerRow, vRowCnt, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, vData);

        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4); 
      };
      
    }else if (this.type === "yuv422"){
      this.drawNextOuptutPictureGL = function(par) {
        var gl = this.contextGL;
        var texturePosBuffer = this.texturePosBuffer;
        
        var textureRef = this.textureRef;
        
        var data = par.data;
        
        var width = this.width;
        var height = this.height;
        
        var dataPerRow = par.dataPerRow || (width * 2);
        var rowCnt     = par.rowCnt || height;

        gl.viewport(0, 0, width, height);

        var tTop = 0;
        var tLeft = 0;
        var tBottom = height / rowCnt;
        var tRight = width / (dataPerRow / 2);
        var texturePosValues = new Float32Array([tRight, tTop, tLeft, tTop, tRight, tBottom, tLeft, tBottom]);

        gl.bindBuffer(gl.ARRAY_BUFFER, texturePosBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, texturePosValues, gl.DYNAMIC_DRAW);
        
        gl.uniform2f(gl.getUniformLocation(this.shaderProgram, 'resolution'), dataPerRow, height);
        
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureRef);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, dataPerRow, rowCnt, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, data);

        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4); 
      };
    };
    
  };

  /**
 * Returns true if the canvas supports WebGL
 */
  H264bsdCanvas.prototype.isWebGL = function() {
    return this.contextGL;
  };

  /**
 * Create the GL context from the canvas element
 */
  H264bsdCanvas.prototype.initContextGL = function() {
    var canvas = this.canvasElement;
    var gl = null;

    var validContextNames = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"];
    var nameIndex = 0;

    while(!gl && nameIndex < validContextNames.length) {
      var contextName = validContextNames[nameIndex];

      try {
        if (this.contextOptions){
          gl = canvas.getContext(contextName, this.contextOptions);
        }else{
          gl = canvas.getContext(contextName);
        };
      } catch (e) {
        gl = null;
      }

      if(!gl || typeof gl.getParameter !== "function") {
        gl = null;
      }    

      ++nameIndex;
    };

    this.contextGL = gl;
  };

/**
 * Initialize GL shader program
 */
H264bsdCanvas.prototype.initProgram = function() {
    var gl = this.contextGL;

  // vertex shader is the same for all types
  var vertexShaderScript;
  var fragmentShaderScript;
  
  if (this.type === "yuv420"){

    vertexShaderScript = [
      'attribute vec4 vertexPos;',
      'attribute vec4 texturePos;',
      'attribute vec4 uTexturePos;',
      'attribute vec4 vTexturePos;',
      'varying vec2 textureCoord;',
      'varying vec2 uTextureCoord;',
      'varying vec2 vTextureCoord;',

      'void main()',
      '{',
      '  gl_Position = vertexPos;',
      '  textureCoord = texturePos.xy;',
      '  uTextureCoord = uTexturePos.xy;',
      '  vTextureCoord = vTexturePos.xy;',
      '}'
    ].join('\n');
    
    fragmentShaderScript = [
      'precision highp float;',
      'varying highp vec2 textureCoord;',
      'varying highp vec2 uTextureCoord;',
      'varying highp vec2 vTextureCoord;',
      'uniform sampler2D ySampler;',
      'uniform sampler2D uSampler;',
      'uniform sampler2D vSampler;',
      'const mat4 YUV2RGB = mat4',
      '(',
      '  1.1643828125, 0, 1.59602734375, -.87078515625,',
      '  1.1643828125, -.39176171875, -.81296875, .52959375,',
      '  1.1643828125, 2.017234375, 0, -1.081390625,',
      '  0, 0, 0, 1',
      ');',

      'void main(void) {',
      '  highp float y = texture2D(ySampler,  textureCoord).r;',
      '  highp float u = texture2D(uSampler,  uTextureCoord).r;',
      '  highp float v = texture2D(vSampler,  vTextureCoord).r;',
      '  gl_FragColor = vec4(y, u, v, 1) * YUV2RGB;',
      '}'
    ].join('\n');
    
  }else if (this.type === "yuv422"){
    vertexShaderScript = [
      'attribute vec4 vertexPos;',
      'attribute vec4 texturePos;',
      'varying vec2 textureCoord;',

      'void main()',
      '{',
      '  gl_Position = vertexPos;',
      '  textureCoord = texturePos.xy;',
      '}'
    ].join('\n');
    
    fragmentShaderScript = [
      'precision highp float;',
      'varying highp vec2 textureCoord;',
      'uniform sampler2D sampler;',
      'uniform highp vec2 resolution;',
      'const mat4 YUV2RGB = mat4',
      '(',
      '  1.1643828125, 0, 1.59602734375, -.87078515625,',
      '  1.1643828125, -.39176171875, -.81296875, .52959375,',
      '  1.1643828125, 2.017234375, 0, -1.081390625,',
      '  0, 0, 0, 1',
      ');',

      'void main(void) {',
      
      '  highp float texPixX = 1.0 / resolution.x;',
      '  highp float logPixX = 2.0 / resolution.x;', // half the resolution of the texture
      '  highp float logHalfPixX = 4.0 / resolution.x;', // half of the logical resolution so every 4th pixel
      '  highp float steps = floor(textureCoord.x / logPixX);',
      '  highp float uvSteps = floor(textureCoord.x / logHalfPixX);',
      '  highp float y = texture2D(sampler, vec2((logPixX * steps) + texPixX, textureCoord.y)).r;',
      '  highp float u = texture2D(sampler, vec2((logHalfPixX * uvSteps), textureCoord.y)).r;',
      '  highp float v = texture2D(sampler, vec2((logHalfPixX * uvSteps) + texPixX + texPixX, textureCoord.y)).r;',
      
      //'  highp float y = texture2D(sampler,  textureCoord).r;',
      //'  gl_FragColor = vec4(y, u, v, 1) * YUV2RGB;',
      '  gl_FragColor = vec4(y, u, v, 1.0) * YUV2RGB;',
      '}'
    ].join('\n');
  };


  var vertexShader = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vertexShader, vertexShaderScript);
  gl.compileShader(vertexShader);
  if(!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
    console.log('Vertex shader failed to compile: ' + gl.getShaderInfoLog(vertexShader));
  }

  var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fragmentShader, fragmentShaderScript);
  gl.compileShader(fragmentShader);
  if(!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
    console.log('Fragment shader failed to compile: ' + gl.getShaderInfoLog(fragmentShader));
  }

  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  if(!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    console.log('Program failed to compile: ' + gl.getProgramInfoLog(program));
  }

  gl.useProgram(program);

  this.shaderProgram = program;
};

/**
 * Initialize vertex buffers and attach to shader program
 */
H264bsdCanvas.prototype.initBuffers = function() {
  var gl = this.contextGL;
  var program = this.shaderProgram;

  var vertexPosBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexPosBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([1, 1, -1, 1, 1, -1, -1, -1]), gl.STATIC_DRAW);

  var vertexPosRef = gl.getAttribLocation(program, 'vertexPos');
  gl.enableVertexAttribArray(vertexPosRef);
  gl.vertexAttribPointer(vertexPosRef, 2, gl.FLOAT, false, 0, 0);
  
  if (this.animationTime){
    
    var animationTime = this.animationTime;
    var timePassed = 0;
    var stepTime = 15;
  
    var aniFun = function(){
      
      timePassed += stepTime;
      var mul = ( 1 * timePassed ) / animationTime;
      
      if (timePassed >= animationTime){
        mul = 1;
      }else{
        setTimeout(aniFun, stepTime);
      };
      
      var neg = -1 * mul;
      var pos = 1 * mul;

      var vertexPosBuffer = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexPosBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([pos, pos, neg, pos, pos, neg, neg, neg]), gl.STATIC_DRAW);

      var vertexPosRef = gl.getAttribLocation(program, 'vertexPos');
      gl.enableVertexAttribArray(vertexPosRef);
      gl.vertexAttribPointer(vertexPosRef, 2, gl.FLOAT, false, 0, 0);
      
      try{
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
      }catch(e){};

    };
    aniFun();
    
  };

  

  var texturePosBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texturePosBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([1, 0, 0, 0, 1, 1, 0, 1]), gl.STATIC_DRAW);

  var texturePosRef = gl.getAttribLocation(program, 'texturePos');
  gl.enableVertexAttribArray(texturePosRef);
  gl.vertexAttribPointer(texturePosRef, 2, gl.FLOAT, false, 0, 0);

  this.texturePosBuffer = texturePosBuffer;

  if (this.type === "yuv420"){
    var uTexturePosBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, uTexturePosBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([1, 0, 0, 0, 1, 1, 0, 1]), gl.STATIC_DRAW);

    var uTexturePosRef = gl.getAttribLocation(program, 'uTexturePos');
    gl.enableVertexAttribArray(uTexturePosRef);
    gl.vertexAttribPointer(uTexturePosRef, 2, gl.FLOAT, false, 0, 0);

    this.uTexturePosBuffer = uTexturePosBuffer;
    
    
    var vTexturePosBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vTexturePosBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([1, 0, 0, 0, 1, 1, 0, 1]), gl.STATIC_DRAW);

    var vTexturePosRef = gl.getAttribLocation(program, 'vTexturePos');
    gl.enableVertexAttribArray(vTexturePosRef);
    gl.vertexAttribPointer(vTexturePosRef, 2, gl.FLOAT, false, 0, 0);

    this.vTexturePosBuffer = vTexturePosBuffer;
  };

};

/**
 * Initialize GL textures and attach to shader program
 */
H264bsdCanvas.prototype.initTextures = function() {
  var gl = this.contextGL;
  var program = this.shaderProgram;

  if (this.type === "yuv420"){

    var yTextureRef = this.initTexture();
    var ySamplerRef = gl.getUniformLocation(program, 'ySampler');
    gl.uniform1i(ySamplerRef, 0);
    this.yTextureRef = yTextureRef;

    var uTextureRef = this.initTexture();
    var uSamplerRef = gl.getUniformLocation(program, 'uSampler');
    gl.uniform1i(uSamplerRef, 1);
    this.uTextureRef = uTextureRef;

    var vTextureRef = this.initTexture();
    var vSamplerRef = gl.getUniformLocation(program, 'vSampler');
    gl.uniform1i(vSamplerRef, 2);
    this.vTextureRef = vTextureRef;
    
  }else if (this.type === "yuv422"){
    // only one texture for 422
    var textureRef = this.initTexture();
    var samplerRef = gl.getUniformLocation(program, 'sampler');
    gl.uniform1i(samplerRef, 0);
    this.textureRef = textureRef;

  };
};

/**
 * Create and configure a single texture
 */
H264bsdCanvas.prototype.initTexture = function() {
    var gl = this.contextGL;

    var textureRef = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, textureRef);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.bindTexture(gl.TEXTURE_2D, null);

    return textureRef;
};

/**
 * Draw picture data to the canvas.
 * If this object is using WebGL, the data must be an I420 formatted ArrayBuffer,
 * Otherwise, data must be an RGBA formatted ArrayBuffer.
 */
H264bsdCanvas.prototype.drawNextOutputPicture = function(width, height, croppingParams, data) {
    var gl = this.contextGL;

    if(gl) {
        this.drawNextOuptutPictureGL(width, height, croppingParams, data);
    } else {
        this.drawNextOuptutPictureRGBA(width, height, croppingParams, data);
    }
};



/**
 * Draw next output picture using ARGB data on a 2d canvas.
 */
H264bsdCanvas.prototype.drawNextOuptutPictureRGBA = function(width, height, croppingParams, data) {
    var canvas = this.canvasElement;

    var croppingParams = null;

    var argbData = data;

    var ctx = canvas.getContext('2d');
    var imageData = ctx.getImageData(0, 0, width, height);
    imageData.data.set(argbData);

    if(croppingParams === null) {
        ctx.putImageData(imageData, 0, 0);
    } else {
        ctx.putImageData(imageData, -croppingParams.left, -croppingParams.top, 0, 0, croppingParams.width, croppingParams.height);
    }
};
  
  return H264bsdCanvas;
  
}));
/*


usage:

p = new Player({
  useWorker: <bool>,
  workerFile: <defaults to "Decoder.js"> // give path to Decoder.js
  webgl: true | false | "auto" // defaults to "auto"
});

// canvas property represents the canvas node
// put it somewhere in the dom
p.canvas;

p.webgl; // contains the used rendering mode. if you pass auto to webgl you can see what auto detection resulted in

p.decode(<binary>);


*/



// universal module definition
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(["./Decoder", "./YUVCanvas"], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require("./Decoder"), require("./YUVCanvas"));
    } else {
        // Browser globals (root is window)
        root.Player = factory(root.Decoder, root.YUVCanvas);
    }
}(this, function (Decoder, WebGLCanvas) {
  "use strict";
  
  
  var nowValue = Decoder.nowValue;
  
  
  var Player = function(parOptions){
    var self = this;
    this._config = parOptions || {};
    
    this.render = true;
    if (this._config.render === false){
      this.render = false;
    };
    
    this.nowValue = nowValue;
    
    this._config.workerFile = this._config.workerFile || (targetPath + "amd.js");
    if (this._config.preserveDrawingBuffer){
      this._config.contextOptions = this._config.contextOptions || {};
      this._config.contextOptions.preserveDrawingBuffer = true;
    };
    
    var webgl = "auto";
    if (this._config.webgl === true){
      webgl = true;
    }else if (this._config.webgl === false){
      webgl = false;
    };
    
    if (webgl == "auto"){
      webgl = true;
      try{
        if (!window.WebGLRenderingContext) {
          // the browser doesn't even know what WebGL is
          webgl = false;
        } else {
          var canvas = document.createElement('canvas');
          var ctx = canvas.getContext("webgl");
          if (!ctx) {
            // browser supports WebGL but initialization failed.
            webgl = false;
          };
        };
      }catch(e){
        webgl = false;
      };
    };
    
    this.webgl = webgl;
    
    // choose functions
    if (this.webgl){
      this.createCanvasObj = this.createCanvasWebGL;
      this.renderFrame = this.renderFrameWebGL;
    }else{
      this.createCanvasObj = this.createCanvasRGB;
      this.renderFrame = this.renderFrameRGB;
    };
    
    
    var lastWidth;
    var lastHeight;
	
	
    var onPictureDecoded = function(buffer, width, height, infos) {
      self.onPictureDecoded(buffer, width, height, infos);
      
      var startTime = nowValue();
      
      if (!buffer || !self.render) {
        return;
      };
      
      self.renderFrame({
        canvasObj: self.canvasObj,
        data: buffer,
        width: width,
        height: height
      });
      
      if (self.onRenderFrameComplete){
        self.onRenderFrameComplete({
          data: buffer,
          width: width,
          height: height,
          infos: infos,
          canvasObj: self.canvasObj
        });
      };
      
    };
    
    // provide size
    
    if (!this._config.size){
      this._config.size = {};
    };
    this._config.size.width = this._config.size.width || 200;
    this._config.size.height = this._config.size.height || 200;
    

     if (this._config.useWorker){
		$AM.ajaxSetup ({ cache: true });
		$AM(this).load( this._config.workerFile, function(data) {
			var that = this;
			var blob = new Blob([data], { type: "text/javascript" });
        
			 var worker = new Worker(window.URL.createObjectURL(blob));
			 this.worker = worker;
			  this.worker.addEventListener('message', function(e) {
				var data = e.data;
				if (data.consoleLog){
				  console.log(data.consoleLog);
				  return;
				};
        
			onPictureDecoded.call(self, new Uint8Array(data.buf, 0, data.length), data.width, data.height, data.infos);
        
			}, false);
      
		  this.worker.postMessage({type: "init", options: {
			rgb: !webgl,
			memsize: this.memsize,
			reuseMemory: this._config.reuseMemory ? true : false
		  }});
		
	  
	  
	  
	  
	  
      if (this._config.transferMemory){
        this.decode = function(parData, parInfo){
          // no copy
          // instead we are transfering the ownership of the buffer
          // dangerous!!!
          
          this.worker.postMessage({buf: parData.buffer, offset: parData.byteOffset, length: parData.length, info: parInfo}, [parData.buffer]); // Send data to our worker.
        };
        
      }else{
        this.decode = function(parData, parInfo){
          // Copy the sample so that we only do a structured clone of the
          // region of interest
          var copyU8 = new Uint8Array(parData.length);
          copyU8.set( parData, 0, parData.length );

          this.worker.postMessage({buf: copyU8.buffer, offset: 0, length: parData.length, info: parInfo}, [copyU8.buffer]); // Send data to our worker.
        };
        
      };
      
      if (this._config.reuseMemory){
        this.recycleMemory = function(parArray){
          //this.beforeRecycle();
         // worker.postMessage({reuse: parArray.buffer}, [parArray.buffer]); // Send data to our worker.
          //this.afterRecycle();
        };
      }
      } );
      
    }else{
      
      this.decoder = new Decoder({
        rgb: !webgl
      });
    //  this.decoder.onPictureDecoded = onPictureDecoded;

      this.decode = function(parData, parInfo){
		  
        self.decoder.decode(parData, parInfo);
      };
      
    };
    
    
    
    if (this.render){
      this.canvasObj = this.createCanvasObj({
        contextOptions: this._config.contextOptions
      });
      this.canvas = this.canvasObj.canvas;
    };

    this.domNode = this.canvas;
    
    lastWidth = this._config.size.width;
    lastHeight = this._config.size.height;
    
  };
  
  Player.prototype = {
    
    onPictureDecoded: function(buffer, width, height, infos){},
    
    // call when memory of decoded frames is not used anymore
    recycleMemory: function(buf){
    },
	
	//renderFrame: function(buffer, width, height, infos) { this.renderFrame(buffer, width, height, infos);},
    /*beforeRecycle: function(){},
    afterRecycle: function(){},*/
    
    // for both functions options is:
    //
    //  width
    //  height
    //  enableScreenshot
    //
    // returns a object that has a property canvas which is a html5 canvas
    createCanvasWebGL: function(options){
      var canvasObj = this._createBasicCanvasObj(options);
      canvasObj.contextOptions = options.contextOptions;
      return canvasObj;
    },
    
    createCanvasRGB: function(options){
      var canvasObj = this._createBasicCanvasObj(options);
      return canvasObj;
    },
    
    // part that is the same for webGL and RGB
    _createBasicCanvasObj: function(options){
      options = options || {};
      
      var obj = {};
      var width = options.width;
      if (!width){
        width = this._config.size.width;
      };
      var height = options.height;
      if (!height){
        height = this._config.size.height;
      };
      obj.canvas = document.createElement('canvas');
      obj.canvas.width = width;
      obj.canvas.height = height;
      obj.canvas.style.backgroundColor = "#0D0E1B";
      
      
      return obj;
    },
    
    // options:
    //
    // canvas
    // data
    renderFrameWebGL: function(options){
      
      var canvasObj = options.canvasObj;
      
      var width = options.width || canvasObj.canvas.width;
      var height = options.height || canvasObj.canvas.height;
      
      if (canvasObj.canvas.width !== width || canvasObj.canvas.height !== height || !canvasObj.webGLCanvas){
        canvasObj.canvas.width = width;
        canvasObj.canvas.height = height;
        canvasObj.webGLCanvas = new WebGLCanvas({
          canvas: canvasObj.canvas,
          contextOptions: canvasObj.contextOptions,
          width: width,
          height: height
        });
      };
      
      var ylen = width * height;
      var uvlen = (width / 2) * (height / 2);
      
        if(options.data !== undefined && options.data !== null){
            canvasObj.webGLCanvas.drawNextOutputPicture({
                yData: options.data.subarray(0, ylen),
                uData: options.data.subarray(ylen, ylen + uvlen),
                vData: options.data.subarray(ylen + uvlen, ylen + uvlen + uvlen)
            });
        }
      
      var self = this;
      self.recycleMemory(options.data);
      
    },
    renderFrameRGB: function(options){
      var canvasObj = options.canvasObj;

      var width = options.width || canvasObj.canvas.width;
      var height = options.height || canvasObj.canvas.height;
      
      if (canvasObj.canvas.width !== width || canvasObj.canvas.height !== height){
        canvasObj.canvas.width = width;
        canvasObj.canvas.height = height;
      };
      
      var ctx = canvasObj.ctx;
      var imgData = canvasObj.imgData;

      if (!ctx){
        canvasObj.ctx = canvasObj.canvas.getContext('2d');
        ctx = canvasObj.ctx;

        canvasObj.imgData = ctx.createImageData(width, height);
        imgData = canvasObj.imgData;
      };

      imgData.data.set(options.data);
      ctx.putImageData(imgData, 0, 0);
      var self = this;
      self.recycleMemory(options.data);
      
    }
    
  };
  
  return Player;
  
}));


'use strict';

var Stream = (function stream() {
  function constructor(url) {
    this.url = url;
  }
  
  constructor.prototype = {
    readAll: function(progress, complete) {
      var xhr = new XMLHttpRequest();
      var async = true;
      xhr.open("GET", this.url, async);
      xhr.responseType = "arraybuffer";
      if (progress) {
        xhr.onprogress = function (event) {
          progress(xhr.response, event.loaded, event.total);
        };
      }
      xhr.onreadystatechange = function (event) {
        if (xhr.readyState === 4) {
          complete(xhr.response);
          // var byteArray = new Uint8Array(xhr.response);
          // var array = Array.prototype.slice.apply(byteArray);
          // complete(array);
        }
      }
      xhr.send(null);
    }
  };
  return constructor;
})();



'use strict';


function assert(condition, message) {
  if (!condition) {
    error(message);
  }
};


/**
 * Represents a 2-dimensional size value. 
 */
var Size = (function size() {
  function constructor(w, h) {
    this.w = w;
    this.h = h;
  }
  constructor.prototype = {
    toString: function () {
      return "(" + this.w + ", " + this.h + ")";
    },
    getHalfSize: function() {
      return new Size(this.w >>> 1, this.h >>> 1);
    },
    length: function() {
      return this.w * this.h;
    }
  };
  return constructor;
})();





var Bytestream = (function BytestreamClosure() {
  function constructor(arrayBuffer, start, length) {
    this.bytes = new Uint8Array(arrayBuffer);
    this.start = start || 0;
    this.pos = this.start;
    this.end = (start + length) || this.bytes.length;
  }
  constructor.prototype = {
    get length() {
      return this.end - this.start;
    },
    get position() {
      return this.pos;
    },
    get remaining() {
      return this.end - this.pos;
    },
    readU8Array: function (length) {
      if (this.pos > this.end - length)
        return null;
      var res = this.bytes.subarray(this.pos, this.pos + length);
      this.pos += length;
      return res;
    },
    readU32Array: function (rows, cols, names) {
      cols = cols || 1;
      if (this.pos > this.end - (rows * cols) * 4)
        return null;
      if (cols == 1) {
        var array = new Uint32Array(rows);
        for (var i = 0; i < rows; i++) {
          array[i] = this.readU32();
        }
        return array;
      } else {
        var array = new Array(rows);
        for (var i = 0; i < rows; i++) {
          var row = null;
          if (names) {
            row = {};
            for (var j = 0; j < cols; j++) {
              row[names[j]] = this.readU32();
            }
          } else {
            row = new Uint32Array(cols);
            for (var j = 0; j < cols; j++) {
              row[j] = this.readU32();
            }
          }
          array[i] = row;
        }
        return array;
      }
    },
    read8: function () {
      return this.readU8() << 24 >> 24;
    },
    readU8: function () {
      if (this.pos >= this.end)
        return null;
      return this.bytes[this.pos++];
    },
    read16: function () {
      return this.readU16() << 16 >> 16;
    },
    readU16: function () {
      if (this.pos >= this.end - 1)
        return null;
      var res = this.bytes[this.pos + 0] << 8 | this.bytes[this.pos + 1];
      this.pos += 2;
      return res;
    },
    read24: function () {
      return this.readU24() << 8 >> 8;
    },
    readU24: function () {
      var pos = this.pos;
      var bytes = this.bytes;
      if (pos > this.end - 3)
        return null;
      var res = bytes[pos + 0] << 16 | bytes[pos + 1] << 8 | bytes[pos + 2];
      this.pos += 3;
      return res;
    },
    peek32: function (advance) {
      var pos = this.pos;
      var bytes = this.bytes;
      if (pos > this.end - 4)
        return null;
      var res = bytes[pos + 0] << 24 | bytes[pos + 1] << 16 | bytes[pos + 2] << 8 | bytes[pos + 3];
      if (advance) {
        this.pos += 4;
      }
      return res;
    },
    read32: function () {
      return this.peek32(true);
    },
    readU32: function () {
      return this.peek32(true) >>> 0;
    },
    read4CC: function () {
      var pos = this.pos;
      if (pos > this.end - 4)
        return null;
      var res = "";
      for (var i = 0; i < 4; i++) {
        res += String.fromCharCode(this.bytes[pos + i]);
      }
      this.pos += 4;
      return res;
    },
    readFP16: function () {
      return this.read32() / 65536;
    },
    readFP8: function () {
      return this.read16() / 256;
    },
    readISO639: function () {
      var bits = this.readU16();
      var res = "";
      for (var i = 0; i < 3; i++) {
        var c = (bits >>> (2 - i) * 5) & 0x1f;
        res += String.fromCharCode(c + 0x60);
      }
      return res;
    },
    readUTF8: function (length) {
      var res = "";
      for (var i = 0; i < length; i++) {
        res += String.fromCharCode(this.readU8());
      }
      return res;
    },
    readPString: function (max) {
      var len = this.readU8();
      assert (len <= max);
      var res = this.readUTF8(len);
      this.reserved(max - len - 1, 0);
      return res;
    },
    skip: function (length) {
      this.seek(this.pos + length);
    },
    reserved: function (length, value) {
      for (var i = 0; i < length; i++) {
        assert (this.readU8() == value);
      }
    },
    seek: function (index) {
      if (index < 0 || index > this.end) {
        error("Index out of bounds (bounds: [0, " + this.end + "], index: " + index + ").");
      }
      this.pos = index;
    },
    subStream: function (start, length) {
      return new Bytestream(this.bytes.buffer, start, length);
    }
  };
  return constructor;
})();


var PARANOID = true; // Heavy-weight assertions.

/**
 * Reads an mp4 file and constructs a object graph that corresponds to the box/atom
 * structure of the file. Mp4 files are based on the ISO Base Media format, which in
 * turn is based on the Apple Quicktime format. The Quicktime spec is available at:
 * http://developer.apple.com/library/mac/#documentation/QuickTime/QTFF. An mp4 spec
 * also exists, but I cannot find it freely available.
 *
 * Mp4 files contain a tree of boxes (or atoms in Quicktime). The general structure
 * is as follows (in a pseudo regex syntax):
 *
 * Box / Atom Structure:
 *
 * [size type [version flags] field* box*]
 *  <32> <4C>  <--8--> <24->  <-?->  <?>
 *  <------------- box size ------------>
 *
 *  The box size indicates the entire size of the box and its children, we can use it
 *  to skip over boxes that are of no interest. Each box has a type indicated by a
 *  four character code (4C), this describes how the box should be parsed and is also
 *  used as an object key name in the resulting box tree. For example, the expression:
 *  "moov.trak[0].mdia.minf" can be used to access individual boxes in the tree based
 *  on their 4C name. If two or more boxes with the same 4C name exist in a box, then
 *  an array is built with that name.
 *
 */
var MP4Reader = (function reader() {
  var BOX_HEADER_SIZE = 8;
  var FULL_BOX_HEADER_SIZE = BOX_HEADER_SIZE + 4;

  function constructor(stream) {
    this.stream = stream;
    this.tracks = {};
  }

  constructor.prototype = {
    readBoxes: function (stream, parent) {
      while (stream.peek32()) {
        var child = this.readBox(stream);
        if (child.type in parent) {
          var old = parent[child.type];
          if (!(old instanceof Array)) {
            parent[child.type] = [old];
          }
          parent[child.type].push(child);
        } else {
          parent[child.type] = child;
        }
      }
    },
    readBox: function readBox(stream) {
      var box = { offset: stream.position };

      function readHeader() {
        box.size = stream.readU32();
        box.type = stream.read4CC();
      }

      function readFullHeader() {
        box.version = stream.readU8();
        box.flags = stream.readU24();
      }

      function remainingBytes() {
        return box.size - (stream.position - box.offset);
      }

      function skipRemainingBytes () {
        stream.skip(remainingBytes());
      }

      var readRemainingBoxes = function () {
        var subStream = stream.subStream(stream.position, remainingBytes());
        this.readBoxes(subStream, box);
        stream.skip(subStream.length);
      }.bind(this);

      readHeader();

      switch (box.type) {
        case 'ftyp':
          box.name = "File Type Box";
          box.majorBrand = stream.read4CC();
          box.minorVersion = stream.readU32();
          box.compatibleBrands = new Array((box.size - 16) / 4);
          for (var i = 0; i < box.compatibleBrands.length; i++) {
            box.compatibleBrands[i] = stream.read4CC();
          }
          break;
        case 'moov':
          box.name = "Movie Box";
          readRemainingBoxes();
          break;
        case 'mvhd':
          box.name = "Movie Header Box";
          readFullHeader();
          assert (box.version == 0);
          box.creationTime = stream.readU32();
          box.modificationTime = stream.readU32();
          box.timeScale = stream.readU32();
          box.duration = stream.readU32();
          box.rate = stream.readFP16();
          box.volume = stream.readFP8();
          stream.skip(10);
          box.matrix = stream.readU32Array(9);
          stream.skip(6 * 4);
          box.nextTrackId = stream.readU32();
          break;
        case 'trak':
          box.name = "Track Box";
          readRemainingBoxes();
          this.tracks[box.tkhd.trackId] = new Track(this, box);
          break;
        case 'tkhd':
          box.name = "Track Header Box";
          readFullHeader();
          assert (box.version == 0);
          box.creationTime = stream.readU32();
          box.modificationTime = stream.readU32();
          box.trackId = stream.readU32();
          stream.skip(4);
          box.duration = stream.readU32();
          stream.skip(8);
          box.layer = stream.readU16();
          box.alternateGroup = stream.readU16();
          box.volume = stream.readFP8();
          stream.skip(2);
          box.matrix = stream.readU32Array(9);
          box.width = stream.readFP16();
          box.height = stream.readFP16();
          break;
        case 'mdia':
          box.name = "Media Box";
          readRemainingBoxes();
          break;
        case 'mdhd':
          box.name = "Media Header Box";
          readFullHeader();
          assert (box.version == 0);
          box.creationTime = stream.readU32();
          box.modificationTime = stream.readU32();
          box.timeScale = stream.readU32();
          box.duration = stream.readU32();
          box.language = stream.readISO639();
          stream.skip(2);
          break;
        case 'hdlr':
          box.name = "Handler Reference Box";
          readFullHeader();
          stream.skip(4);
          box.handlerType = stream.read4CC();
          stream.skip(4 * 3);
          var bytesLeft = box.size - 32;
          if (bytesLeft > 0) {
            box.name = stream.readUTF8(bytesLeft);
          }
          break;
        case 'minf':
          box.name = "Media Information Box";
          readRemainingBoxes();
          break;
        case 'stbl':
          box.name = "Sample Table Box";
          readRemainingBoxes();
          break;
        case 'stsd':
          box.name = "Sample Description Box";
          readFullHeader();
          box.sd = [];
          var entries = stream.readU32();
          readRemainingBoxes();
          break;
        case 'avc1':
          stream.reserved(6, 0);
          box.dataReferenceIndex = stream.readU16();
          assert (stream.readU16() == 0); // Version
          assert (stream.readU16() == 0); // Revision Level
          stream.readU32(); // Vendor
          stream.readU32(); // Temporal Quality
          stream.readU32(); // Spatial Quality
          box.width = stream.readU16();
          box.height = stream.readU16();
          box.horizontalResolution = stream.readFP16();
          box.verticalResolution = stream.readFP16();
          assert (stream.readU32() == 0); // Reserved
          box.frameCount = stream.readU16();
          box.compressorName = stream.readPString(32);
          box.depth = stream.readU16();
          assert (stream.readU16() == 0xFFFF); // Color Table Id
          readRemainingBoxes();
          break;
        case 'mp4a':
          stream.reserved(6, 0);
          box.dataReferenceIndex = stream.readU16();
          box.version = stream.readU16();
          stream.skip(2);
          stream.skip(4);
          box.channelCount = stream.readU16();
          box.sampleSize = stream.readU16();
          box.compressionId = stream.readU16();
          box.packetSize = stream.readU16();
          box.sampleRate = stream.readU32() >>> 16;

          // TODO: Parse other version levels.
          assert (box.version == 0);
          readRemainingBoxes();
          break;
        case 'esds':
          box.name = "Elementary Stream Descriptor";
          readFullHeader();
          // TODO: Do we really need to parse this?
          skipRemainingBytes();
          break;
        case 'avcC':
          box.name = "AVC Configuration Box";
          box.configurationVersion = stream.readU8();
          box.avcProfileIndicaation = stream.readU8();
          box.profileCompatibility = stream.readU8();
          box.avcLevelIndication = stream.readU8();
          box.lengthSizeMinusOne = stream.readU8() & 3;
          assert (box.lengthSizeMinusOne == 3, "TODO");
          var count = stream.readU8() & 31;
          box.sps = [];
          for (var i = 0; i < count; i++) {
            box.sps.push(stream.readU8Array(stream.readU16()));
          }
          var count = stream.readU8() & 31;
          box.pps = [];
          for (var i = 0; i < count; i++) {
            box.pps.push(stream.readU8Array(stream.readU16()));
          }
          skipRemainingBytes();
          break;
        case 'btrt':
          box.name = "Bit Rate Box";
          box.bufferSizeDb = stream.readU32();
          box.maxBitrate = stream.readU32();
          box.avgBitrate = stream.readU32();
          break;
        case 'stts':
          box.name = "Decoding Time to Sample Box";
          readFullHeader();
          box.table = stream.readU32Array(stream.readU32(), 2, ["count", "delta"]);
          break;
        case 'stss':
          box.name = "Sync Sample Box";
          readFullHeader();
          box.samples = stream.readU32Array(stream.readU32());
          break;
        case 'stsc':
          box.name = "Sample to Chunk Box";
          readFullHeader();
          box.table = stream.readU32Array(stream.readU32(), 3,
            ["firstChunk", "samplesPerChunk", "sampleDescriptionId"]);
          break;
        case 'stsz':
          box.name = "Sample Size Box";
          readFullHeader();
          box.sampleSize = stream.readU32();
          var count = stream.readU32();
          if (box.sampleSize == 0) {
            box.table = stream.readU32Array(count);
          }
          break;
        case 'stco':
          box.name = "Chunk Offset Box";
          readFullHeader();
          box.table = stream.readU32Array(stream.readU32());
          break;
        case 'smhd':
          box.name = "Sound Media Header Box";
          readFullHeader();
          box.balance = stream.readFP8();
          stream.reserved(2, 0);
          break;
        case 'mdat':
          box.name = "Media Data Box";
          assert (box.size >= 8, "Cannot parse large media data yet.");
          box.data = stream.readU8Array(remainingBytes());
          break;
        default:
          skipRemainingBytes();
          break;
      };
      return box;
    },
    read: function () {
      var start = (new Date).getTime();
      this.file = {};
      this.readBoxes(this.stream, this.file);
      //console.info("Parsed stream in " + ((new Date).getTime() - start) + " ms");
    },
    traceSamples: function () {
      var video = this.tracks[1];
      var audio = this.tracks[2];

      //console.info("Video Samples: " + video.getSampleCount());
      //console.info("Audio Samples: " + audio.getSampleCount());

      var vi = 0;
      var ai = 0;

      for (var i = 0; i < 100; i++) {
        var vo = video.sampleToOffset(vi);
        var ao = audio.sampleToOffset(ai);

        var vs = video.sampleToSize(vi, 1);
        var as = audio.sampleToSize(ai, 1);

        if (vo < ao) {
          //console.info("V Sample " + vi + " Offset : " + vo + ", Size : " + vs);
          vi ++;
        } else {
          //console.info("A Sample " + ai + " Offset : " + ao + ", Size : " + as);
          ai ++;
        }
      }
    }
  };
  return constructor;
})();

var Track = (function track () {
  function constructor(file, trak) {
    this.file = file;
    this.trak = trak;
  }

  constructor.prototype = {
    getSampleSizeTable: function () {
      return this.trak.mdia.minf.stbl.stsz.table;
    },
    getSampleCount: function () {
      return this.getSampleSizeTable().length;
    },
    /**
     * Computes the size of a range of samples, returns zero if length is zero.
     */
    sampleToSize: function (start, length) {
      var table = this.getSampleSizeTable();
      var size = 0;
      for (var i = start; i < start + length; i++) {
        size += table[i];
      }
      return size;
    },
    /**
     * Computes the chunk that contains the specified sample, as well as the offset of
     * the sample in the computed chunk.
     */
    sampleToChunk: function (sample) {

      /* Samples are grouped in chunks which may contain a variable number of samples.
       * The sample-to-chunk table in the stsc box describes how samples are arranged
       * in chunks. Each table row corresponds to a set of consecutive chunks with the
       * same number of samples and description ids. For example, the following table:
       *
       * +-------------+-------------------+----------------------+
       * | firstChunk  |  samplesPerChunk  |  sampleDescriptionId |
       * +-------------+-------------------+----------------------+
       * | 1           |  3                |  23                  |
       * | 3           |  1                |  23                  |
       * | 5           |  1                |  24                  |
       * +-------------+-------------------+----------------------+
       *
       * describes 5 chunks with a total of (2 * 3) + (2 * 1) + (1 * 1) = 9 samples,
       * each chunk containing samples 3, 3, 1, 1, 1 in chunk order, or
       * chunks 1, 1, 1, 2, 2, 2, 3, 4, 5 in sample order.
       *
       * This function determines the chunk that contains a specified sample by iterating
       * over every entry in the table. It also returns the position of the sample in the
       * chunk which can be used to compute the sample's exact position in the file.
       *
       * TODO: Determine if we should memoize this function.
       */

      var table = this.trak.mdia.minf.stbl.stsc.table;

      if (table.length === 1) {
        var row = table[0];
        assert (row.firstChunk === 1);
        return {
          index: Math.floor(sample / row.samplesPerChunk),
          offset: sample % row.samplesPerChunk
        };
      }

      var totalChunkCount = 0;
      for (var i = 0; i < table.length; i++) {
        var row = table[i];
        if (i > 0) {
          var previousRow = table[i - 1];
          var previousChunkCount = row.firstChunk - previousRow.firstChunk;
          var previousSampleCount = previousRow.samplesPerChunk * previousChunkCount;
          if (sample >= previousSampleCount) {
            sample -= previousSampleCount;
            if (i == table.length - 1) {
              return {
                index: totalChunkCount + previousChunkCount + Math.floor(sample / row.samplesPerChunk),
                offset: sample % row.samplesPerChunk
              };
            }
          } else {
            return {
              index: totalChunkCount + Math.floor(sample / previousRow.samplesPerChunk),
              offset: sample % previousRow.samplesPerChunk
            };
          }
          totalChunkCount += previousChunkCount;
        }
      }
      assert(false);
    },
    chunkToOffset: function (chunk) {
      var table = this.trak.mdia.minf.stbl.stco.table;
      return table[chunk];
    },
    sampleToOffset: function (sample) {
      var res = this.sampleToChunk(sample);
      var offset = this.chunkToOffset(res.index);
      return offset + this.sampleToSize(sample - res.offset, res.offset);
    },
    /**
     * Computes the sample at the specified time.
     */
    timeToSample: function (time) {
      /* In the time-to-sample table samples are grouped by their duration. The count field
       * indicates the number of consecutive samples that have the same duration. For example,
       * the following table:
       *
       * +-------+-------+
       * | count | delta |
       * +-------+-------+
       * |   4   |   3   |
       * |   2   |   1   |
       * |   3   |   2   |
       * +-------+-------+
       *
       * describes 9 samples with a total time of (4 * 3) + (2 * 1) + (3 * 2) = 20.
       *
       * This function determines the sample at the specified time by iterating over every
       * entry in the table.
       *
       * TODO: Determine if we should memoize this function.
       */
      var table = this.trak.mdia.minf.stbl.stts.table;
      var sample = 0;
      for (var i = 0; i < table.length; i++) {
        var delta = table[i].count * table[i].delta;
        if (time >= delta) {
          time -= delta;
          sample += table[i].count;
        } else {
          return sample + Math.floor(time / table[i].delta);
        }
      }
    },
    /**
     * Gets the total time of the track.
     */
    getTotalTime: function () {
      if (PARANOID) {
        var table = this.trak.mdia.minf.stbl.stts.table;
        var duration = 0;
        for (var i = 0; i < table.length; i++) {
          duration += table[i].count * table[i].delta;
        }
        assert (this.trak.mdia.mdhd.duration == duration);
      }
      return this.trak.mdia.mdhd.duration;
    },
    getTotalTimeInSeconds: function () {
      return this.timeToSeconds(this.getTotalTime());
    },
    getTimeScale: function () {
      return this.trak.mdia.mdhd.timeScale;
    },
    /**
     * Converts time units to real time (seconds).
     */
    timeToSeconds: function (time) {
      return time / this.getTimeScale();
    },
    /**
     * Converts real time (seconds) to time units.
     */
    secondsToTime: function (seconds) {
      return seconds * this.getTimeScale();
    },
    foo: function () {
      /*
      for (var i = 0; i < this.getSampleCount(); i++) {
        var res = this.sampleToChunk(i);
        //console.info("Sample " + i + " -> " + res.index + " % " + res.offset +
                     " @ " + this.chunkToOffset(res.index) +
                     " @@ " + this.sampleToOffset(i));
      }
      //console.info("Total Time: " + this.timeToSeconds(this.getTotalTime()));
      var total = this.getTotalTimeInSeconds();
      for (var i = 50; i < total; i += 0.1) {
        // console.info("Time: " + i.toFixed(2) + " " + this.secondsToTime(i));

        //console.info("Time: " + i.toFixed(2) + " " + this.timeToSample(this.secondsToTime(i)));
      }
      */
    },
    /**
     * AVC samples contain one or more NAL units each of which have a length prefix.
     * This function returns an array of NAL units without their length prefixes.
     */
    getSampleNALUnits: function (sample) {
      var bytes = this.file.stream.bytes;
      var offset = this.sampleToOffset(sample);
      var end = offset + this.sampleToSize(sample, 1);
      var nalUnits = [];
      while(end - offset > 0) {
        var length = (new Bytestream(bytes.buffer, offset)).readU32();
        nalUnits.push(bytes.subarray(offset + 4, offset + length + 4));
        offset = offset + length + 4;
      }
      return nalUnits;
    }
  };
  return constructor;
})();


// Only add setZeroTimeout to the window object, and hide everything
// else in a closure. (http://dbaron.org/log/20100309-faster-timeouts)
(function() {
    var timeouts = [];
    var messageName = "zero-timeout-message";

    // Like setTimeout, but only takes a function argument.  There's
    // no time argument (always zero) and no arguments (you have to
    // use a closure).
    function setZeroTimeout(fn) {
        timeouts.push(fn);
        window.postMessage(messageName, "*");
    }

    function handleMessage(event) {
        if (event.source == window && event.data == messageName) {
            event.stopPropagation();
            if (timeouts.length > 0) {
                var fn = timeouts.shift();
                fn();
            }
        }
    }

    window.addEventListener("message", handleMessage, true);

    // Add the one thing we want added to the window object.
    window.setZeroTimeout = setZeroTimeout;
})();

var MP4Player = (function reader() {
  var defaultConfig = {
    filter: "original",
    filterHorLuma: "optimized",
    filterVerLumaEdge: "optimized",
    getBoundaryStrengthsA: "optimized"
  };

  function constructor(stream, useWorkers, webgl, render) {
    this.stream = stream;
    this.useWorkers = useWorkers;
    this.webgl = webgl;
    this.render = render;

    this.statistics = {
      videoStartTime: 0,
      videoPictureCounter: 0,
      windowStartTime: 0,
      windowPictureCounter: 0,
      fps: 0,
      fpsMin: 1000,
      fpsMax: -1000,
      webGLTextureUploadTime: 0
    };

    this.onStatisticsUpdated = function () {};

    this.avc = new Player({
      useWorker: useWorkers,
      reuseMemory: true,
      webgl: webgl,
      size: {
        width: 640,
        height: 368
      }
    });
    
    this.webgl = this.avc.webgl;
    
    var self = this;
    //this.avc.onPictureDecoded = function(){
     // updateStatistics.call(self);
    //};
    
    this.canvas = this.avc.canvas;
  }

  function updateStatistics() {
    var s = this.statistics;
    s.videoPictureCounter += 1;
    s.windowPictureCounter += 1;
    var now = Date.now();
    if (!s.videoStartTime) {
      s.videoStartTime = now;
    }
    var videoElapsedTime = now - s.videoStartTime;
    s.elapsed = videoElapsedTime / 1000;
    if (videoElapsedTime < 1000) {
      return;
    }

    if (!s.windowStartTime) {
      s.windowStartTime = now;
      return;
    } else if ((now - s.windowStartTime) > 1000) {
      var windowElapsedTime = now - s.windowStartTime;
      var fps = (s.windowPictureCounter / windowElapsedTime) * 1000;
      s.windowStartTime = now;
      s.windowPictureCounter = 0;

      if (fps < s.fpsMin) s.fpsMin = fps;
      if (fps > s.fpsMax) s.fpsMax = fps;
      s.fps = fps;
    }

    var fps = (s.videoPictureCounter / videoElapsedTime) * 1000;
    s.fpsSinceStart = fps;
    this.onStatisticsUpdated(this.statistics);
    return;
  }

  constructor.prototype = {
    readAll: function(callback) {
		
      //console.info("MP4Player::readAll()");
      this.stream.readAll(null, function (buffer) {
        this.reader = new MP4Reader(new Bytestream(buffer));
        this.reader.read();
        var video = this.reader.tracks[1];
        this.size = new Size(video.trak.tkhd.width, video.trak.tkhd.height);
        //console.info("MP4Player::readAll(), length: " +  this.reader.stream.length);
        if (callback) callback();
      }.bind(this));
    },
	
	avc: function() { return this.avc;},
	
	video: function() { return this.reader.tracks[1]},
	
	audio: function() { return this.reader.tracks[2]},
	
    play: function() {
      var reader = this.reader;

      if (!reader) {
        this.readAll(this.play.bind(this));
        return;
      };

      var video = reader.tracks[1];
	  
	  //this.video = video;
	  
      var audio = reader.tracks[2];
	  //this.audio = audio;
	  
      var avc = reader.tracks[1].trak.mdia.minf.stbl.stsd.avc1.avcC;
      var sps = avc.sps[0];
      var pps = avc.pps[0];

      /* Decode Sequence & Picture Parameter Sets */
      this.avc.decode(sps);
      this.avc.decode(pps);

      /* Decode Pictures */
      var pic = 0;
      setTimeout(function foo() {
        var avc = this.avc;
        video.getSampleNALUnits(pic).forEach(function (nal) {
          avc.decode(nal);
        });
        pic ++;
        if (pic < 3000) {
          setTimeout(foo.bind(this), 1);
        };
      }.bind(this), 1);
    }
  };

  return constructor;
})();

var avFileSRC;
var BWC = (function bw() {
  function constructor(div) {
    var src = div.attributes.src ? div.attributes.src.value : undefined;
	avFileSRC = src;
      
    var width = div.attributes.width ? div.attributes.width.value : 1024;
    var height = div.attributes.height ? div.attributes.height.value : 768;

    var useWorkers = div.attributes.workers ? div.attributes.workers.value == "true" : false;
    var render = div.attributes.render ? div.attributes.render.value == "true" : false;
    
    var webgl = "auto";
    if (div.attributes.webgl){
      if (div.attributes.webgl.value == "true"){
        webgl = true;
      };
      if (div.attributes.webgl.value == "false"){
        webgl = false;
      };
    };

    this.player = new MP4Player(new Stream(src), useWorkers, webgl, render);
    this.canvas = this.player.canvas;
   
    this.score = null;
    this.player.onStatisticsUpdated = function (statistics) {
      if (statistics.videoPictureCounter % 10 != 0) {
        return;
      }
      var info = "";
      if (statistics.fps) {
        info += " fps: " + statistics.fps.toFixed(2);
      }
      if (statistics.fpsSinceStart) {
        info += " avg: " + statistics.fpsSinceStart.toFixed(2);
      }
      var scoreCutoff = 1200;
      if (statistics.videoPictureCounter < scoreCutoff) {
        this.score = scoreCutoff - statistics.videoPictureCounter;
      } else if (statistics.videoPictureCounter == scoreCutoff) {
        this.score = statistics.fpsSinceStart.toFixed(2);
      }
      // info += " score: " + this.score;

      this.info.innerHTML = infoStr + info;
    }.bind(this);
  }
  constructor.prototype = {
    play: function () {
      this.player.play();
    }
  };
  return constructor;
})();

/*jslint browser: true, debug: true, plusplus: true, bitwise: true*/
/*global $AM, console*/

var AMUtils = {
    AMCV_VERSION: "1.0.00.000",
    artiDebug: false, //TODOPROD: comment out this line on release!!
    AMTALOG_LOG: console.log,
    AMTALOG_INFO: console.info,
    AMTALOG_ERROR: console.error,
    AMTALOG_WARN: console.warn,
    //loadingClipURL: "http://akamai.advsnx.net/CDN/ta-extra/artisdk.mp4",

    getQS: function (name) {
        "use strict";
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results;

        results = regex.exec(this.getTopmostLocation());

        return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    getTopmostLocation: function () {
        "use strict";
        var result;
        try {
            result = parent.location.href;
        } catch (e) {
            result = location.href;
        }
        if (!AMUtils.hasValue(result)) {
            return location.href;
        }
        return result;
    },

    logAMTA: function () {
        "use strict";

        if ((this.getQS('artiLog') !== null || this.getQS('artiQA') !== null) || this.artiDebug === true) {
            var argArr = $AM.makeArray(arguments),
                method = this.AMTALOG_LOG;

            if (typeof argArr[0] === "function") {
                method = argArr.shift();
            }

            argArr.unshift("AMTA: ");

            method.apply(console, argArr);
        }
    },

    hasValue: function (str) {
        "use strict";
        if (str !== undefined && str !== null && str !== "") {
            return true;
        } else {
            return false;
        }
    },

    getTime: function (totalTime) {
        "use strict";
        if (totalTime < 0) {
            return "00:00";
        }

        totalTime = parseInt(totalTime, 10);

        var seconds = totalTime % 60,
            minutes = (totalTime - seconds) / 60,
            output;

        if (minutes >= 10) {
            output = minutes.toString();
        } else {
            output = "0" + minutes;
        }
        output += ":";
        if (seconds >= 10) {
            output += seconds;
        } else {
            output += "0" + seconds;
        }

        return output;
    }
};


var CanvasVideoEvent = {
    READY: "ready",
    PLAYING: "playing",
    PLAY: "play",
    PAUSE: "pause",
    TIME_UPDATE: "timeupdate",
    ENDED: "ended",
    ERROR: "error",
    LOADED_METADATA: "loadedmetadata"
};

var CanvasVideo = function (canvasObject, autoPlay) {
    "use strict";
    AMUtils.logAMTA("Starting...");
    this.frameBuffers.push([]);
    this.frameBuffers.push([]);
    this.autoPlay = autoPlay;
    this.bw = new BWC(canvasObject);

    this.targetObj = {};
    this.targetObj.canvas = canvasObject;
};

CanvasVideo.prototype = {
    //Variables
    ready: false,
    bw: null,
    targetObj: null,
    bufferLength: 2,
    frameBuffers: [],
    framesLoaded: 0,
    currentFrame: 0,
    currentTime: 0,
    duration: 0,
    bufferTime: 0,
    isPlaying: null,
    isFilling: false,
    lastFrame: 0,
    vW: 0,
    vH: 0,
    FPS: 0,
    aFPS: 0,
    autoPlay: false,
    videoWidth: 0,
    videoHeight: 0,
    //Methods
    init: function() {
        var that = this;
        this.bw.player.readAll(function () {
            that.startBuffer();
        });
    },
        
    onloadedmetadata: function () {
        "use strict";
    },

    onplaying: function () {
        "use strict";
    },

    ontimeupdate: function () {
        "use strict";
    },

    onended: function () {
        "use strict";
    },

    onpause: function () {
        "use strict";
    },

    startBuffer: function () {
        "use strict";

        AMUtils.logAMTA("start buffer");
        var that = this,
            fIndex = 0;

        this.bw.player.avc.onPictureDecoded = function (e, w, h) {
            var i;
            that.vW = w;
            that.vH = h;

            var bID = Math.floor(((that.framesLoaded) % (that.bufferLength * that.FPS * 2)) / (that.bufferLength * that.FPS)) % 2;

            that.frameBuffers[bID][that.framesLoaded % (that.bufferLength * that.FPS)] = e;

            if (that.framesLoaded >= that.framesToLoad) {
                if (!that.isPlaying) {
                    that.ready = true;
                    $AM(that).trigger(CanvasVideoEvent.READY);
                    if (that.autoPlay === true) {
                        that.play();
                    }
                }
                that.isFilling = false;
            } else {

                for (i = 0; i < that.FPS; i++) {
                    window.requestAnimationFrame(that.requestAnimationFrameFunction.bind(that));
                }
            }
            that.framesLoaded++;
        };
        // Decode headers
        this.bw.player.avc.decode(this.bw.player.video().trak.mdia.minf.stbl.stsd.avc1.avcC.sps[0]);
        this.bw.player.avc.decode(this.bw.player.video().trak.mdia.minf.stbl.stsd.avc1.avcC.pps[0]);

        // Get total frames count
        this.totalFrames = this.bw.player.video().trak.mdia.minf.stbl.stsz.table.length;
        this.duration = (this.bw.player.video().trak.tkhd.duration / 1000);
        this.FPS = (this.totalFrames / this.duration);
        this.videoWidth = this.bw.player.size.w;
        this.videoHeight = this.bw.player.size.h;
        this.onloadedmetadata({
            type: "loadedmetadata"
        });
        AMUtils.logAMTA("total frames: " + this.totalFrames + ", total time (s): " + this.duration + ", fps: " + this.FPS);

        // prepare the buffer arrays
        for (fIndex = 0; fIndex < this.bufferLength * this.FPS; fIndex++) {
            this.frameBuffers[0].push(0);
            this.frameBuffers[1].push(0);
        }

        this.fillBuffer();

    },

    requestAnimationFrameFunction: function () {
        "use strict";
        var that = this;
        this.bw.player.video().getSampleNALUnits(this.framesLoaded).forEach(function (nal) {
            that.bw.player.avc.decode(nal);
        });
    },

    fillBuffer: function () {
        "use strict";
        var that = this;
        if ((!this.isFilling) && ((this.framesLoaded - this.currentFrame) <= (this.FPS * this.bufferLength))) {
            this.isFilling = true;

            if (this.framesLoaded >= this.totalFrames) {
                this.framesLoaded = 0;
            }

            this.framesToLoad = this.framesLoaded + (this.bufferLength * this.FPS);
            AMUtils.logAMTA("Refilling buffer...");
            this.bw.player.video().getSampleNALUnits(this.framesLoaded).forEach(function (nal) {
                that.bw.player.avc.decode(nal);
            });
        }
    },
    
    audioElem: null,
    audioSource: null,

    play: function () {
        "use strict";
        var i = 0,
            that = this;
        if (this.isPlaying === null) {
            /*if(!this.audioElem){
                this.audioElem = $AM("#iScrollAudioTrack")[0];
            }*/
            ///console.log("playing first time");

            this.isPlaying = true;
			this.audioElem.oncanplay = function () {
				that.audioElem.onended = onAudioPlaybackEnded;
				that.audioElem.addEventListener("ended", that.onAudioPlaybackEnded);
				function onAudioPlaybackEnded(e) {
					that.onended({
						type: "ended"
					});
				}

				that.audioStart = 0;
				window.requestAnimationFrame(that.renderFrame.bind(that));
				
				that.onplaying({
					type: "playing"
				});
				
				that.aFPS = (that.audioElem.duration * 1000) / that.totalFrames;
				AMUtils.logAMTA("FPS ratio: " + (that.FPS / that.aFPS));
				

				setInterval(function () {
                    if(AMUtils.hasValue(that.audioElem)){
						that.currentTime = that.audioElem.currentTime;// - that.audioStart;
					}
					that.bufferTime = that.framesLoaded / that.FPS;
				}, 100);
                
                that.audioElem.play();
			}
			this.audioElem.src = avFileSRC.trim();
			this.audioElem.play();
            ///console.log("playing first time");
            /*this.isPlaying = true;

            var audioTrackBA = this.bw.player.audio().file.stream.bytes;
            var arrayBuffer = new ArrayBuffer(audioTrackBA.length);
            var bufferView = new Uint8Array(arrayBuffer);

            for (i = 0; i < audioTrackBA.length; i++) {
                bufferView[i] = audioTrackBA[i];
            }
            
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            this.audioContext = new window.AudioContext();
            this.audioContextGain = this.audioContext.createGain();
			this.audioContextGain.gain.value = 0;
            this.audioSource = this.audioContext.createBufferSource();
            
            this.audioContext.decodeAudioData(arrayBuffer, function (buffer) {
                
                that.audioSource.buffer = buffer;
                that.audioSource.connect(that.audioContextGain);
                that.audioContextGain.connect(that.audioContext.destination);
                that.audioSource.start(0);
                
                that.audioSource.onended = onAudioPlaybackEnded;
                that.audioSource.addEventListener("ended", that.onAudioPlaybackEnded);
                function onAudioPlaybackEnded(e) {
                //    that.audioContext.close();
                    that.onended({
                        type: "ended"
                    });
                }
                                
                that.audioStart = that.audioContext.currentTime;
                window.requestAnimationFrame(that.renderFrame.bind(that));
                //$AM(that).trigger(CanvasVideoEvent.PLAY);
                that.onplaying({
                    type: "playing"
                });
                that.aFPS = (that.audioSource.buffer.duration * 1000) / that.totalFrames;
                AMUtils.logAMTA("FPS ratio: " + (that.FPS / that.aFPS));
            });

            setInterval(function () {
                that.currentTime = that.audioContext.currentTime - that.audioStart;
                that.bufferTime = that.framesLoaded / that.FPS;
            }, 100);*/
        } else if (this.isPlaying === false) {
            //console.log("resuming playback");
            //this.audioContext.resume();
            this.audioElem.play();
            this.isPlaying = true;
            this.paused = false;
            //$AM(this).trigger(CanvasVideoEvent.PLAY);
        }
    },

    pause: function () {
        "use strict";
        //this.audioContext.suspend();
        this.audioElem.pause()
        this.isPlaying = false;
        this.paused = true;
        this.onpause({
            type: "pause"
        });
        //$AM(this).trigger(CanvasVideoEvent.PAUSE);
    },

    renderFrame: function () {
        "use strict";

        var cTime,
            that = this;

        cTime = ((this.audioElem.currentTime - this.audioStart) * 1000);
        
        this.currentFrame = ((cTime / this.FPS) * (this.FPS / this.aFPS)).toFixed(0);

        if (cTime < this.duration * 1000) {
            var frameID = this.currentFrame % (this.bufferLength * this.FPS);
            var bufID = Math.floor(((this.currentFrame) % (this.bufferLength * this.FPS * 2)) / (this.bufferLength * this.FPS)) % 2;
            this.bw.player.avc.renderFrame({
                canvasObj: this.targetObj,
                data: this.frameBuffers[bufID][frameID],
                width: this.vW,
                height: this.vH
            });
            //$AM(this).trigger(CanvasVideoEvent.TIME_UPDATE);
            this.ontimeupdate({
                type: "timeupdate"
            });
            this.fillBuffer();


            if (((this.framesLoaded - this.currentFrame) < (this.FPS )) && (this.framesLoaded !== this.totalFrames) ){
                if (this.isPlaying) {
                    this.pause();
                }
            } else {
                if (((this.framesLoaded - this.currentFrame) > (this.FPS * this.bufferLength / 3))) {
                    if (this.isPlaying) {
                        this.play();
                    }
                }
            }
			//if(this.currentFrame < this.totalFrames) {
				setTimeout(function () {
					window.requestAnimationFrame(that.renderFrame.bind(that));
				}, 1000 / this.FPS);
			//}
        }
        this.lastFrame = this.currentFrame;
    },

    pMuted: false,
    set muted(val) {
        "use strict";
        if (this.audioElem) {
            this.audioElem.volume = ((val === true) ? 0 : 1);
            this.pMuted = val;
        }
    },
};
