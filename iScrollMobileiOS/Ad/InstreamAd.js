/*jslint browser: true*/
/*global $AM, logAMTA, AMTALOG_LOG, jQuery, Ad, console, ArtiMediaEvents, InstreamAd*/
var InstreamAdTypes = {
	ISCROLL: "iScroll"
};

var InstreamAd = function (type, showFirstAdAfter, totalMaxAds, groupMaxAds, groupInterval) {
	"use strict";
	this.type = type;
	//logAMTA(AMTALOG_LOG, showFirstAdAfter);
	//logAMTA(AMTALOG_LOG, parseInt(showFirstAdAfter, 10));
	this.adProperties.showFirstAdAfter = parseInt(showFirstAdAfter, 10);
	this.adProperties.totalMaxAds = parseInt(totalMaxAds, 10);
	this.adProperties.groupMaxAds = parseInt(groupMaxAds, 10);
	this.adProperties.groupInterval = parseInt(groupInterval, 10);
};

/*InstreamAd.prototype = new Ad();
InstreamAd.prototype.constructor = InstreamAd;*/

//InstreamAd.prototype = new Ad();
InstreamAd.prototype = Ad.prototype;
InstreamAd.prototype.zoneTagURL = null;
InstreamAd.prototype.type = null;
InstreamAd.prototype.mediaURL = null;
InstreamAd.prototype.duration = 0;
InstreamAd.prototype.viewCount = 0;
InstreamAd.prototype.currentGroupAd = 0;
InstreamAd.prototype.groupIndex = 0;

InstreamAd.prototype.constructor = InstreamAd;

/*$AM.extend(InstreamAd.prototype, {
	zoneTagURL: null,
	type: null,
	mediaURL: null,
	duration: 0
});*/