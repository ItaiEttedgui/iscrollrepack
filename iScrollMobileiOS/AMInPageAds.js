/*jslint browser: true, plusplus: true, bitwise: true, vars: true, debug: true, regexp: true*/
/*global $AM, performance, artiMediaSiteKey, uuid, console, hasOwnProperty, artiDebug, ArtiMediaPageAds, CanvasVideo,CanvasVideoEvent, jQuery,  TagGetter, VASTer, TagGetterEvents, VASTerEvents*/

//@prepros-prepend TagGetter.js
//@prepros-prepend VASTer.js
//@prepros-prepend Ad/Ad.js
//@prepros-prepend Ad/InstreamAd.js
//@prepros-prepend CanvasVideo2.js
//@prepros-prepend ../Kjs.js
//@prepros-prepend ../CookieHelper.js

var AMTALOG_LOG = console.log;
var AMTALOG_INFO = console.info;
var AMTALOG_ERROR = console.error;
var AMTALOG_WARN = console.warn;

var QA_SERVER_PATH = "staging1.advsnx.net";
var PROD_SERVER_PATH = "lb.advsnx.net";
var AMTAH5_TITLE = "ArtiMedia iScroll Mobile HQ";
var AMTAH5_VERSION = "1.0.5.54"; //TODO: Needs to be changed

var artiDebug = false;
var artiMediaPageAds;

function getQS(name) {
    "use strict";
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function isQA() {
    "use strict";
    return ((getQS('artiLog') !== null || getQS('artiQA') !== null) || artiDebug === true);
}

function hasValue(str) {
    "use strict";
    if (str !== undefined && str !== null && str !== "") {
        return true;
    } else {
        return false;
    }
}

function logAMTA(method) {
    "use strict";
    if (isQA()) {
        var argumentsToPass = [];
        argumentsToPass.push("AMTALog:");
        $AM(arguments).each(function (a, b) {
            if (a > 0) {
                argumentsToPass.push(b);
            }
        });

        method.apply(console, argumentsToPass);

        if (getQS("artiHttpLog")) {
            $AM.ajax({
                type: "GET",
                url: "//AM_IScroll_Log/" + (argumentsToPass.join()).replace(/ /g, "_").replace("AMTALog:,", ""),
                cache: true
            });
        }
    }
}


function getTopmostLocation() {
    "use strict";
    var result;
    try {
        result = parent.location.href;
    } catch (e) {
        result = location.href;
    }
    if (!hasValue(result)) {
        return location.href;
    }
    return result;
}

function initArtiMediaPageAds() {
    "use strict";


    //if ($AM('[data-adEnabled="true"]').length > 0) {
    logAMTA(AMTALOG_LOG, "initArtiMediaPageAds()");

    var pArtiMediaSiteKey,
        i,
        proceedLoading = false;

    if (window.hasOwnProperty("artiMediaSiteKey") && hasValue(artiMediaSiteKey)) {
        pArtiMediaSiteKey = artiMediaSiteKey;
    } else {
        logAMTA(AMTALOG_WARN, "No site key variable was defined for iScroll (var artiMediaSiteKey), iScroll loading cancelled");
        return false;
    }

    var UAExcluded = false;
    if (hasValue($AM)) {
        $AM.ajax({
            type: "GET",
            url: "//akamai.advsnx.net/CDN/" + pArtiMediaSiteKey + "/" + pArtiMediaSiteKey + "-exUA.xml",
            dataType: "xml",
            success: function (data) {
                $AM(data).find("UA").each(function () {
                    var matchingAttributes = 0;
                    for (i = 0; i < this.attributes.length; i++) {
                        if (window.navigator.userAgent.search(this.attributes[i].value) > -1) {
                            matchingAttributes++;
                        }
                    }
                    if (matchingAttributes === this.attributes.length) {
                        logAMTA(AMTALOG_LOG, "You current user agent is not supported by ArtiMedia SDK, destroying the current artimedia instance and returning to playback.");
                        UAExcluded = true;
                        return false;
                    }
                });
            },
            error: function (error) {
                logAMTA(AMTALOG_WARN, "Error loading User Agent Exclusion list, proceeding as normal.");
            },
            complete: function () {
                if (!UAExcluded) {
                    var paramsObject = {};
                    paramsObject.category = window.hasOwnProperty("artiMediaCategory") && hasValue(window.artiMediaCategory) ? window.artiMediaCategory : '';
                    paramsObject.siteKey = pArtiMediaSiteKey;

                    logAMTA(AMTALOG_INFO, "artiParams.category: " + paramsObject.category);
                    $AM.ajax({
                        type: "GET",
                        url: "//akamai.advsnx.net/CDN/" + paramsObject.siteKey + "/" + paramsObject.siteKey + "-cat.xml",
                        dataType: "xml",
                        success: function (data) {
                            $AM(data).find('SiteLink').each(function () {
                                var that = this;
                                if ($AM(that).find("category") && hasValue($AM(that).find("category").text())) {
                                    if (paramsObject.category.toLowerCase().search($AM(that).find("category").text().trim().toLowerCase()) > -1) {
                                        paramsObject.newSiteKey = $AM(this).find("siteKey").text().trim();
                                        logAMTA(AMTALOG_INFO, "siteKey changed by category to '" + paramsObject.newSiteKey + "'");
                                    }
                                } else if ($AM(that).find("url") && hasValue($AM(that).find("url").text())) {
                                    if (getTopmostLocation().search($AM(that).find("url").text().trim()) > -1) {
                                        paramsObject.newSiteKey = $AM(this).find("siteKey").text().trim();
                                        logAMTA(AMTALOG_INFO, "no category, siteKey changed by url to '" + paramsObject.newSiteKey + "'");
                                    }
                                }

                                if (paramsObject.hasOwnProperty("newSiteKey")) {
                                    return false;
                                }
                            });

                            if (!paramsObject.hasOwnProperty("newSiteKey")) {
                                var defaultSiteKey = $AM(data).find("default").text().trim();
                                paramsObject.newSiteKey = hasValue(defaultSiteKey) ? defaultSiteKey : paramsObject.siteKey;
                                logAMTA(AMTALOG_INFO, "No matching category nor URL found, siteKey changed to default ('" + paramsObject.newSiteKey + "')");
                            }
                        },
                        error: function (error) {
                            if (!paramsObject.hasOwnProperty("newSiteKey")) {
                                paramsObject.newSiteKey = paramsObject.siteKey;
                                logAMTA(AMTALOG_WARN, "Error loading siteKey mapping file, using the siteKey provided in 'artiParams': " + paramsObject.newSiteKey);
                            }
                        },
                        complete: function () {
                            function isMobile() {
                                var check = false;
                                var a = (navigator.userAgent || navigator.vendor || window.opera);
                                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
                                    check = true;
                                }
                                return check;
                            }
                            if (!paramsObject.hasOwnProperty("newSiteKey")) {
                                paramsObject.newSiteKey = paramsObject.siteKey;
                            }
                            if (isMobile()) {
                                logAMTA(AMTALOG_LOG, "Mobile environment detected");
                                if (paramsObject.newSiteKey.substr(0, 2) !== "m.") {
                                    logAMTA(AMTALOG_LOG, "Provided siteKey (either through the client or the siteKey mapping file) is missing 'm.' prefix, adding it now.");
                                    paramsObject.newSiteKey = "m." + paramsObject.newSiteKey;
                                }
                            } else {
                                logAMTA(AMTALOG_LOG, "Desktop environment detected");
                                if (paramsObject.newSiteKey.substr(0, 2) === "m.") {
                                    logAMTA(AMTALOG_LOG, "Provided siteKey (either through the client or the siteKey mapping file) contains a redundant 'm.' prefix, removing it now.");
                                    paramsObject.newSiteKey = paramsObject.newSiteKey.substr(2);
                                }
                            }
                            logAMTA(AMTALOG_LOG, "Finalized SiteKey (newSiteKey) : " + paramsObject.newSiteKey);
                            var vIScrollAuto = false,
                                vIScrollTarget = null;

                            artiMediaPageAds = new ArtiMediaPageAds(paramsObject.newSiteKey, window.iScrollAuto, window.iScrollTarget);
                        }
                    });
                }
            }
        });
    }
}

function getServerPath() {
    "use strict";
    return ((getQS('artiQA') !== null) ? QA_SERVER_PATH : PROD_SERVER_PATH);
}

var ArtiMediaPageAds = function (siteKey, iScrollAuto, externalIScrollTarget) {
    "use strict";
    logAMTA(AMTALOG_LOG, "ArtiMediaPageAds constructor()");
    var that = this;

    this.iScrollAuto = iScrollAuto;
    this.externalIScrollTarget = externalIScrollTarget;
    this.siteKey = siteKey;
    this.tagGetter = new TagGetter();
    this.vaster = new VASTer();
    var target;

    //$AM('html > head').append(this.getCSS());

    var topIfr = ($AM("div")[0].ownerDocument.parentWindow || $AM("div")[0].ownerDocument.defaultView);
    if (topIfr.hasOwnProperty("inDapIF") && topIfr.inDapIF === true) {
        this.inDapIF = true;
        this.topWindow = window.parent;
        this.parentIframe = topIfr.frameElement;
        target = this.topWindow;
    } else {
        target = window;
    }

    $AM(document.body).on("touchstart", this.onTouchMove);
    $AM(window.top).on("touchstart", this.onTouchMove); // Added handler to top window 1.0.5.22 @SF
    $AM(window.top).focus(this.onWindowFocus);
    if (this.inDapIF) {
        $AM(window).focus(this.onWindowFocus);
    }
    that.getTag();
};

ArtiMediaPageAds.prototype = {
    el: null,
    canvasVideoElem: null,
    amAdIfr: null,
    amAdIfrContent: null,
    v: null,
    //windowScrollInterval: null,
    externalIScrollTarget: null,
    isAdActive: false,
    activeAdElement: null,
    tagGetter: null,
    vaster: null,
    uuid: null,
    siteKey: null,
    videoAspectRatio: (16 / 9),
    completeViewReported: false,
    minTimeView: 30,
    queueQuartilesPopulation: false,
    clickThroughHappened: false,
    inDapIF: false,
    parentIframe: null,
    topWindow: null,

    getCSS: function () {
        "use strict";
        return '<style>' +
            '#iScrollAudioTrack {' +
            '   width: 0px;' +
            '   height: 0px;' +
            '   visibility: hidden;' +
            '}' +
            '.noScroll {' +
            '	overflow-y: hidden;' +
            '}' +
            '.noBorder {' +
            '	border: none;' +
            '}' +
            '#AM_vol {' +
            '	position: absolute;' +
            '	bottom: 10px;' +
            '	left: 10px;' +
            '	width: 30%;' +
            '	height: 30%;' +
            '   background-repeat: no-repeat;' +
            '   background-position: bottom left;' +
            '}' +
            '.AM_moreInfo {\n' +
            '   position:absolute;\n' +
            '   padding: 5px;\n' +
            '   border-radius: 4px;\n' +
            '   cursor:pointer;\n' +
            '   background-color: rgba(0, 0, 0, 0.33);\n' +
            '   color: white;\n' +
            '   white-space: nowrap;\n' +
            '   text-shadow: 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black;\n' +
            '   top:20px;\n' +
            '   left:0;\n' +
            '   font-family: "Alef Hebrew", "Helvetica Neue", Helvetica, Arial, sans-serif;' +
            '}\n' +
            '.AM_muted {' +
            '	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAaCAMAAADhRa4NAAACrFBMVEUAAAABAQECAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGBgYAAAAAAAAJCQkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEHBwcNDQ0PDw8QEBAUFBQWFhYZGRkaGhocHBwhISEjIyMlJSUnJycoKCgqKiosLCwtLS0vLy8yMjI1NTY3Nzc4ODg5OTk7Ozw8PDs8PDw9PT1BQUFDQ0NEREVGRkZHR0hJSUlKSkpLS0tQUFBTU1NdXV1fX19gYGBhYWFiYmJkZGRmZmZnZ2dqamptbW1tbW5vb29xcXFzc3N0dHR1dXV2dnZ4eHd4eHh5eXl7e3t8fHx9fX1+fn2EhISGhoaHh4eIiIiMjIuMjIyRkZGTk5OWlpaXl5ednZ2enp6fn5+hoaGmpqampqenp6eqqqqxsbG2tra3t7e4uLi+vr6+vr+/v76/v7/Dw8TExMTJycnJycrMzMzOzs7Pz8/V1dXZ2dnb29vk5OTp6ent7e3x8fH39/f4+Pj5+fn7+/v+/v7////PdqI4AAAAd3RSTlMAAAABAgMGCAoLEhQVFhcYGRscHB0eHyAhIyYnKCotMDI1Njk6PT9CSU1QVFVZXF5hYnFyc3V5e35/gIGDhIWIjI2QkZKVl5ugoqOlpqepqqutrq+zv8LEyc7R0tTY2tvd3uLl5ujp6uvs7u7v8fHy8/T3+fr7/WIFIzMAAAH+SURBVHgBfdLndwxRGMfx30QvRFmr9xIRWUH0LgoRxSohFmEjooSElUfvw0YZJTqREL333kuCJKIXovD7R+zdnD3sOSbfF/NmPueZufc8QICGvzUcM6U7/kmDP2gjB3IWNzUHXeUI6W5vCobLHZLrOpiA8uPkGT+/p24yoVHCyk98t/AR9fD/gtaS9oM5Iq+o2+pHRQRC1WLwQAtQ2tapc8dhcoi8JqOT8qlHzrqQlljbe6ajWZNgcS5b7+k6mSkhcBVQj1tFbkm0et4fJwWR235R9d1wBaGS5FHvG3uC3DSttxwjjWjE3OLHF7m52Ssm1gQsCgTXk5PkRvUwZgTCcZX7JSVl+oBSAKop0A6hcorfbpBb51bXMOEyM0aVKAnAB8KBYEknC9c4ywV4QaYd8ActZQ/5dbmzjAY4rnC3uObH9SsC+eoTIZLFn/fIDbOrADG3+fbJ0+z7Sx01fD9ZRw6T2+USf6fGV8SQnfRW6J7XHJUV6DM5g9yRECHnSfdYWONXG4ax+S6ZLq2w4GXRRe1KroW2cpEUoGxYtx5d7OrUZ8We5JkQNefmvuS6gEecOx0LaFCFyl7ygchr6rYGI/pXhSpo6CCrD6DJzLVfmCeP1T745QOoMH5RAT+8MQfASHnoXTlTgJ5yhkw1nwCEycHnS5oVA9A4emov+PUHn1n4rvkF6WYAAAAASUVORK5CYII=);' +
            '}' +
            '.AM_unmuted {' +
            '   background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAaCAYAAADWm14/AAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC45bDN+TgAABMFJREFUSEu9VllIo1cUNpmoHYso7ih0tCiKjChmarEPxWLtS20VtUJFqg/Th4ogolN8MtSWqn1wQdGC+FDcsBYFjQgySmkCZuoyVnGLO1ZtTaLGJc1++51rIkbrMMbQA4ecc8/Jvd892/3d7kuMMZFN/H/JarXG6XS6pc3NTQPkb2zLDoT1h+Bfjo+P1fittS3fn7DZR9vb29r4+HgmFArZ7OysRalUBtrMlwS/L6urq7lPV1cXM5vNX9lMzhM2fTo9PW0KCwtjUDmPjIywoaGheO4AQloewO8dcGlzczP38fb2Zuvr62eI2qMLrzsSNhViw+rh4WG+GS3ZmQDghmKbn8BisYz29vYypEcHWZeWlsb9MjMzmclk6iW/OxE2fQOH97S0tDCRSORwOPFVAPB7NDY2xtejo6PZ+fm5YW1tzezp6ckEAgGbmJiwaDSax+T7WoTDA5A7eXl5+Y2D7UwA+vv7k3H4J+B3j46OtsPDw7mtqqqKYU1fVFTE9dzcXKbX63+EfEG20DarVCr1/v6+9jrv7Ozos7OzHQ68zgQAN1W2trYyuVxuRdi7BgcHuc3Hx4dptVrj0tKSmSJAkVCr1X+LxWJ32HnI0np6enh46A/OMAFYWFjgsr+/Px3wF6L2IiEhga+1tbUxgPoHh3Kd6mN1dfU9yBxAXmVl5eVmzjABQHXv2g+oqalhBoNB2tDQwPX09HQCoKuoqOB6cXExQ8S/huw6ACsrK8+o10knIMjz2NzcnIl0igoiYujr6+P2lJQUhjrpgCxwGYDu7u4nBwcHKkoldcrJycmfALHm4eHBfXBjPeYHlyMjI6kufoX8wGUAqA2xl4KKjtYwdIwozOng4GCuowgtyLuV5ICAAAI4A9nD1QBe2IfUxsaGEYe8tANYXl4mAFwODAxkeB9eQvZ0GQBUdtLu7u4R6e7u7pTjfYR5h9JBaUFnGCcnJ7l/VFQUpUQG2XURwLSTtLe3cz05OZlhhvyOnFtIDwoKohFsoHYnPTU1lcZ0H2QRAfhcIpE4bHhXJgDItyo2NpbrjY2NbGtrS1FbW8v1rKwsZjQa9WVlZVwvLS1lCoXiO8hCmoQJmM9Wap24uLgbTBVLf3oVEwA8zzzUoaGhbG9vT4OO2IiJieH2jo4OAmCg/UgfGBhgTU1Nn0K+IAyRz5AjKVDLEBr5VT48PFylCL1qUhIAFNXz8fFxtri4aJqfn/8NbcltVHCnp6fmmZkZ3gFeXl6UHjXkMLAD0Wz2Ar95lTMyMnyB/icQs/f0dSYAsD+RyWTikpKS9ykCISEh3FZfX0+3NxUWFnK9oKCAYUD9DPkh+LVJiChJ8MxafX19HQ4nJgBXnmN/FB+/bWJiItWGCQdaqRvoywiRsKAWPoZdQP53IcHZ2dkXCK8+IiLiVgBEmPuNFHK0JX0BmagrsMzy8/Pp9s8h+3FHZwj9/QE21iQlJd0KACTAQ/TW1NTUs7q6Ou5D7wGG0zk64kPoQu7lLCHHsZhy6zk5OczPz4/hg9SYl5f3ts18SUhHBhUjTUOpVMo6Ozt/wDLV1v0JHRKC8PYjIkq8ct9i6cbG9LGDAfQ9JuIfo6Oj9BUUemFxHVEovcFU0bcVFa2T/T983Nz+BdvNcMIyn8o0AAAAAElFTkSuQmCC)' +
            '}' +
            '.AM_progressBar {' +
            '    width: 100%;' +
            '	 direction: ltr;' +
            '    height: 3px;' +
            '	 position: absolute;' +
            '	 bottom: 0px;' +
            '	 text-align: left;' +
            '}' +
            '.AM_progressBar div {' +
            '    height: 100%;' +
            '    width: 0;' +
            '    background-color: #C83339;' +
            '    background:-webkit-linear-gradient(top, #FFAAAA, #C83339) ;' +
            '}' +
            '#AM_logo {' +
            '	right: 6px;' +
            '	bottom: 6px;' +
            '	width: 20px;' +
            '	height: 20px;' +
            '	background-size: cover;' +
            '	position: absolute;' +
            '	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAABgFBMVEUAAAAAAAD/AAB/AAD/AH+qVVX/VVW/Pz/UKiraJEi/Pz/MMzO/Kj/EOjrINjbMMzPPLz/JNTXOMDzFLjnHNzfLMj3EMTrGLzjGODjINjbKND3HLzfKNDzEMjrLMjrJNTzKNDrKNDnGMjjJMTrFNDnKNDnGMzjIMjrGNDjIMjbIMjrGNDjGMTjFMjnHMTfHNDfHNDrJMjjIMznJMjjIMzjJMjjHMjfHMTnGMzjGMjjIMzjGMzjJMzjIMzjIMjnIMjjHMzfIMjjIMjjGMjjIMjnHMznHMzjIMjjJMjnIMzjIMzjIMjnIMznIMzjIMznHMjnHMjjHMzjIMjjJMznHMjjIMznIMznIMznHMjjHMzjJMjjIMjjHMzjIMjnHMjjIMjnIMjnHMjjIMjnIMjjHMjjIMjjIMznHMznIMjjHMjjHMjnHMjnHMzjHMjnHMjjIMzjHMznHMjnIMjjHMzjHMzjHMznHMjnIMjnIMjjHMzjHMjnIMjjIMjjHMzjHMznIMzlKtzwsAAAAf3RSTlMAAQECAgMDBAYHCAoMDQ4PEBMVFhcZGhsbHB0gIiMjJicsMjQ1NTY9P0FBREhMTk5OUVlbXl9gYWNkZ2hobHR6e36DhISGi4yOkJWXmp6foaKip6qrrK2usLO0tb3DxcjJys3O19fa29zd3uPl7O7v8PHy8/T09fb3+Pr7/P3+ZPhxBAAAAPhJREFUeNp1ztc7gnEchvGvt8iKssnMlq1ssvfesrMpVEjrd//r6tXlcuI+eq7P0SP/ZRnZebzbdpp/wTD+hp7fqf2IcQ0+D2fmjiOwlKGTG/aKRdOk0gMTKamOsiF2z0foqCFzn1BJkhbw5bVGAN6bLC9MJ+mGWcMtT67hZ7yyzIVIQZyuerCLtKMq+gnki1XR0UjcKlIDdX18FUqWnzFzmJXs3HVecyZ5MIkccCrzEAjClHbFpoh0o3pNqwmILRoGUG2p82cEHVI7NGiTnjAn+v0qH4ktR1l5567ivkj0bF5AKeC8VNKZRq9jEL10GeVP1pZmS3p+A+cFSARDr8qsAAAAAElFTkSuQmCC);' +
            '}' +
            '#AM_adHeader {' +
            '	direction: ltr;' +
            '	height: 16px;' +
            '	background-color: #000000;' +
            '	position: relative;' +
            '}' +
            '#AM_adHeader p {' +
            '	padding-top: 2px;' +
            '	color:#ffffff;' +
            '	width:100%;' +
            '	text-align:center;' +
            '	font-size: 14px;' +
            '	font-family: Arial;' +
            '}' + '</style>';
    },

    updateProgress: function (percent, $element) {
        "use strict";
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').css({
            width: progressBarWidth
        });
    },

    isHalfwayViewable: function (elem) {
        "use strict";
        if (!this.isAdActive) {
            $AM(elem).css("display", "block");
        }

        var w = $AM(window);

        if (this.inDapIF) {
            elem = $AM(elem);
            w = $AM(this.topWindow);
        }

        var docViewTop = w.scrollTop();
        var docViewBottom = docViewTop + w[0].innerHeight; //used to be docViewTop + w.height() but the final calculation was wrong

        var elemHeight = elem.height();
        var elemTop = elem.offset().top;
        var elemBottom = elemTop + elemHeight;

        if (!this.isAdActive) {
            var elemTargetHeight = (elem.width() / this.videoAspectRatio) + 22;
            $AM(elem).css("display", "none");
            elemBottom = elemTop + (elemTargetHeight * 0.50);
            var min = docViewTop; // - elemTargetHeight/2;
            var max = docViewBottom; // + elemTargetHeight/2;
            return (elemTop > min) && (elemBottom < max);
        }

        return ((elemBottom - elemHeight * 0.25 <= docViewBottom) && (elemTop + elemHeight * 0.25 >= docViewTop));
    },

    adStarted: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "adStarted()");
        var that = this;

        this.showIScrollElements();

        this.el.css({
            position: 'relative',
            width: "100%",
            direction: "ltr",
            height: 0,
            display: 'block'
        });

        var toWidth = "100%";
        var toHeight = this.cv.iScrollContainer.targetObj.canvas.clientHeight + 21;

        if (this.inDapIF) {
            var toHeightIfr = toHeight + 22 + "px";

            $AM(this.parentIframe).animate({
                height: toHeightIfr
            }, 500, function () {
                $AM(this.parentIframe).css({
                    backgroundColor: "#000000",
                    height: toHeightIfr
                });
                $AM(that.parentIframe).attr("height", (toHeight + 39 + "px"));
            });
        }



        that.amAdIfr.animate({
            width: toWidth,
            height: toHeight + "px"
        }, 500);


        that.el.animate({
            width: toWidth,
            height: toHeight + "px"
        }, 500, function () {

            $AM(that.el).css({
                height: toHeight + "px"
            });
        });

        that.vaster.trackImpression();
        that.vaster.adStarted();
        that.v.muted = true;

        // Call developer SDK event function 1.0.5.17 @Shmuel
        window.AMSDK.onAdStart();

        if (that.vaster.clickThroughEnabled) {
            $AM(that.v).css('cursor', 'pointer');
        }
    },

    adEnded: function () {
        "use strict";
        var that = this;
        logAMTA(AMTALOG_LOG, "adEnded()");
        window.AMSDK.onAdComplete();
        if (that.inDapIF) {
            $AM(this.parentIframe).animate({
                height: 0
            }, 500);
        }

        that.el.animate({
            height: 0
        }, 500);
        /*$AM("#AM_adHeader").animate({
            height: 0
        }, 400, function () {
            $AM("#AM_adHeader").remove();
        });*/

        that.isAdActive = false;
        that.vaster.adEnded();

        that.destroyVideo();
        that.destroyActiveAdElement();


        if ($AM('[data-adEnabled="true"]').length === 0) {
            that.unload();
        }
        
        setTimeout(function () {
            window.AMSDK.onAdComplete();
        }, 500);
    },

    unload: function () {
        "use strict";
        logAMTA(AMTALOG_INFO, "Unloading iScroll!");

        if (this.inDapIF) {
            $AM(this.topWindow).off("touchstart", this.onTouchMove);
            $AM(this.topWindow).off("scroll", this.onTouchMove);
            $AM(this.topWindow).focus(this.onWindowFocus);
        } else {
            $AM(document.body).off("touchstart", this.onTouchMove);
            $AM(window).off("scroll", this.onTouchMove);
            $AM(window).off("focus", this.onWindowFocus);
        }

        $AM(window.top).off("touchstart", this.onTouchMove); // Added handler to top window 1.0.5.22 @SF

        // Call developer SDK event function 1.0.5.17 @Shmuel
        // Send onAdComplete on any completion of ad cycle

        //window.AMSDK.onAdComplete();
    },

    adError: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "adError()");
        this.isAdActive = false;
    },

    browserSupportCookieRemoved: false,

    browserSupportLocalStorageItemSet: false,

    onTouchMove: function (e) {
        "use strict";
        logAMTA(AMTALOG_LOG, e.type);
        var that = artiMediaPageAds,
            targetElementToCheck = null;

        if (that.inDapIF) {
            targetElementToCheck = that.parentIframe;
        }
        if (!that.isAdActive) {
            $AM('[data-adEnabled="true"]').each(function () {
                if (that.isAdActive) {
                    return false; //each loop "break"
                }

                if (!that.inDapIF) {
                    targetElementToCheck = $AM(this);
                }
                if (that.isHalfwayViewable(targetElementToCheck) && that.activeAdElement !== $AM(this)) {
                    logAMTA(AMTALOG_INFO, "halfway visible! shouldn't be here twice!");
                    that.isAdActive = true;
                    that.activeAdElement = that.el = $AM(this);

                    if (that.browserSupportLocalStorageItemSet === false) {
                        that.browserSupportLocalStorageItemSet = true;
                        logAMTA(AMTALOG_INFO, "Trying to set 'AM_browserNotSupportedOnVersion' cookie");
                        setCookie('AM_browserNotSupportedOnVersion', AMTAH5_VERSION);
                    }

                    var checkIfLocalStorageCookieSet = function () {
                        if (getCookie('AM_browserNotSupportedOnVersion') !== AMTAH5_VERSION) {
                            window.setTimeout(checkIfLocalStorageCookieSet, 100);
                        } else {
                            logAMTA(AMTALOG_INFO, "'AM_browserNotSupportedOnVersion' cookie was set successfully! trying playback...");
                            var playAdIfReady = function () {
                                if (that.videoCanvasObjectReady()) {
                                    logAMTA(AMTALOG_INFO, "Video canvas is ready! playing...");
                                    that.cv[that.activeAdElement.attr("id")].audioElem = $AM(that.amAdIfrContent.document.body).find("#iScrollAudioTrack")[0];
                                    that.playAd();
                                    $AM(document.body).off("touchstart", that.onTouchMove);
                                    $AM(window.top).off("touchstart", that.onTouchMove); // Added handler to top window 1.0.5.22 @SF
                                    $AM(window.top).on("scroll", that.onTouchMove);
                                } else {
                                    logAMTA(AMTALOG_INFO, "Video canvas isn't ready yet.. trying again in 500ms");
                                    window.setTimeout(playAdIfReady, 1000);
                                }
                            };
                            playAdIfReady();
                        }
                    };
                    checkIfLocalStorageCookieSet();


                }
            });
        } else {
            if (that.v) {
                if (!that.inDapIF) {
                    targetElementToCheck = that.activeAdElement;
                }
                if (!that.isHalfwayViewable(targetElementToCheck)) {
                    if (!that.v.paused) {
                        that.pauseAd(that.activeAdElement);
                    }
                } else if (that.v.paused && !that.clickThroughHappened) {
                    that.resumeAd(that.activeAdElement);
                }
            }
        }
    },

    onWindowFocus: function () {
        "use strict";
        var that = artiMediaPageAds;
        logAMTA(AMTALOG_LOG, "got focus");
        var checkTarget = that.el;
        if (that.inDapIF) {
            checkTarget = that.parentIframe;
        }

        if (that.v && that.v.paused && that.isHalfwayViewable(checkTarget)) {
            that.resumeAd();
			that.clickThroughHappened = false;
            //that.v.muted = true;
        }
    },

    /*countVideoView: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "countVideoView()");

        var that = this;
        var countVideoViewURL = "//" + getServerPath() + "/AdServer/Service.svc/cvv?" +
            "sk=" + this.siteKey +
            "&EMD=" + this.externalMediaID +
            "&cb=" + (Math.floor(Math.random() * 9000000000000) + 1000000000000);

        if (that.siteKey.search("walla") > -1) {
            var wallaImpressionURL = "//fus.walla.co.il/impression/xnzvw/" + that.mediaZone + "/art_l_res/3848360411?" + //"//fus.walla.co.il/impression/xnzvw/mint.new_walla.advertorial/art_l_res/3848360411?" +
                "affiliate=" + that.affiliateID +
                "&custom2=" + that.externalMediaID;

            $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + wallaImpressionURL + '"/>');
            var trackFUSURL = "//logger.logidea.info:8080/?t=vr&cs=s&sk=wjw." + window.location.host + "&cb" + (new Date().getTime());
            $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + trackFUSURL + '"/>');
        }

        $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + countVideoViewURL + '"/>');

        var trackCVVURL = "//logger.logidea.info:8080/?t=vv&cs=c&sk=wjw." + window.location.host + "&cb" + (new Date().getTime());
        $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + trackCVVURL + '"/>');
    },*/

    initVideoEvents: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "initVideoEvents()");
        this.v.onloadedmetadata = this.onAdVideoEvent;
        this.v.onplaying = this.onAdVideoEvent;
        this.v.ontimeupdate = this.onAdVideoEvent;
        this.v.onended = this.onAdVideoEvent;
    },

    destroyVideo: function () {
        "use strict";
        logAMTA(AMTALOG_INFO, "destroying the active ad element's video");
        //this.v.onloadstart = 
        this.v.onclick = this.v.loadedmetadata = this.v.onloadedmetadata = this.v.onplaying = this.v.ontimeupdate = this.v.onended = this.cv = null;
        this.v = null;
    },

    destroyActiveAdElement: function () {
        "use strict";
        logAMTA(AMTALOG_INFO, "destroying the active ad element's iframe and container");

        this.activeAdElement.html("");
        this.activeAdElement = null;
    },

    onAdVideoEvent: function (event) {
        "use strict";
        var that = artiMediaPageAds;

        switch (event.type) {
        case "loadedmetadata":
            that.videoAspectRatio = that.v.videoWidth / that.v.videoHeight;
            if (that.v.duration > 0) {
                that.vaster.populateQuartiles(that.v.duration);
            } else {
                that.queueQuartilesPopulation = true;
            }
            break;
        case "click":
            if (that.isHalfwayViewable(that.el) && that.clickThroughHappened && that.v.paused) {
                that.clickThroughHappened = false;
                that.resumeAd();
                return;
            }
            window.focus();
            that.clickThroughHappened = true;
            that.pauseAd();
			$AM("#AM_vol").attr("class", "AM_muted");
            that.vaster.adClicked(that.v.currentTime);
            break;
        case "playing":
            that.v.onplaying = null;
            that.activeAdElement.attr("data-adEnabled", false);
            break;
        case "timeupdate":
            that.onTimeUpdate();
            break;
        case "ended":
            setTimeout(function () {
                that.adEnded();
            }, 2000);
            break;
        }
    },

    hasVideoCanvasObject: function () {
        "use strict";
        if (Object.keys(this.cv).length > 0) {
            //this.v = this.cv[this.activeAdElement.attr("id")];
            this.v = this.cv.iScrollContainer;
            if (this.v) {
                return true;
            }
        }
        return false;
    },

    videoCanvasObjectReady: function () {
        "use strict";
        if (this.hasVideoCanvasObject() && this.v.ready === true) {
            return true;
        }
        return false;
    },

    playAd: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "playAd()");

        this.initVideoEvents();
        this.v.muted = true;
        this.v.play();
    },

    doOnceOnFirstRealPlayback: true,

    onTimeUpdate: function () {
        "use strict";

        this.updateProgress((this.v.currentTime / this.v.duration) * 100, $AM(this.amAdIfrContent.document.body).find('.AM_progressBar'));

        if (this.v.currentTime > 0.2) {
            if (this.doOnceOnFirstRealPlayback) {
                this.doOnceOnFirstRealPlayback = false;
                if (!this.browserSupportCookieRemoved) {
                    this.browserSupportCookieRemoved = true;
                    logAMTA(AMTALOG_INFO, "Removing 'AM_browserNotSupportedOnVersion' cookie");
                    //localStorage.removeItem("AM_browserNotSupportedOnVersion");
                    removeCookie("AM_browserNotSupportedOnVersion");
                }
                this.adStarted();
            }
            if (parseInt(this.v.currentTime, 10) === this.minTimeView && !this.completeViewReported) {
                this.completeViewReported = true;
                this.vaster.completeView(this.v.currentTime);
            }
            if (this.queueQuartilesPopulation === true) {
                this.queueQuartilesPopulation = false;
                this.vaster.populateQuartiles(this.v.duration);
            }
            this.vaster.checkForQuartileTracking(this.v.currentTime);
        }

    },

    pauseAd: function (activeAdElement) {
        "use strict";
        logAMTA(AMTALOG_LOG, "pauseAd()");
        if (artiMediaPageAds.isAdActive && !artiMediaPageAds.v.paused) {
            artiMediaPageAds.v.pause();
        }
    },

    resumeAd: function (activeAdElement) {
        "use strict";
        logAMTA(AMTALOG_LOG, "resumeAd()");
        artiMediaPageAds.v.play();
    },

    getTag: function () {
        "use strict";
        var that = this;
        logAMTA(AMTALOG_LOG, "getTag()");
        $AM(this.tagGetter).on(TagGetterEvents.TAG_ZONES_LOADED, function (ev) {
            that.tagZonesLoaded(ev);
        });
        $AM(this.tagGetter).on(TagGetterEvents.EMPTY_TAG_RESPONSE, function () {
            logAMTA(AMTALOG_ERROR, "Failed GetNewTags response.");
            window.AMSDK.onPassBack();
            that.unload();

        });

        this.tagGetter.getNewTag(this.siteKey, window.amLoadedUID, AMTAH5_VERSION);
    },

    loadAd: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "loadAd()");
        if (this.ads) {
            var artipreview = getQS("artipreview");
            this.vaster.loadVAST((this.ads.iScroll.zoneTagURL).replace(/&amp;/g, '&').replace("[timestamp]", (Math.floor(Math.random() * 9000000000000) + 1000000000000)) + (hasValue(artipreview) ? ("&artipreview=" + artipreview) : ''));
        } else {
            setTimeout(this.loadAd, 250);
        }
    },

    cv: {},

    tagZonesLoaded: function (event) {
        "use strict";
        var that = this;

        var targetIScrollContainer = hasValue(event.iScrollTarget) ? event.iScrollTarget : (hasValue(this.externalIScrollTarget) ? this.externalIScrollTarget : null);

        var success = this.initUI(targetIScrollContainer, hasValue(event.iScrollParagraphIndexTargetPosition) ? parseInt(event.iScrollParagraphIndexTargetPosition, 10) : 3, event.optionalSeparator);
        if (!success) {
            window.AMSDK.onPassBack();
            return this.unload();
        }

        logAMTA(AMTALOG_LOG, "tagZonesLoaded()");
        if (this.timeoutReached) {
            return null;
        }

        this.vaster = new VASTer();

        $AM(this.vaster).on(VASTerEvents.VAST_VIDEO_URI_READY, function (ev) {
            logAMTA(AMTALOG_LOG, "VAST Loaded");
            var target = $AM("#iScrollContainer");
            that.canvasVideoElem = $AM(that.amAdIfrContent.document.body).find("#iScrollAdVideoCanvas");
            that.canvasVideoElem.attr("src", ev.url);

            that.cv[target.attr("id")] = new CanvasVideo(that.canvasVideoElem.get(0), false);
            that.hasVideoCanvasObject();
            that.initVideoEvents();
            that.cv[target.attr("id")].init();

            $AM(that.cv[target.attr("id")]).on(CanvasVideoEvent.READY, function () {
                logAMTA(AMTALOG_INFO, "Video is ready to play, took", performance.now() + "ms");
            });
        });

        $AM(this.vaster).on(VASTerEvents.EMPTY_VAST_RESPONSE, function (ev) {
            logAMTA(AMTALOG_ERROR, "Empty VAST tag response, Aborting!");
            window.AMSDK.onPassBack();
            that.unload();
            return null;
        });

        this.ads = event.ads;
        this.loadAd();
    },

    hideIScrollElements: function () {
        "use strict";
        $AM('[data-adEnabled]').css("opacity", 0);
    },

    showIScrollElements: function () {
        "use strict";
        $AM('[data-adEnabled]').css("opacity", 1);
    },

    initUI: function (targetContainerID, targetParagraph, optionalSeparator) {
        "use strict";
        var that = this;

        if (this.iScrollAuto === "true" || this.iScrollAuto === true) { //First integration method - Get the target position for the iScroll from GNT
            if (!hasValue(optionalSeparator)) {
                optionalSeparator = "p";
            }
            //$AM('<'+optionalSeparator+'>'+'<div id="iScrollContainer" data-adEnabled="true" data-adType="iScroll"></div>'+'</'+optionalSeparator+'>').insertAfter($AM(targetContainerID + " " + optionalSeparator + ":nth-of-type(" + targetParagraph + ")"));
            //$AM('<'+optionalSeparator+'>'+'<div id="iScrollContainer" data-adEnabled="true" data-adType="iScroll"></div>'+'</'+optionalSeparator+'>').insertAfter($AM(targetContainerID).find(optionalSeparator)[targetParagraph]);
            var targetContainer = '<' + optionalSeparator + '><div id="iScrollContainer" data-adEnabled="true" data-adType="iScroll"></div></' + optionalSeparator + '>';
            var placeHolder = $AM(targetContainerID).find(optionalSeparator)[targetParagraph];
            if (!hasValue(placeHolder)) {
                logAMTA(AMTALOG_ERROR, "iScrollAuto integration has been used, but mo matching container (CP: 'iScrollTarget') or sub-element (CP: 'iScrollOptionalSeparator', default = 'p') with its defined position (CP: 'iScrollPosition', default: 3) has been found.");
                return false;
            }
            $AM(targetContainer).insertAfter(placeHolder);
        } else if ($AM('[data-adEnabled="true"]').length < 1) { //Second integration method - If no designated container was defined by the publisher (element with data-adEnabled="true") - we create it on the fly at the script location and then procceed as if a designated container was created by the publisher
            var currentScript = $AM("script[src$='AMInPageAds.js']");
            // Check backwards compatibility

            if (currentScript.length < 1) {
                currentScript = $AM("script[src$='amloader.js']");
            }

            $AM('<div id="iScrollContainer" data-adEnabled="true" data-adType="iScroll"></div>').insertAfter($AM(currentScript));
        }


        var getIframeWindow = function (iframe_object) {
            var doc;

            if (iframe_object.contentWindow) {
                return iframe_object.contentWindow;
            }

            if (iframe_object.window) {
                return iframe_object.window;
            }

            if (!doc && iframe_object.contentDocument) {
                doc = iframe_object.contentDocument;
            }

            if (!doc && iframe_object.document) {
                doc = iframe_object.document;
            }

            if (doc && doc.defaultView) {
                return doc.defaultView;
            }

            if (doc && doc.parentWindow) {
                return doc.parentWindow;
            }

            return undefined;
        };

        this.amAdIfr = $AM('<iframe id="AMiScrollIframe" width="100%" style="' +
            ' top: 0; ' +
            ' left: 0; ' +
            ' width: 100%; ' +
            ' overflow: hidden; ' +
            ' border: 0; ' +
            ' outline: none; ' +
            ' height: 0;" ' +
            ' scrolling="no" ' +
            ' src="javascript:document.write(\'' + encodeURIComponent('<scr' + 'ipt>document.domain="' + document.domain + '"</sc' + 'ript>') + '\')"></iframe>');
        $AM('[data-adEnabled="true"]').append(this.amAdIfr);

        this.amAdIfrContent = getIframeWindow(this.amAdIfr[0]);
        if (!hasValue(this.amAdIfrContent)) {
            logAMTA(AMTALOG_ERROR, "No matching container for iScroll has been found.");
            return false;
        }

        var amPageInnerHTMLString = '<html><head><style>' +
            '	.artiMediaAdCover {' +
            '		width: 100%;' +
            '		height: 100%;' +
            '		top: 0;' +
            '		left: 0;' +
            '		position: absolute;' +
            '		cursor: pointer;' +
            '	}' +
            '</style></head><body style="padding: 0;border: 0;outline: 0;margin: 0;">' +
            '<div id="AM_adHeader" align="left" style="padding:4px 0 0 4px">' +
            '<div style="font-size: 10px;font-family: Arial;vertical-align: middle;float: left;color:#ffffff;">' +
            'Ad by <b style="letter-spacing: -0.5px;">a r t i m e d i a</b>' +
            '<img style="vertical-align: middle; color: black; margin: 1px 0 0 3px;" src="//akamai.advsnx.net/CDN/ta-extra/small.png"/>' +
            '</div>' +
            '</div>' +
            '<canvas id="iScrollAdVideoCanvas" style="position:relative;background-color: #000;width:100%;" style="display:block; float: left; position: relative;" workers="true" render="false" webgl="auto"></canvas>' +
            '<div class="artiMediaAdCover"></div>' +
            '<span id="AM_vol" class="AM_muted"></span>' +
            '<div id="AM_logo"></div>' +
            '<div dir="rtl" class="AM_moreInfo"></div>\n' +
            '<div class="AM_progressBar"><div></div></div>' +
            '<audio id="iScrollAudioTrack" muted></audio></body></html>';
        this.amAdIfrContent.document.write(amPageInnerHTMLString);
        this.amAdIfrContent.document.close();

        $AM(this.amAdIfrContent.document.head).append(this.getCSS());

        var amMoreInfo = $AM(this.amAdIfrContent.document.body).find(".AM_moreInfo");
        amMoreInfo.click(that.onAdVideoEvent);
        //$AM(this.amAdIfrContent.document.body).click(this.onAdVideoEvent); //CLICK DOESN'T WORK

        amMoreInfo.html("&#x5DC;&#x5DE;&#x5D9;&#x5D3;&#x5E2; &#x5E0;&#x5D5;&#x5E1;&#x5E3; &#x3E;");
        var amVol = $AM(this.amAdIfrContent.document.body).find("#AM_vol");

        amVol.click(function () {
            var v = that.cv[that.activeAdElement.attr("id")];

            if (v.pMuted === true) {
                v.muted = false;
                amVol.attr("class", "AM_unmuted");
            } else {
                v.muted = true;
                amVol.attr("class", "AM_muted");
            }
        });

        /*$AM('[data-adEnabled="true"]').each(function () {
            $AM(this).html('<style>' +
                '	.artiMediaAdCover {' +
                '		width: 100%;' +
                '		height: 100%;' +
                '		top: 0;' +
                '		left: 0;' +
                '		position: absolute;' +
                '		cursor: pointer;' +
                '	}' +
                '</style>' +
                '<canvas id="iScrollAdVideoCanvas" style="position:relative;background-color: #000;width:100%;" style="display:block; float: left; position: relative;" workers="true" render="false" webgl="auto"></canvas>' +
                '<div class="artiMediaAdCover"></div>' +
                '<span id="AM_vol" class="AM_muted"></span>' +
                '<div id="AM_logo"></div>' +
                '<div dir="rtl" class="AM_moreInfo"></div>\n' +
                '<div class="AM_progressBar"><div></div></div>' +
                '<audio id="iScrollAudioTrack"></audio>');
            $AM(this).find(".AM_moreInfo").click(that.onAdVideoEvent);
            //$AM(this.amAdIfrContent.document.body).click(this.onAdVideoEvent); //CLICK DOESN'T WORK

            $AM(this).find(".AM_moreInfo").html("&#x5DC;&#x5DE;&#x5D9;&#x5D3;&#x5E2; &#x5E0;&#x5D5;&#x5E1;&#x5E3; &#x3E;");
            $AM(this).find("#AM_vol").click(function () {
                var v = that.cv[that.activeAdElement.attr("id")];

                if (v.pMuted === true) {
                    v.muted = false;
                    $AM("#AM_vol").attr("class", "AM_unmuted");
                } else {
                    v.muted = true;
                    $AM("#AM_vol").attr("class", "AM_muted");
                }
            });
        });*/

        $AM('[data-adEnabled="true"]').css({
            backgroundColor: "black"
        });
        $AM('[data-adEnabled="true"]').attr("id", "iScrollContainer");
        this.hideIScrollElements();


        if ($AM('[data-adEnabled="true"]').length === 0) {
            logAMTA(AMTALOG_ERROR, "No matching container for iScroll has been found.");
            return false;
        }
        return true;
    }
};

(function () {
    "use strict";
    var script = document.createElement("SCRIPT");
    script.src = '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function (callback) {
        if (window.jQuery && window.jQuery.fn.jquery === "2.1.4") {
            callback(jQuery);
        } else {
            window.setTimeout(function () {
                checkReady(callback);
            }, 250);
        }
    };


    // Start polling...
    checkReady(function ($) {
        window.$AM = jQuery.noConflict(true);
        $AM.ajaxSetup({
            cache: true
        });

        var uidCookie = CookieHelper.getCookieUUID();
        // if cookie exist than current UUID was generated using latest k.html (after change from direct call to k.js) so no need to go again. otherwise do the k.html process
        if (hasValue(uidCookie && uidCookie !== 'undefined')) {
            window.amLoadedUID = uidCookie;
        } else {
            Kjs.getUUID(window, function (uuid) {
                window.amLoadedUID = uuid;
                CookieHelper.setCookieUUID(uuid);
            });
        }

        $AM.ajaxSetup({
            cache: false
        });

        var msPassedSinceUUIDRequest = 0;

        function procceedToInit() {
            if (hasValue(window.amLoadedUID) || msPassedSinceUUIDRequest > 9000) {
                initArtiMediaPageAds();
            } else {
                msPassedSinceUUIDRequest += 1000;
                setTimeout(procceedToInit, 1000);
            }
        };

        procceedToInit();
    });
}());

var $AM;