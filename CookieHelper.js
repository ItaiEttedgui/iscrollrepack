var uidCookie = 'AM-UUID';

var CookieHelper = {
    set: function (key, value, path) {
        path = path ? ';path=' + path : '';
        document.cookie = key + '=' + value + path;
    },

    get: function (key) {
        var split = document.cookie.split(';');

        for (var i = 0; i < split.length; i++) {
            var keyVal = split[i].split('=');

            if (keyVal[0] && keyVal[0].trim() === key) {
                return keyVal[1];
            }
        }

        return null;
    },

    getCookieUUID: function () {
        return CookieHelper.get(uidCookie);
    },

    setCookieUUID: function (value) {
        CookieHelper.set(uidCookie, value, '/');
    }
};