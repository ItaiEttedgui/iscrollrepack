/*jslint browser: true*/
/*global $AM, debugger, jQuery, getServerPath, getQS, hasValue, Ad, OverlayAd, OnPauseAd, DisplayAd, InstreamAd, InstreamAdTypes */

var TagGetter = function () {
    "use strict";
};

var TagGetterEvents = {
    TAG_ZONES_LOADED: "tagZonesLoaded",
    EMPTY_TAG_RESPONSE: "emptyTagResponse"
};

TagGetter.prototype = {
    tagXMLData: null,
    getNewTag: function (siteKey, uuid, clientVer) {
        "use strict";
        var getNewTagsURL = "//" + getServerPath() + "/as/Service.svc/gnt?" +
            "sk=" + siteKey +
            "&pu=" + location.href +
            "&cv=" + clientVer +
            "&cb=" + (Math.floor(Math.random() * 9000000000000) + 1000000000000) +
            (hasValue(uuid) ? ("&uuid=" + uuid) : ''),
            that = this;
        if (hasValue(getQS("artiXML"))) {
            getNewTagsURL = getQS("artiXML");
        }

        $AM.ajax({
            /*xhrFields: {
            withCredentials: true
            },*/
            type: "GET",
            url: getNewTagsURL,
            dataType: "xml"
        }).done(function (data) {
            //<Zone Tag="//lb.advsnx.net/asa/a.aspx?ZoneID=3410&Task=Get&SiteID=62&Random=[timestamp]&podSessionID=715952a4-48d4-4b99-b6fa-45e2588c90b5&uuid=4AckxmAcOrUpNKFy8bkENyZNqiH9qIifdH81R66goVFuSGjIGpuPnGGTf2%2bOW0NY" TotalMaxAds="1" ShowFirstAdAfter="0" MaxRollDuration="7" IntervalMinAds="0" GroupMaxAds="1" GroupInterval="0" ShowEffects="False" ZoneTypeID="16" Description="In Board" ZoneID="3410"/>
            var ads = {},
                hasValidAd = false;
            $AM(data).find('Zone').each(function () {

                var that = this,
                    getAdType = function (type) {
                        return ($AM(that).attr("Description").toLowerCase().search(type.toLowerCase()) > -1);
                    };


                if (getAdType("iScroll")) {
                    hasValidAd = true;
                    ads.iScroll = new InstreamAd(InstreamAdTypes.ISCROLL);
                    ads.iScroll.zoneTagURL = $AM(that).attr("Tag"); //"//lb.advsnx.net/asa/a.aspx?ZoneID=3242&Task=Get&SiteID=62&Random=[timestamp]&podSessionID=715952a4-48d4-4b99-b6fa-45e2588c90b5&uuid=4AckxmAcOrUpNKFy8bkENyZNqiH9qIifdH81R66goVFuSGjIGpuPnGGTf2%2bOW0NY";
                }
                /*else if (getAdType("In Board")) {
					ads.inBoard = new InstreamAd(InstreamAdTypes.INBOARD);
					//ads.inBoard.zoneTagURL = $AM(that).attr("Tag");
					ads.inBoard.zoneTagURL = "//lb.advsnx.net/asa/a.aspx?ZoneID=3410&Task=Get&SiteID=62&Random=[timestamp]&podSessionID=715952a4-48d4-4b99-b6fa-45e2588c90b5&uuid=4AckxmAcOrUpNKFy8bkENyZNqiH9qIifdH81R66goVFuSGjIGpuPnGGTf2%2bOW0NY";
				}*/

                //ads.inRead = new InstreamAd(InstreamAdTypes.INREAD);	
                //ads.inRead.zoneTagURL = "//lb.advsnx.net/asa/a.aspx?ZoneID=3242&Task=Get&SiteID=62&Random=[timestamp]&podSessionID=715952a4-48d4-4b99-b6fa-45e2588c90b5&uuid=4AckxmAcOrUpNKFy8bkENyZNqiH9qIifdH81R66goVFuSGjIGpuPnGGTf2%2bOW0NY";
            });

            if (hasValidAd === true) {
                $AM(that).trigger({
                    type: TagGetterEvents.TAG_ZONES_LOADED,
                    ads: ads,
                    xml: data,
                    mediaID: $AM(data).find('MediaInfo').attr("MediaID"),
                    targetTags: $AM(data).find('SiteInfo').attr("TargetTags"),
                    iScrollTarget: $AM(data).find('SiteInfo').attr("iScrollTarget"),
                    iScrollParagraphIndexTargetPosition: $AM(data).find('SiteInfo').attr("iScrollPosition"),
                    optionalSeparator: $AM(data).find('SiteInfo').attr("iScrollOptionalSeparator")
                });
            } else {
                $AM(that).trigger(TagGetterEvents.EMPTY_TAG_RESPONSE);
            }
        }).fail(function (error) {
            $AM(that).trigger(TagGetterEvents.EMPTY_TAG_RESPONSE);
        });
    }
};