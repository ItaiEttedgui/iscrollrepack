/*jslint browser: true, plusplus: true, bitwise: true, vars: true, debug: true, regexp: true*/
/*global $AM, Kjs, CookieHelper, artiMediaSiteKey, uuid, console, hasOwnProperty, artiDebug, ArtiMediaPageAds, jQuery,  TagGetter, VASTer, TagGetterEvents, VASTerEvents*/

//@prepros-prepend TagGetter.js
//@prepros-prepend VASTer.js
//@prepros-prepend Ad/Ad.js
//@prepros-prepend Ad/InstreamAd.js
//@prepros-prepend css.js
//@prepros-prepend ../Kjs.js
//@prepros-prepend ../CookieHelper.js

var AMTALOG_LOG = console.log;
var AMTALOG_INFO = console.info;
var AMTALOG_ERROR = console.error;
var AMTALOG_WARN = console.warn;

var QA_SERVER_PATH = "staging1.advsnx.net";
var PROD_SERVER_PATH = "lb.advsnx.net";
var AMTAH5_TITLE = "ArtiMedia iScroll desktop bundle";
var AMTAH5_VERSION = "1.0.5.60"; //TODO: Needs to be changed

var artiDebug = false; //TODO: change to false in prod!!!

var artiMediaPageAds;

function getQS(name) {
    "use strict";
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function isQA() {
    "use strict";
    return ((getQS('artiLog') !== null || getQS('artiQA') !== null) || artiDebug === true);
}

function hasValue(str) {
    "use strict";
    if (str !== undefined && str !== null && str !== "") {
        return true;
    } else {
        return false;
    }
}

function logAMTA(method) {
    "use strict";
    if (isQA()) {
        var argumentsToPass = ["AMTALog:"],
            i = 0;

        for (i = 1; i < arguments.length; i++) {
            argumentsToPass.push(arguments[i]);
        }

        method.apply(console, argumentsToPass);
    }
}


function getTopmostLocation() {
    "use strict";
    var result;
    try {
        result = parent.location.href;
    } catch (e) {
        result = location.href;
    }
    if (!hasValue(result)) {
        return location.href;
    }
    return result;
}

function initArtiMediaPageAds() {
    "use strict";


    //if ($AM('[data-adEnabled="true"]').length > 0) {
    logAMTA(AMTALOG_LOG, "initArtiMediaPageAds()");

    var pArtiMediaSiteKey,
        i,
        proceedLoading = false;

    if (window.hasOwnProperty("artiMediaSiteKey") && hasValue(artiMediaSiteKey)) {
        pArtiMediaSiteKey = artiMediaSiteKey;
    } else {
        logAMTA(AMTALOG_WARN, "No site key variable was defined for iScroll (var artiMediaSiteKey), iScroll loading cancelled");
        return false;
    }

    var UAExcluded = false;
    var skipCatAndEXUALoading = false;

    if (hasValue($AM)) {
        if (pArtiMediaSiteKey.search("yad2") > -1) {
            skipCatAndEXUALoading = true;
        }

        var onGetEXUAComplete = function () {
            if (!UAExcluded) {
                var paramsObject = {};
                paramsObject.category = window.hasOwnProperty("artiMediaCategory") && hasValue(window.artiMediaCategory) ? window.artiMediaCategory : '';
                paramsObject.siteKey = pArtiMediaSiteKey;

                logAMTA(AMTALOG_INFO, "artiParams.category: " + paramsObject.category);

                var onGetCatComplete = function () {
                    function isMobile() {
                        var check = false;
                        var a = (navigator.userAgent || navigator.vendor || window.opera);
                        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
                            check = true;
                        }
                        return check;
                    }

                    if (!paramsObject.hasOwnProperty("newSiteKey")) {
                        paramsObject.newSiteKey = paramsObject.siteKey;
                    }

                    if (isMobile()) {
                        logAMTA(AMTALOG_LOG, "Mobile environment detected");
                        if (paramsObject.newSiteKey.substr(0, 2) !== "m.") {
                            logAMTA(AMTALOG_LOG, "Provided siteKey (either through the client or the siteKey mapping file) is missing 'm.' prefix, adding it now.");
                            paramsObject.newSiteKey = "m." + paramsObject.newSiteKey;
                        }
                    } else {
                        logAMTA(AMTALOG_LOG, "Desktop environment detected");
                        if (paramsObject.newSiteKey.substr(0, 2) === "m.") {
                            logAMTA(AMTALOG_LOG, "Provided siteKey (either through the client or the siteKey mapping file) contains a redundant 'm.' prefix, removing it now.");
                            paramsObject.newSiteKey = paramsObject.newSiteKey.substr(2);
                        }
                    }
                    logAMTA(AMTALOG_LOG, "Finalized SiteKey (newSiteKey) : " + paramsObject.newSiteKey);
                    var vIScrollAuto = false,
                        vIScrollTarget = null;

                    artiMediaPageAds = new ArtiMediaPageAds(paramsObject.newSiteKey, window.iScrollAuto, window.iScrollTarget);
                };
                if (skipCatAndEXUALoading) {
                    onGetCatComplete();
                } else {
                    $AM.ajax({
                        type: "GET",
                        url: "//akamai.advsnx.net/CDN/" + paramsObject.siteKey + "/" + paramsObject.siteKey + "-cat.xml",
                        dataType: "xml",
                        success: function (data) {
                            $AM(data).find('SiteLink').each(function () {
                                var that = this;
                                if ($AM(that).find("category") && hasValue($AM(that).find("category").text())) {
                                    if (paramsObject.category.toLowerCase().search($AM(that).find("category").text().trim().toLowerCase()) > -1) {
                                        paramsObject.newSiteKey = $AM(this).find("siteKey").text().trim();
                                        logAMTA(AMTALOG_INFO, "siteKey changed by category to '" + paramsObject.newSiteKey + "'");
                                    }
                                } else if ($AM(that).find("url") && hasValue($AM(that).find("url").text())) {
                                    if (getTopmostLocation().search($AM(that).find("url").text().trim()) > -1) {
                                        paramsObject.newSiteKey = $AM(this).find("siteKey").text().trim();
                                        logAMTA(AMTALOG_INFO, "no category, siteKey changed by url to '" + paramsObject.newSiteKey + "'");
                                    }
                                }

                                if (paramsObject.hasOwnProperty("newSiteKey")) {
                                    return false;
                                }
                            });

                            if (!paramsObject.hasOwnProperty("newSiteKey")) {
                                var defaultSiteKey = $AM(data).find("default").text().trim();
                                paramsObject.newSiteKey = hasValue(defaultSiteKey) ? defaultSiteKey : paramsObject.siteKey;
                                logAMTA(AMTALOG_INFO, "No matching category nor URL found, siteKey changed to default ('" + paramsObject.newSiteKey + "')");
                            }
                        },
                        error: function (error) {
                            if (!paramsObject.hasOwnProperty("newSiteKey")) {
                                paramsObject.newSiteKey = paramsObject.siteKey;
                                logAMTA(AMTALOG_WARN, "Error loading siteKey mapping file, using the siteKey provided in 'artiParams': " + paramsObject.newSiteKey);
                            }
                        },
                        complete: onGetCatComplete
                    });
                }
            }
        };

        if (skipCatAndEXUALoading) {
            onGetEXUAComplete();
        } else {
            $AM.ajax({
                type: "GET",
                url: "//akamai.advsnx.net/CDN/" + pArtiMediaSiteKey + "/" + pArtiMediaSiteKey + "-exUA.xml",
                dataType: "xml",
                success: function (data) {
                    $AM(data).find("UA").each(function () {
                        var matchingAttributes = 0;
                        for (i = 0; i < this.attributes.length; i++) {
                            if (window.navigator.userAgent.search(this.attributes[i].value) > -1) {
                                matchingAttributes++;
                            }
                        }
                        if (matchingAttributes === this.attributes.length) {
                            logAMTA(AMTALOG_LOG, "You current user agent is not supported by ArtiMedia SDK, destroying the current artimedia instance and returning to playback.");
                            UAExcluded = true;
                            return false;
                        }
                    });
                },
                error: function (error) {
                    logAMTA(AMTALOG_WARN, "Error loading User Agent Exclusion list, proceeding as normal.");
                },
                complete: onGetEXUAComplete
            });
        }
    }
}

function getServerPath() {
    "use strict";
    return ((getQS('artiQA') !== null) ? QA_SERVER_PATH : PROD_SERVER_PATH);
}

var ArtiMediaPageAds = function (siteKey, iScrollAuto, externalIScrollTarget) {
    "use strict";
    logAMTA(AMTALOG_LOG, "ArtiMediaPageAds constructor()");
    var that = this;

    this.iScrollAuto = iScrollAuto;
    this.externalIScrollTarget = externalIScrollTarget;
    this.siteKey = siteKey;
    this.tagGetter = new TagGetter();
    this.vaster = new VASTer();
    if (window.hasOwnProperty("amIScrollExternalControl") && amIScrollExternalControl === true) {
        this.amIScrollExternalControl = true;
    }

    //$AM('html > head').append(window.getAMCSS());

    var topIfr = ($AM("div")[0].ownerDocument.parentWindow || $AM("div")[0].ownerDocument.defaultView);
    if (topIfr.hasOwnProperty("inDapIF") && topIfr.inDapIF === true) {
        this.inDapIF = true;
        this.topWindow = window.parent;
        this.parentIframe = topIfr.frameElement;
        this.target = this.topWindow;
    } else {
        this.target = window;
    }

    //this.target = ($AM(this.target).height() > $AM(document).height()) ? this.target : document;
    //console.log(target);

    //console.log(target);
    if (!this.amIScrollExternalControl) {
        $AM(this.target).scroll(this.onWindowScroll);
    }
    $AM(this.target).focus(this.onWindowFocus);
    $AM(this.target).blur(this.onWindowFocus);
    //$AM(window).focus(this.onWindowFocus);
    //$AM(window).blur(this.onWindowFocus);
    that.getTag();


};

ArtiMediaPageAds.prototype = {
    el: null,
    amAdIfr: null,
    amAdIfrContent: null,
    v: null,
    target: null,
    //windowScrollInterval: null,
    externalIScrollTarget: null,
    isAdActive: false,
    activeAdElement: null,
    tagGetter: null,
    vaster: null,
    uuid: null,
    siteKey: null,
    videoAspectRatio: (16 / 9),
    completeViewReported: false,
    minTimeView: 30,
    queueQuartilesPopulation: false,
    clickThroughHappened: false,

    inDapIF: false,
    parentIframe: null,
    topWindow: null,
    queuedAdURL: null,
    amIScrollExternalControl: null,

    updateProgress: function (percent, $element) {
        "use strict";
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').css({
            width: progressBarWidth
        });
    },

    w: null,

    playIScroll: function () {
        if (this.v && this.v.paused) {
            this.resumeAd(this.activeAdElement);
        } else {
            this.activeAdElement = this.el = this.findParentElement($AM('[data-adEnabled="true"]'));
            this.isAdActive = true;
            this.playAd(this.queuedAdURL);
        }
    },

    pauseIScroll: function () {
        if (!this.v.paused) {
            this.pauseAd(this.activeAdElement);
        }
    },

    isHalfwayViewable: function (elem) {
        //New method, fixes the slowness and wrong height/offset values at yummies and reshset
        "use strict";
        if (this.w === null) {
            this.w = $AM(window);
        }
        var w = this.w;

        if (this.inDapIF) {
            elem = $AM(this.parentIframe);
            w = $AM(this.topWindow);
        }

        var docViewTop = w.scrollTop();
        var docViewBottom = docViewTop + w[0].innerHeight; //used to be docViewTop + w.height() but the final calculation was wrong
        var elemHeight = elem.height(); //this.isAdActive ? elem.height() : 1;
        var elemTargetHeight = ((elem.width() > 0 ? elem.width() : elem.parent().width()) / this.videoAspectRatio) + 22;
        var elemTop = elem.offset().top;
        var elemBottom = elemTop + elemHeight;

        if (this.isAdActive) {
            //logAMTA(AMTALOG_LOG, "Checking whether to pause the ad due to lack of visibility");
            return ((elemBottom - elemHeight * 0.25 <= docViewBottom) && (elemTop + elemHeight * 0.25 >= docViewTop));
        } else {
            //logAMTA(AMTALOG_LOG, "Checking whether 75% of the ad's appointed vertical area is visible so we can start it");
            elemBottom = elemTop + (elemTargetHeight * 0.50);
            var min = docViewTop; // - elemTargetHeight/2;
            var max = docViewBottom; // + elemTargetHeight/2;
            return (elemTop > min) && (elemBottom < max);

            //return ((elemBottom - elemTargetHeight / 2 <= docViewBottom) && (elemTop + elemTargetHeight / 2 >= docViewTop)); //TODO: Old calculation.. see if the one above doesn't ruin existing integrations
        }
    },

    adStarted: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "adStarted()");
        var that = this;
        var toWidth = "100%";

        var toHeight = (Math.round(((that.el.width() || that.el.parent().css("width").replace("px", "")) / that.videoAspectRatio)) + 29) + "px";
        this.showIScrollElements();

        var pxToDeduct = 3;
        if (this.inDapIF) {
            //var toHeightIfr = Math.round((this.parentIframe.width / that.videoAspectRatio)) + 29 + "px";
            //var toHeightIfr = $AM(this.amAdIfrContent.document.body).find("#artiAdContainer").height() + "px";
            pxToDeduct = 0;
            var toHeightIfr = Math.round(($AM(window.frameElement).parent().width() / that.videoAspectRatio + 3)) + "px";
            $AM(this.parentIframe).css({
                width: "100%"
            });

            $AM(this.parentIframe).animate({
                height: toHeightIfr
            }, 500, function () {
                $AM(this.parentIframe).css({
                    backgroundColor: "#000000",
                    height: toHeightIfr
                });
                $AM(that.parentIframe).attr("height", toHeightIfr); //(Math.round(that.el.parent().css("width").replace("px", "") / that.videoAspectRatio)));
            });
        }

        that.el.css({
            position: 'relative',
            width: "100%",
            direction: "ltr",
            height: 0,
            display: 'block'
        });

        that.amAdIfr.animate({
            width: toWidth,
            height: that.amAdIfr.contents().height() - pxToDeduct + "px"
        }, 500);

        that.el.animate({
            width: toWidth,
            height: that.amAdIfr.contents().height() - pxToDeduct + "px"
        }, 500, function () {

            /*$AM(that.el).prepend('<div id="AM_adHeader" align="left" style="vertical-align:middle;">' +
                '<div style="font-size: 10px; font-family: Arial; float: left; color:#ffffff; margin:3px 0 0 5px;">Ad by <b style="letter-spacing: -0.5px;">a r t i m e d i a</b></div>' + '<div style="float:left; margin: 3px 0 0 5px;"><img src="//akamai.advsnx.net/CDN/ta-extra/small.png" /></div>' +
                '</div>');
*/
            $AM(that.el).css({
                height: that.amAdIfr.contents().height() - pxToDeduct + "px"
            });

            that.el.css("height", that.amAdIfr.contents().height() - pxToDeduct + "px"); // Fixes the ui problems
        });

        that.vaster.trackImpression();
        that.vaster.adStarted();

        // Call developer SDK event function 1.0.5.17 @Shmuel
        window.AMSDK.onAdStart();

        if (that.vaster.clickThroughEnabled) {
            $AM(that.v).css('cursor', 'pointer');
        }
    },

    adEnded: function () {
        "use strict";
        var that = this;
        //window.AMSDK.onAdComplete();
        logAMTA(AMTALOG_LOG, "adEnded()");

        if (that.inDapIF) {
            $AM(this.parentIframe).animate({
                height: 0
            }, 500);
        }

        that.el.animate({
            height: 0
        }, 500);
        $AM("#AM_adHeader").animate({
            height: 0
        }, 400, function () {
            $AM("#AM_adHeader").remove();
            $AM('[data-adEnabled]').remove();
        });

        that.isAdActive = false;
        that.vaster.adEnded();

        that.destroyVideo();
        that.destroyActiveAdElement();


        if ($AM('[data-adEnabled="true"]').length === 0) {
            if (that.activeAdElement) {
                that.activeAdElement.style.display = "none";
            }
            that.unload();
        }

        // Wait for animation finish. 1.0.5.19 @Shmuel
        setTimeout(function () {
            window.AMSDK.onAdComplete();
        }, 500);
    },

    unload: function () {
        "use strict";
        logAMTA(AMTALOG_INFO, "Unloading iScroll!");

        //if (this.inDapIF) {
        //$AM(this.topWindow).off("scroll", this.onWindowScroll);
        //$AM(this.topWindow).off("focus", this.onWindowFocus);
        $AM(this.target).off("focus", this.onWindowFocus);
        $AM(this.target).off("blur", this.onWindowFocus);
        $AM(this.target).off("scroll", this.onWindowScroll);
        //} else {
        //$AM(window).off("scroll", this.onWindowScroll);
        //$AM(window).off("focus", this.onWindowFocus);
        //$AM(window).off("blur", this.onWindowFocus);
        //}

        // Call developer SDK event function 1.0.5.17 @Shmuel
        // Send onAdComplete on any completion of ad cycle

        //window.AMSDK.onAdComplete();
        artiMediaPageAds = null;
    },

    adError: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "adError()");
        this.isAdActive = false;
    },

    findParentElement: function (inElement) {
        "use strict";
        var outElement = inElement;

        if (outElement.width() < 1) {
            while (outElement.parent().width() < 1) {
                outElement = outElement.parent();
            }
        }
        return outElement;

    },

    onWindowScroll: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "onWindowScroll");
        var that = artiMediaPageAds,
            targetElementToCheck = null;

        if (that.inDapIF) {
            targetElementToCheck = this.parentIframe;
        }
        if (!that.isAdActive) {
            $AM('[data-adEnabled="true"]').each(function () {
                if (that.isAdActive) {
                    return false; //each loop "break"
                }

                if (!that.inDapIF) {
                    targetElementToCheck = that.findParentElement($AM(this));
                }
                if (that.isHalfwayViewable(targetElementToCheck) && that.activeAdElement !== $AM(this) && hasValue(that.queuedAdURL)) {
                    that.activeAdElement = that.el = that.findParentElement($AM(this));
                    that.isAdActive = true;
                    logAMTA(AMTALOG_INFO, "halfway visible! shouldn't be here twice!");

                    that.playAd(that.queuedAdURL);
                    //that.loadAd();
                }

            });
        } else {
            if (that.v) {
                if (!that.inDapIF) {
                    targetElementToCheck = that.activeAdElement;
                }
                if (!that.isHalfwayViewable(targetElementToCheck)) {
                    if (!that.v.paused) {
                        that.pauseAd(that.activeAdElement);
                    }
                } else if (that.v.paused) {
                    that.resumeAd(that.activeAdElement);
                }
            }
        }
    },

    onWindowFocus: function () {
        "use strict";
        var that = artiMediaPageAds;
        logAMTA(AMTALOG_LOG, "got focus");
        if (that.v && that.v.paused && that.isHalfwayViewable(that.el)) {
            that.resumeAd();
            that.v.muted = true;
            $AM(".AM_muted").show();
        }
        //	console.log("onFocus");
        //	console.log(that);
        //onWindowScroll();
    },

    /*countVideoView: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "countVideoView()");

        var that = this;
        var countVideoViewURL = "//" + getServerPath() + "/AdServer/Service.svc/cvv?" +
            "sk=" + this.siteKey +
            "&EMD=" + this.externalMediaID +
            "&cb=" + (Math.floor(Math.random() * 9000000000000) + 1000000000000);

        if (that.siteKey.search("walla") > -1) {
            var wallaImpressionURL = "//fus.walla.co.il/impression/xnzvw/" + that.mediaZone + "/art_l_res/3848360411?" + //"//fus.walla.co.il/impression/xnzvw/mint.new_walla.advertorial/art_l_res/3848360411?" +
                "affiliate=" + that.affiliateID +
                "&custom2=" + that.externalMediaID;

            $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + wallaImpressionURL + '"/>');
            var trackFUSURL = "//logger.logidea.info:8080/?t=vr&cs=s&sk=wjw." + window.location.host + "&cb" + (new Date().getTime());
            $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + trackFUSURL + '"/>');
        }

        $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + countVideoViewURL + '"/>');

        var trackCVVURL = "//logger.logidea.info:8080/?t=vv&cs=c&sk=wjw." + window.location.host + "&cb" + (new Date().getTime());
        $AM(".AM_pixelsHolder").append('<img class="AM_pixel" src="' + trackCVVURL + '"/>');
    },*/

    initVideoEvents: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "initVideoEvents()");
        this.v.onloadstart = this.onAdVideoEvent;
        this.v.ondurationchange = this.onAdVideoEvent;
        this.v.onplaying = this.onAdVideoEvent;
        this.v.ontimeupdate = this.onAdVideoEvent;
        this.v.onended = this.onAdVideoEvent;
    },

    initActiveAdElementEvents: function () {
        "use strict";
        //$AM(this.amAdIfrContent.document.body).find("iframe").contents().find(".cover").click(this.onAdVideoEvent);

        $AM(this.amAdIfrContent.document.body).click(this.onAdVideoEvent);
        $AM(this.activeAdElement).mouseenter(this.onAdVideoEvent);
        $AM(this.activeAdElement).mouseleave(this.onAdVideoEvent);
    },

    destroyVideo: function () {
        "use strict";
        logAMTA(AMTALOG_INFO, "destroying the active ad element's video");
        this.v.onloadstart = this.v.onclick = this.v.ondurationchange = this.v.onplaying = this.v.ontimeupdate = this.v.onended = this.v.onmouseenter = this.v.onmouseleave = null;
        this.v = null;
    },

    destroyActiveAdElement: function () {
        "use strict";
        logAMTA(AMTALOG_INFO, "destroying the active ad element's iframe and container");
        //$AM(this.amAdIfrContent.document.body).find("iframe").contents().find(".cover").unbind("click");
        $AM(this.amAdIfr).unbind("click");
        $AM(this.activeAdElement).unbind("mouseenter");
        $AM(this.activeAdElement).unbind("mouseleave");
        this.activeAdElement.html("");
        this.activeAdElement = null;
    },

    onAdVideoEvent: function (event) {
        "use strict";
        var that = artiMediaPageAds;

        switch (event.type) {
        case "loadstart":
            break;
        case "durationchange":
            that.videoAspectRatio = that.v.videoWidth / that.v.videoHeight;
            if (that.v.duration > 0) {
                that.vaster.populateQuartiles(that.v.duration);
            } else {
                that.queueQuartilesPopulation = true;
            }
            break;
        case "click":
            if (that.isHalfwayViewable(that.el) && that.clickThroughHappened && that.v.paused) {
                that.clickThroughHappened = false;
                return that.resumeAd();
            }
            $AM(that.target).focus();
            that.clickThroughHappened = true;

            setTimeout(function () {
                that.pauseAd();
            }, 100);

            that.vaster.adClicked(that.v.currentTime);
            //}
            break;
        case "playing":
            that.v.onplaying = null;
            that.activeAdElement.attr("data-adEnabled", false);
            that.adStarted();
            break;
        case "timeupdate":
            that.onTimeUpdate();
            break;
        case "ended":
            that.isAdActive = false;
            setTimeout(function () {
                that.adEnded();
            }, 2000);
            break;
        case "mouseenter":
            that.v.muted = false;
            if (!that.v.paused) {
                $AM(".AM_muted").hide();
            }
            break;
        case "mouseleave":
            that.v.muted = true;
            $AM(".AM_muted").show();
            break;
        }
    },

    playAd: function (url) {
        "use strict";
        logAMTA(AMTALOG_LOG, "playAd()");

        //this.v = $AM(this.amAdIfrContent.document.body).find("iframe").contents().find("video")[0];
        this.v = $AM(this.amAdIfrContent.document.body).find("video")[0];

        this.initVideoEvents();
        this.initActiveAdElementEvents();

        this.v.src = url;
        logAMTA(AMTALOG_INFO, "video source set");

        // Display the container only on successfull ad
        $AM(this.amAdIfrContent.document.body).find("#artiAdContainer").css({
            "display": "block"
        });

        this.v.muted = true;
        this.v.play();
    },

    onTimeUpdate: function () {
        "use strict";

        this.updateProgress((this.v.currentTime / this.v.duration) * 100, this.amAdIfr.contents().find(".AM_progressBar"));
        if (this.v.currentTime > 0.2) {
            if (parseInt(this.v.currentTime, 10) === this.minTimeView && !this.completeViewReported) {
                this.completeViewReported = true;
                this.vaster.completeView(this.v.currentTime);
            }
            if (this.queueQuartilesPopulation === true) {
                this.queueQuartilesPopulation = false;
                this.vaster.populateQuartiles(this.v.duration);
            }
            this.vaster.checkForQuartileTracking(this.v.currentTime);
        }

    },

    pauseAd: function (activeAdElement) {
        "use strict";
        logAMTA(AMTALOG_LOG, "pauseAd()");
        if (artiMediaPageAds.isAdActive && !artiMediaPageAds.v.paused) {
            artiMediaPageAds.v.pause();
        }
    },

    resumeAd: function (activeAdElement) {
        "use strict";
        logAMTA(AMTALOG_LOG, "resumeAd()");
        if (artiMediaPageAds.isAdActive && artiMediaPageAds.v.paused) {
            artiMediaPageAds.v.play();
        }
    },

    getTag: function () {
        "use strict";
        var that = this;
        logAMTA(AMTALOG_LOG, "getTag()");
        $AM(this.tagGetter).on(TagGetterEvents.TAG_ZONES_LOADED, function (ev) {
            that.tagZonesLoaded(ev);
        });
        $AM(this.tagGetter).on(TagGetterEvents.EMPTY_TAG_RESPONSE, function () {
            logAMTA(AMTALOG_ERROR, "Failed GetNewTags response, Destroying!");
            window.AMSDK.onPassBack();
            that.unload();
        });

        this.tagGetter.getNewTag(this.siteKey, window.amLoadedUID, AMTAH5_VERSION);
    },

    loadAd: function () {
        "use strict";
        logAMTA(AMTALOG_LOG, "loadAd()");
        if (this.ads) {
            var artipreview = getQS("artipreview");
            this.vaster.loadVAST((this.ads.iScroll.zoneTagURL).replace(/&amp;/g, '&').replace("[timestamp]", (Math.floor(Math.random() * 9000000000000) + 1000000000000)) + (hasValue(artipreview) ? ("&artipreview=" + artipreview) : ''));
        } else {
            setTimeout(this.loadAd, 250);
        }
    },

    tagZonesLoaded: function (event) {
        "use strict";
        var that = this;

        var targetIScrollContainer = hasValue(event.iScrollTarget) ? event.iScrollTarget : (hasValue(this.externalIScrollTarget) ? this.externalIScrollTarget : null);

        var success = this.initUI(targetIScrollContainer, hasValue(event.iScrollParagraphIndexTargetPosition) ? parseInt(event.iScrollParagraphIndexTargetPosition, 10) : 3, event.optionalSeparator, event.logoSize);
        if (!success) {
            window.AMSDK.onPassBack();
            return this.unload();
        }

        logAMTA(AMTALOG_LOG, "tagZonesLoaded()");

        this.vaster = new VASTer();

        $AM(this.vaster).on(VASTerEvents.VAST_VIDEO_URI_READY, function (ev) {
            logAMTA(AMTALOG_LOG, "VAST Loaded");
            that.queuedAdURL = ev.url.trim();
            window.AMSDK.onIScrollReady();
            if (!that.amIScrollExternalControl) {
                that.onWindowScroll();
            }
        });

        $AM(this.vaster).on(VASTerEvents.EMPTY_VAST_RESPONSE, function (ev) {
            logAMTA(AMTALOG_ERROR, "Empty VAST tag response, Aborting!");
            window.AMSDK.onPassBack();
            that.unload();
            return null;
        });

        this.ads = event.ads;
        this.loadAd();
    },

    hideIScrollElements: function () {
        "use strict";
        $AM('[data-adEnabled]').css("opacity", 0);
    },

    showIScrollElements: function () {
        "use strict";
        $AM('[data-adEnabled]').css("opacity", 1);
    },

    initUI: function (targetContainerID, targetParagraph, optionalSeparator, logoSize) {
        "use strict";
        if (!hasValue(logoSize)) {
            logoSize = "large";
        }
        if (this.iScrollAuto === "true" || this.iScrollAuto === true) { //First integration method - Get the target position for the iScroll from GNT
            if (!hasValue(optionalSeparator)) {
                optionalSeparator = "p";
            }

            /*var targetContainer = $AM('<' + optionalSeparator + '></' + optionalSeparator + '>').insertAfter($AM(targetContainerID + " " + optionalSeparator + ":nth-of-type(" + targetParagraph + ")").first());
            targetContainer.append($AM('<div id="iScrollContainer" data-adEnabled="true" data-adType="iScroll"></div>'));*/
            var targetContainer = '<' + optionalSeparator + '><div id="iScrollContainer" data-adEnabled="true" data-adType="iScroll" style="clear:both"></div></' + optionalSeparator + '>';
            var placeHolder = $AM(targetContainerID).find(optionalSeparator)[targetParagraph];
            if (!hasValue(placeHolder)) {
                logAMTA(AMTALOG_ERROR, "iScrollAuto integration has been used, but mo matching container (CP: 'iScrollTarget') or sub-element (CP: 'iScrollOptionalSeparator', default = 'p') with its defined position (CP: 'iScrollPosition', default: 3) has been found.");
                return false;
            }
            $AM(targetContainer).insertAfter(placeHolder);

        } else if ($AM('[data-adEnabled="true"]').length < 1) { //Second integration method - If no designated container was defined by the publisher (element with data-adEnabled="true") - we create it on the fly at the script location and then procceed as if a designated container was created by the publisher
            var currentScript = $AM("script[src$='AMInPageAds.js']");
            // Check backwards compatibility

            if (currentScript.length < 1) {
                currentScript = $AM("script[src$='amloader.js']");
            }

            $AM('<div id="ad2" data-adEnabled="true" data-adType="iScroll"></div>').insertAfter($AM(currentScript));
        }


        var getIframeWindow = function (iframe_object) {
            var doc;

            if (iframe_object.contentWindow) {
                return iframe_object.contentWindow;
            }

            if (iframe_object.window) {
                return iframe_object.window;
            }

            if (!doc && iframe_object.contentDocument) {
                doc = iframe_object.contentDocument;
            }

            if (!doc && iframe_object.document) {
                doc = iframe_object.document;
            }

            if (doc && doc.defaultView) {
                return doc.defaultView;
            }

            if (doc && doc.parentWindow) {
                return doc.parentWindow;
            }

            return undefined;
        };

        var setDomain = 'src="javascript:document.write(\'' + encodeURIComponent('<scr' + 'ipt>document.domain="' + document.domain + '"</sc' + 'ript>') + '\')"';
        if (this.siteKey.search("yad2") > -1) {
            setDomain = "";
        }
        this.amAdIfr = $AM('<iframe id="AMiScrollIframe" width="100%" style="' +
            ' position: absolute;' +
            ' top: 0; ' +
            ' left: 0; ' +
            ' width: 100%; ' +
            ' overflow: hidden; ' +
            ' border: 0; ' +
            ' outline: none; ' +
            ' height: 0;" ' +
            ' scrolling="no" ' +
            ' ' + setDomain + '></iframe>');
        $AM('[data-adEnabled="true"]').append(this.amAdIfr);

        this.amAdIfrContent = getIframeWindow(this.amAdIfr[0]);
        if (!hasValue(this.amAdIfrContent)) {
            logAMTA(AMTALOG_ERROR, "No matching container for iScroll has been found.");
            return false;
        }

        var amPageInnerHTMLString = '<html><head><style>' +
            '	.artiMediaAdCover {' +
            '		width: 100%;' +
            '		height: 100%;' +
            '		top: 0;' +
            '		left: 0;' +
            '		position: absolute;' +
            '		cursor: pointer;' +
            '	}' +
            '</style></head><body style="margin: 0;padding: 0;"><div id="artiAdContainer" style="position:absolute;width:100%;display:none">' +
            '<div id="AM_adHeader" align="left" style="vertical-align:middle;">' +
            '<div style="font-size: 10px; font-family: Arial; float: left; color:#ffffff; margin:5px 0 0 6px;">Ad by <b style="letter-spacing: -0.5px;">a r t i m e d i a</b></div>' + '<div style="float:left; margin: 5px 0 0 4px;"><img src="//akamai.advsnx.net/CDN/ta-extra/small.png" /></div>' +
            '</div>' +
            '<video width="100%" src="//akamai.advsnx.net/CDN/ta-extra/artimediasdk.mp4"></video>' +
            '<div class="artiMediaAdCover"></div>' +
            //'<span class="AM_muted"></span>' +
            '<div id="AM_logo" ' + (logoSize.toLowerCase() === "large" ? "class='AM_logo_large'" : '') + ' ></div>' +
            '<div class="AM_progressBar"><div></div></div></div></body></html>';
        this.amAdIfrContent.document.write(amPageInnerHTMLString);
        this.amAdIfrContent.document.close();

        $AM(this.amAdIfrContent.document.head).append(window.getAMCSS());

        if (!this.inDapIF) {
            $AM(this.amAdIfrContent.document).find(".AM_progressBar").css("bottom", "3px");
        }
        logoSize = (hasValue(logoSize) ? logoSize : "normal");
        /*$AM(this).html('<iframe class="noScroll noBorder" width="100%" height="100%" border="no" scrolling="no" seamless="seamless" src="' + SOURCES_PATH + '/ipa_vastMGR.html"></iframe>' +
            '<span class="AM_muted"></span>' +
            '<div class="AM_progressBar"><div></div></div>');*/

        $AM('[data-adEnabled="true"]').css({
            backgroundColor: "black",
            display: "block",
            height: "1px"
        });

        this.hideIScrollElements();

        if ($AM('[data-adEnabled="true"]').length === 0) {
            logAMTA(AMTALOG_ERROR, "No matching container for iScroll has been found.");
            return false;
        }
        return true;
    }
};

(function () {
    "use strict";
    window.amLoadedUID = null;
    logAMTA(AMTALOG_INFO, "ArtiMedia iScroll V" + AMTAH5_VERSION + " starting");
    var script = document.createElement("SCRIPT");
    script.src = '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existence
    var checkReady = function (callback) {
        if (window.jQuery && window.jQuery.fn.jquery === "2.1.4") {
            callback(jQuery);
        } else {
            window.setTimeout(function () {
                checkReady(callback);
            }, 250);
        }
    };

    // Start polling...
    checkReady(function ($) {
        window.$AM = jQuery.noConflict(true);
        $AM.ajaxSetup({
            cache: true
        });

        var uidCookie = CookieHelper.getCookieUUID();
        // if cookie exist than current UUID was generated using latest k.html (after change from direct call to k.js) so no need to go again. otherwise do the k.html process
        if (hasValue(uidCookie && uidCookie !== 'undefined')) {
            window.amLoadedUID = uidCookie;
        } else {
            Kjs.getUUID(window, function (uuid) {
                window.amLoadedUID = uuid;
                CookieHelper.setCookieUUID(uuid);
            });
        }

        $AM.ajaxSetup({
            cache: false
        });

        var msPassedSinceUUIDRequest = 0;

        function procceedToInit() {
            if (hasValue(window.amLoadedUID) || msPassedSinceUUIDRequest > 9000) {
                initArtiMediaPageAds();
            } else {
                msPassedSinceUUIDRequest += 1000;
                setTimeout(procceedToInit, 1000);
            }
        }

        procceedToInit();
    });
}());

var $AM;