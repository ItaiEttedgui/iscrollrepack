/*jslint browser: true, vars: true, forin: true*/
/*global logAMTA, AMTALOG_LOG, AMTALOG_WARN, $AM, jQuery, console, hasValue*/

var VASTer = function () {
    "use strict";
};

var VASTerEvents = {
    VAST_VIDEO_URI_READY: "VASTVideoURIReady",
    VAST_TRACKER_REPORT_FAILED: "VASTTrackerReportFailed",
    EMPTY_VAST_RESPONSE: "emptyVASTResponse"
};
VASTer.prototype = {
    adVASTTrackers: {
        start: [],
        ClickThrough: [],
        ClickTracking: [],
        skipped: [],
        firstQuartile: [],
        midpoint: [],
        thirdQuartile: [],
        completeview: [],
        complete: [],
        Impression: []
    },
    skippable: false,
    quartiles: null,
    currentQuartile: 0,
    clickThroughEnabled: true,

    populateQuartiles: function (duration) {
        "use strict";
        logAMTA(AMTALOG_LOG, "populateQuartiles()");
        var quartileInterval = parseInt(duration / 4, 10);
        this.quartiles = [{
            time: quartileInterval,
            trackingEventName: "firstQuartile"
        }, {
            time: quartileInterval * 2,
            trackingEventName: "midpoint"
        }, {
            time: quartileInterval * 3,
            trackingEventName: "thirdQuartile"
        }];
    },

    reportTrackers: function (trackerURIsNode, eventName) {
        "use strict";
        var i = 0;
        for (i; i < trackerURIsNode.length; i = i + 1) {
            if (hasValue(trackerURIsNode[i])) {
                this.reportVASTEvent(trackerURIsNode[i]);
            }
        }

        logAMTA(AMTALOG_LOG, "Reporting ", eventName);
    },

    reportVASTEvent: function (trackerURL) {
        "use strict";
        var that = this;
        trackerURL = trackerURL.replace("[timestamp]", (Math.floor(Math.random() * 9000000000000) + 1000000000000));
        trackerURL = trackerURL.replace("{PingTime}", this.currentTime);

        $AM.ajax({
            type: "GET",
            url: trackerURL,
            dataType: "xml"
        })
            .done(function (e) {})
            .fail(function (e) {
                $AM(that).trigger(VASTerEvents.VAST_TRACKER_REPORT_FAILED);
            });
    },

    skipClicked: function (time) {
        "use strict";
        var currentTime = time, // = parseInt(time, 10),
            i = 0;
        var skippedTrackers = [];
        for (i; i < this.adVASTTrackers.skipped.length; i = i + 1) {
            skippedTrackers.push(this.adVASTTrackers.skipped[i].replace("{PingTime}", parseInt(currentTime * 1000, 10)));
            //this.adVASTTrackers.skipped[i] = this.adVASTTrackers.skipped[i].replace("{PingTime}", currentTime);
        }
        this.reportTrackers(skippedTrackers, "skipped");
        this.currentQuartile = 0;
    },

    completeView: function (time) {
        "use strict";
        var currentTime = time, // = parseInt(time, 10),
            i = 0;
        var completeViewTrackers = [];
        for (i; i < this.adVASTTrackers.completeview.length; i = i + 1) {
            completeViewTrackers.push(this.adVASTTrackers.completeview[i].replace("{PingTime}", parseInt(currentTime * 1000, 10)));
            //this.adVASTTrackers.completeview[i] = this.adVASTTrackers.completeview[i].replace("{PingTime}", currentTime);
        }
        this.reportTrackers(completeViewTrackers, "completeView");
        //this.reportTrackers(this.adVASTTrackers.completeview, "completeView");
        //this.reportTrackers(this.adVASTTrackers.completeview, "completeView");
    },
    adStarted: function () {
        "use strict";
        this.reportTrackers(this.adVASTTrackers.start, "start");
    },
    adEnded: function () {
        "use strict";
        this.reportTrackers(this.adVASTTrackers.complete, "complete");
        this.currentQuartile = 0;
    },
    adClicked: function (time) {
        "use strict";
        var currentTime = time,
            i = 0;

        var clickTrackers = [];
        for (i; i < this.adVASTTrackers.ClickTracking.length; i = i + 1) {
            clickTrackers.push(this.adVASTTrackers.ClickTracking[i].replace("{PingTime}", parseInt(currentTime * 1000, 10)));
        }

        this.reportTrackers(clickTrackers, "ClickTracking");

        i = 0;
        for (i; i < this.adVASTTrackers.ClickThrough.length; i = i + 1) {
            if (this.adVASTTrackers.ClickThrough[i] !== "") {
                window.open(this.adVASTTrackers.ClickThrough[i]);
            }
        }

    },
    checkForQuartileTracking: function (time) {
        "use strict";
        var i;
        if (this.quartiles) {
            for (i = 0; i < this.quartiles.length; i = i + 1) {
                if (this.quartiles[i].time === parseInt(time, 10)) {
                    this.currentQuartile = this.currentQuartile + 1;
                    this.reportTrackers(this.adVASTTrackers[this.quartiles[i].trackingEventName], 'Quartile #' + this.currentQuartile);
                    this.quartiles.shift();
                }
            }
        }
    },

    resetAdVASTTrackers: function () {
        "use strict";
        this.adVASTTrackers = {
            start: [],
            ClickThrough: [],
            ClickTracking: [],
            skipped: [],
            firstQuartile: [],
            midpoint: [],
            thirdQuartile: [],
            completeview: [],
            complete: [],
            Impression: []
        };
    },

    loadVAST: function (VASTURL) {
        "use strict";
        logAMTA(AMTALOG_LOG, "populateQuartiles()");

        var that = this;

        $AM.ajax({
            type: "GET",
            url: VASTURL,
            dataType: "xml"
        }).done(function (data) {
            var mediaURL = $AM(data).find('MediaFile[delivery="progressive"]').text();
            var trackerEventName;

            //Populate this.adVASTTrackers with "Tracking" and "ClickThrough" Node(s) value.
            for (trackerEventName in that.adVASTTrackers) {
                that.adVASTTrackers[trackerEventName].push($AM(data).find('Tracking[event="' + trackerEventName + '"]').text());
            }

            var clickThroughLink = $AM(data).find('ClickThrough').text();
            that.clickThroughEnabled = hasValue(clickThroughLink);

            $AM(data).find('Impression').each(function () {
                that.adVASTTrackers.Impression.push(this.textContent.trim());
            });
            
            that.adVASTTrackers.ClickThrough.push(clickThroughLink);
            that.adVASTTrackers.ClickTracking.push($AM(data).find('ClickTracking').text());

            $AM(data).find('Creative').each(function () {
                if ($AM(this).find('MediaFile[delivery="progressive"]').text() === mediaURL) {
                    if ($AM(this).attr("skippable") === "1") {
                        that.skippable = true;
                    }
                }
            });

            if (hasValue(mediaURL)) {
                $AM(that).trigger({
                    type: VASTerEvents.VAST_VIDEO_URI_READY, //.AD_READY,
                    url: mediaURL
                });
            } else {
                var thirdPartyVSATTagURI = $AM(data).find('VASTAdTagURI').text();
                if (thirdPartyVSATTagURI !== "") {
                    that.loadVAST(thirdPartyVSATTagURI); //Proceed to load third party tag
                } else {
                    logAMTA(AMTALOG_WARN, "NO media url/third party vast tag found");
                }
            }
        }).fail(function (data) {
            $AM(that).trigger({
                type: VASTerEvents.EMPTY_VAST_RESPONSE
            });
        });
    },

    trackImpression: function () {
        "use strict";
        this.reportTrackers(this.adVASTTrackers.Impression, "impression");
    }
};