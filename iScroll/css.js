window.getAMCSS = function () {
    "use strict";
    return '<style>' +
        '.noScroll {' +
        '	overflow-y: hidden;' +
        '}' +
        '.noBorder {' +
        '	border: none;' +
        '}' +
        '.AM_muted {' +
        '	position: absolute;' +
        '	bottom: 10px;' +
        '	right: 10px;' +
        '	width: 32px;' +
        '	height: 26px;' +
        '	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAaCAMAAADhRa4NAAACrFBMVEUAAAABAQECAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGBgYAAAAAAAAJCQkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEHBwcNDQ0PDw8QEBAUFBQWFhYZGRkaGhocHBwhISEjIyMlJSUnJycoKCgqKiosLCwtLS0vLy8yMjI1NTY3Nzc4ODg5OTk7Ozw8PDs8PDw9PT1BQUFDQ0NEREVGRkZHR0hJSUlKSkpLS0tQUFBTU1NdXV1fX19gYGBhYWFiYmJkZGRmZmZnZ2dqamptbW1tbW5vb29xcXFzc3N0dHR1dXV2dnZ4eHd4eHh5eXl7e3t8fHx9fX1+fn2EhISGhoaHh4eIiIiMjIuMjIyRkZGTk5OWlpaXl5ednZ2enp6fn5+hoaGmpqampqenp6eqqqqxsbG2tra3t7e4uLi+vr6+vr+/v76/v7/Dw8TExMTJycnJycrMzMzOzs7Pz8/V1dXZ2dnb29vk5OTp6ent7e3x8fH39/f4+Pj5+fn7+/v+/v7////PdqI4AAAAd3RSTlMAAAABAgMGCAoLEhQVFhcYGRscHB0eHyAhIyYnKCotMDI1Njk6PT9CSU1QVFVZXF5hYnFyc3V5e35/gIGDhIWIjI2QkZKVl5ugoqOlpqepqqutrq+zv8LEyc7R0tTY2tvd3uLl5ujp6uvs7u7v8fHy8/T3+fr7/WIFIzMAAAH+SURBVHgBfdLndwxRGMfx30QvRFmr9xIRWUH0LgoRxSohFmEjooSElUfvw0YZJTqREL333kuCJKIXovD7R+zdnD3sOSbfF/NmPueZufc8QICGvzUcM6U7/kmDP2gjB3IWNzUHXeUI6W5vCobLHZLrOpiA8uPkGT+/p24yoVHCyk98t/AR9fD/gtaS9oM5Iq+o2+pHRQRC1WLwQAtQ2tapc8dhcoi8JqOT8qlHzrqQlljbe6ajWZNgcS5b7+k6mSkhcBVQj1tFbkm0et4fJwWR235R9d1wBaGS5FHvG3uC3DSttxwjjWjE3OLHF7m52Ssm1gQsCgTXk5PkRvUwZgTCcZX7JSVl+oBSAKop0A6hcorfbpBb51bXMOEyM0aVKAnAB8KBYEknC9c4ywV4QaYd8ActZQ/5dbmzjAY4rnC3uObH9SsC+eoTIZLFn/fIDbOrADG3+fbJ0+z7Sx01fD9ZRw6T2+USf6fGV8SQnfRW6J7XHJUV6DM5g9yRECHnSfdYWONXG4ax+S6ZLq2w4GXRRe1KroW2cpEUoGxYtx5d7OrUZ8We5JkQNefmvuS6gEecOx0LaFCFyl7ygchr6rYGI/pXhSpo6CCrD6DJzLVfmCeP1T745QOoMH5RAT+8MQfASHnoXTlTgJ5yhkw1nwCEycHnS5oVA9A4emov+PUHn1n4rvkF6WYAAAAASUVORK5CYII=);' +
        '}' +
        '.AM_progressBar {' +
        '    width: 100%;' +
        '	 direction: ltr;' +
        '    height: 3px;' +
        '	 position: relative;' +
        '	 bottom: 0px;' +
        '	 text-align: left;' +
        '}' +
        '.AM_progressBar div {' +
        '    height: 100%;' +
        '    width: 0;' +
        '    background: #D2223A;' + 
        '    background:-webkit-linear-gradient(top, #FFAAAA, #C83339) ;' +
        '}' +
        '#AM_logo {' +
        '	right: 6px;' +
        '	bottom: 6px;' +
        '	width: 20px;' +
        '	height: 20px;' +
        '	position: absolute;' +
        '	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGOfPtRkwAAACBjSFJNAACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAKOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAEjHnZZ3VFTXFofPvXd6oc0wAlKG3rvAANJ7k15FYZgZYCgDDjM0sSGiAhFFRJoiSFDEgNFQJFZEsRAUVLAHJAgoMRhFVCxvRtaLrqy89/Ly++Osb+2z97n77L3PWhcAkqcvl5cGSwGQyhPwgzyc6RGRUXTsAIABHmCAKQBMVka6X7B7CBDJy82FniFyAl8EAfB6WLwCcNPQM4BOB/+fpFnpfIHomAARm7M5GSwRF4g4JUuQLrbPipgalyxmGCVmvihBEcuJOWGRDT77LLKjmNmpPLaIxTmns1PZYu4V8bZMIUfEiK+ICzO5nCwR3xKxRoowlSviN+LYVA4zAwAUSWwXcFiJIjYRMYkfEuQi4uUA4EgJX3HcVyzgZAvEl3JJS8/hcxMSBXQdli7d1NqaQffkZKVwBALDACYrmcln013SUtOZvBwAFu/8WTLi2tJFRbY0tba0NDQzMv2qUP91829K3NtFehn4uWcQrf+L7a/80hoAYMyJarPziy2uCoDOLQDI3fti0zgAgKSobx3Xv7oPTTwviQJBuo2xcVZWlhGXwzISF/QP/U+Hv6GvvmckPu6P8tBdOfFMYYqALq4bKy0lTcinZ6QzWRy64Z+H+B8H/nUeBkGceA6fwxNFhImmjMtLELWbx+YKuGk8Opf3n5r4D8P+pMW5FonS+BFQY4yA1HUqQH7tBygKESDR+8Vd/6NvvvgwIH554SqTi3P/7zf9Z8Gl4iWDm/A5ziUohM4S8jMX98TPEqABAUgCKpAHykAd6ABDYAasgC1wBG7AG/iDEBAJVgMWSASpgA+yQB7YBApBMdgJ9oBqUAcaQTNoBcdBJzgFzoNL4Bq4AW6D+2AUTIBnYBa8BgsQBGEhMkSB5CEVSBPSh8wgBmQPuUG+UBAUCcVCCRAPEkJ50GaoGCqDqqF6qBn6HjoJnYeuQIPQXWgMmoZ+h97BCEyCqbASrAUbwwzYCfaBQ+BVcAK8Bs6FC+AdcCXcAB+FO+Dz8DX4NjwKP4PnEIAQERqiihgiDMQF8UeikHiEj6xHipAKpAFpRbqRPuQmMorMIG9RGBQFRUcZomxRnqhQFAu1BrUeVYKqRh1GdaB6UTdRY6hZ1Ec0Ga2I1kfboL3QEegEdBa6EF2BbkK3oy+ib6Mn0K8xGAwNo42xwnhiIjFJmLWYEsw+TBvmHGYQM46Zw2Kx8lh9rB3WH8vECrCF2CrsUexZ7BB2AvsGR8Sp4Mxw7rgoHA+Xj6vAHcGdwQ3hJnELeCm8Jt4G749n43PwpfhGfDf+On4Cv0CQJmgT7AghhCTCJkIloZVwkfCA8JJIJKoRrYmBRC5xI7GSeIx4mThGfEuSIemRXEjRJCFpB+kQ6RzpLuklmUzWIjuSo8gC8g5yM/kC+RH5jQRFwkjCS4ItsUGiRqJDYkjiuSReUlPSSXK1ZK5kheQJyeuSM1J4KS0pFymm1HqpGqmTUiNSc9IUaVNpf+lU6RLpI9JXpKdksDJaMm4ybJkCmYMyF2TGKQhFneJCYVE2UxopFykTVAxVm+pFTaIWU7+jDlBnZWVkl8mGyWbL1sielh2lITQtmhcthVZKO04bpr1borTEaQlnyfYlrUuGlszLLZVzlOPIFcm1yd2WeydPl3eTT5bfJd8p/1ABpaCnEKiQpbBf4aLCzFLqUtulrKVFS48vvacIK+opBimuVTyo2K84p6Ss5KGUrlSldEFpRpmm7KicpFyufEZ5WoWiYq/CVSlXOavylC5Ld6Kn0CvpvfRZVUVVT1Whar3qgOqCmrZaqFq+WpvaQ3WCOkM9Xr1cvUd9VkNFw08jT6NF454mXpOhmai5V7NPc15LWytca6tWp9aUtpy2l3audov2Ax2yjoPOGp0GnVu6GF2GbrLuPt0berCehV6iXo3edX1Y31Kfq79Pf9AAbWBtwDNoMBgxJBk6GWYathiOGdGMfI3yjTqNnhtrGEcZ7zLuM/5oYmGSYtJoct9UxtTbNN+02/R3Mz0zllmN2S1zsrm7+QbzLvMXy/SXcZbtX3bHgmLhZ7HVosfig6WVJd+y1XLaSsMq1qrWaoRBZQQwShiXrdHWztYbrE9Zv7WxtBHYHLf5zdbQNtn2iO3Ucu3lnOWNy8ft1OyYdvV2o/Z0+1j7A/ajDqoOTIcGh8eO6o5sxybHSSddpySno07PnU2c+c7tzvMuNi7rXM65Iq4erkWuA24ybqFu1W6P3NXcE9xb3Gc9LDzWepzzRHv6eO7yHPFS8mJ5NXvNelt5r/Pu9SH5BPtU+zz21fPl+3b7wX7efrv9HqzQXMFb0ekP/L38d/s/DNAOWBPwYyAmMCCwJvBJkGlQXlBfMCU4JvhI8OsQ55DSkPuhOqHC0J4wybDosOaw+XDX8LLw0QjjiHUR1yIVIrmRXVHYqLCopqi5lW4r96yciLaILoweXqW9KnvVldUKq1NWn46RjGHGnIhFx4bHHol9z/RnNjDn4rziauNmWS6svaxnbEd2OXuaY8cp40zG28WXxU8l2CXsTphOdEisSJzhunCruS+SPJPqkuaT/ZMPJX9KCU9pS8Wlxqae5Mnwknm9acpp2WmD6frphemja2zW7Fkzy/fhN2VAGasyugRU0c9Uv1BHuEU4lmmfWZP5Jiss60S2dDYvuz9HL2d7zmSue+63a1FrWWt78lTzNuWNrXNaV78eWh+3vmeD+oaCDRMbPTYe3kTYlLzpp3yT/LL8V5vDN3cXKBVsLBjf4rGlpVCikF84stV2a9021DbutoHt5turtn8sYhddLTYprih+X8IqufqN6TeV33zaEb9joNSydP9OzE7ezuFdDrsOl0mX5ZaN7/bb3VFOLy8qf7UnZs+VimUVdXsJe4V7Ryt9K7uqNKp2Vr2vTqy+XeNc01arWLu9dn4fe9/Qfsf9rXVKdcV17w5wD9yp96jvaNBqqDiIOZh58EljWGPft4xvm5sUmoqbPhziHRo9HHS4t9mqufmI4pHSFrhF2DJ9NProje9cv+tqNWytb6O1FR8Dx4THnn4f+/3wcZ/jPScYJ1p/0Pyhtp3SXtQBdeR0zHYmdo52RXYNnvQ+2dNt293+o9GPh06pnqo5LXu69AzhTMGZT2dzz86dSz83cz7h/HhPTM/9CxEXbvUG9g5c9Ll4+ZL7pQt9Tn1nL9tdPnXF5srJq4yrndcsr3X0W/S3/2TxU/uA5UDHdavrXTesb3QPLh88M+QwdP6m681Lt7xuXbu94vbgcOjwnZHokdE77DtTd1PuvriXeW/h/sYH6AdFD6UeVjxSfNTws+7PbaOWo6fHXMf6Hwc/vj/OGn/2S8Yv7ycKnpCfVEyqTDZPmU2dmnafvvF05dOJZ+nPFmYKf5X+tfa5zvMffnP8rX82YnbiBf/Fp99LXsq/PPRq2aueuYC5R69TXy/MF72Rf3P4LeNt37vwd5MLWe+x7ys/6H7o/ujz8cGn1E+f/gUDmPP8usTo0wAAAAlwSFlzAAALEgAACxIB0t1+/AAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC45bDN+TgAACC5JREFUWEetlwlwlOUZx7/dTXZzbM5Nsptzw+Y+SbK57xCEUGzRgQ62tbQpdixMEey0RXEqUyuKtMWbTi+d6YEdxdY6rUKtaFFboYK1iBotRm0CylFF0BqOvv3/vm/DTNVQLH1m3snme57veZ/j//zf97M+oiTlen3tzSlpi2ZmBFbOCWSv1rq6Lz3z8ppk/wK/x1Mds/v/Slli8pwrwpFNm2obD/65udO83NFnxjoHzHhsvdbZb15o6zWPNraZH1TUjlwUzF2v16qct89B3C5X6TWRsq2vdPSbt3vP00YDZndrt3lKQex439rV3KUgeszB7kHzlmwJRtW5UW5cjrePKB1p6Sv+0Nh24mjvLHvTJ5razZ+iHfbvVxXQG9rosNYhrf1dM8zf2vvsICbtqNA/emaa75RW7vG63b0xt2cnQ4HsdUeUBY4fa2o77ZDNtivbjdXTzTXTysySvKKJFQXFEzeVVZnf1EftNrypTanENlWASv2zb7Z5RL+T3J55MfdnltmZWWtfj2VEGZ+XM5z+oqbBLMzJHcv1+W6R2YVa9DhHK6hVqTrPrfenrF1ZFHlxuwI+3D3TPKm/j6siJPNAfbNJdHvOl+3U0uBPXUqv2fyRxlbzmkq9V4D7bCjfSP0Vx+q/S7bXO/z9itrjx9S+nWrLtsZ2c1R+f1nbhJ86x+qDkqueG7Inc5C9V4E0+lNHpetwTCyrNDFp8HOh/PXfKC7dsjZS8dS1kfLHl+aHf9qdnrFE6izHypbqK8Mlu8meyfmjKnGif8h8Kb9oJKb/T7kyHNn5Tt8s9bxdZe+1Ed+QkrpPKttpRlx8523l1U+OtPfaJT2stryhYEH9mz3n2YHfXxc92pmWsRr7mCQIK6Pvyi+t2KN24ldtvDqmdyTflzD4qjIG4QCONlyYHaRcUfSB+Ph++nqif7Z5pqXbzob/Ka+dXbTd7Ih2mgMKhuAuyA7ey3sxqVBgNqC3CdD4/nZJJb4DjlqyvLD4PnrECO2T4caa6RisQJcT720jqEMCFcgGWPu6Bmy7EVVqVDhh47+0dBlayDOC6EnP+AnvI4UJiUsY3V2yIQEwVuBLuCqmtrx31TQcY8xwDuKHMrNej+ms28ur99M7nLMJmfxMYzg/O7Rvuj/lwe60jN9dH6k4glMYkpGDuBhJgfELMTfyU3PwQNegXb0jatnFobwdtiIQ7+2AyZ5V+fe09qicHSbZ4/kuutrklAXjypbIsTnQPQMQUZ3LtPzYxCS7KSX1BuxeEkbA0Xua/6uKSw5JF4+BWnrr5GjCJ+tKKo7rcdBqSUlbPKrIIQ2qcEdlHRvM4iXN9GZaA4AAm7JAtxjdh8lARuDH2IEPJuj3Da3G43LZs1+elLzgOVXlaQUJ3uAVPR6EeFaRJRlCrV8riqDI5aUfVdaN02snsz5Tmpj8DM/PIKH766MnaQcbPSfU650bUKTHxTdRGZ6Bk83TW0ycy7XImpcdXLOfAIRi+n9xKP+Y7Upyb23j8ZfVzxdV1i16QY++6WimFnH/CKBkOsi0IzX9Lp7rPJj2kHyMKADYFUzp+F5mfTwr51pQTQAcHotzC08HcE9t4wQoh9u3qpwul3VFTDWl3FxWPQonEABgbEtNv5vniR5P6cPygS8WYNWzZVZ/euZX/67eOyAbNNdHyv8le/jd2lBR8wpkgzPaJMQ/zPOpRP0uBy+UmXH7q4BdkJC4AV3I67PBDtfQIrVKCbmGraok/0X0ZJeUZLvJ4es+XvpyQfi+d8TnOCVIQKXH3eg+TJYXFD9AGxk1GI8TUo+haCuakjYMFwB2krmjygb7HMvndldt1cEDxQIc+iPisHtdlJA4FzQ/q/EkCO4Ad1bVHRWVXoB+UnRxCS/NL9rIpnAFtjHeZxO7mvq9ieAgNf6uCpdQ6TA6Sxw/xozChIydjPfaColG8Xkz42M2P6Cnv9ubO8zakordlxWE79YZsuXXddETvE/ZOcjA0kOqltfl/l7Mjfe39c2nqDABcI4MZgR2x3SWNS8reCscjZJ20HONyEJ06mvVnVX173KgsDHZURECgVCYe6rEecCY8T9kpOqxgU1WcwPZtxMU/vcqCBJJcLvXorPFbbmKQSVExNmNsSj4PakKHQurWv+/fWpgyO4tAcJokwviAdmw33ZNUyQxaY/eSeXFQl/CfGeSeu2NOXEX5oRoTRH606Kr9QbKD0hwiuHXVX6pEhwLq3Q4VLAZPuCuwMRQAdZ45wyzU72/LlJukjyen8vWzlxZNqglJ6kKreEciQGZW9UHRSM4PqEsKCUXzCOqxJpIOUE0OBa29LSkpt20ICf02KJQ/shngnkviIIfzYyPXyddk2NiWfpO+LzIxt4c0uGcoc26dY1J7Xas3ifxLncrkXKccnZTasbwV3VNRofN0pjZGUWYCX06mHcLs06VJk/RCd0lLs2zp6LfsZxC0uLi5lNmoqVngI7zAIc/rKwd+1Qw7zZl90llXK1yB7Vy9bUU7UrLuOTywuJ7xPEnaQvjTDIAlpux7hxsfvp4PqP4XO55GhsbVBzTXERoCT0kK5iONkHPLEAI7XKYMRGg3Ql8wJ75ZQVhNh92vJ+9dK4MR57GMU4oIwwHQCEsejrJ69Ar7WJj9FQLQFMB0fdL8jXHcfk/iKj6WxvKa45zPX9LNxkqMDlWlJnFaQlVM76UnyC+mFdo9K0A2n2Op3OTosok/yqdlE+sL62c0EeqPU5kyHpQ7eKatnpa6anzAzm7dKNao3fO/eN0CoGchvSVc4nfE7dcZ/oKsealevYJrTIMzl4s69/BE7dlu7XDeQAAAABJRU5ErkJggg==);' +
        '   background-size: cover;' +
        '   opacity: 0.65;' +
        '}' +
        '.AM_logo_large {' +
        //'	right: 6px;' +
        //'	bottom: 6px;' +
        '	width: 32px!important;' +
        '	height: 32px!important;' +
        //'	background-size: contain;' +
        //'	background-position: center center;' +
        //'	position: absolute;' +
        '}' +
        '#AM_adHeader {' +
        '	direction: ltr;' +
        '	height: 22px;' +
        '	width: 100%;' +
        '	background-color: #000000;' +
        '	position: relative;' +
        '   margin-bottom: 0px;' +
        '   position: absolute;' +
        '}' +
        '#AM_adHeader p {' +
        '	padding-top: 2px;' +
        '   margin-bottom: 0px;' +
        '   margin-left: 0px;' +
        '   margin-right: 0px;' +
        '	color:#ffffff;' +
        '	text-align:center !important;' +
        '	font-size: 14px;' +
        '	font-family: Arial;' +
        '}' +
        '</style>';
};